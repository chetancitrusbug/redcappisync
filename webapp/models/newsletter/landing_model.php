<?php
/*
	Model class for campaign
*/
class Landing_Model extends CI_Model
{
	//Constructor class with parent constructor
	function Landing_Model()
	{
		parent::__construct();
		$this->load->model('newsletter/signup_Model');
		
	}
	
	//function to create campaign
	function create_landing($input_array)
	{
		$this->db->insert('red_landing',$input_array);
		return $this->db->insert_id();
	}
        
   /*  function get_campaign($conditions_array)
	{       $this->db->select('camp.campaign_id,camp.campaign_title,camp.is_deleted');
		$this->db->from('red_email_campaigns as camp');
		$this->db->where($conditions_array, NULL, FALSE);
		$result=$this->db->get();			
		$retval = $result->result_array();		
		$result->free_result();
                
                foreach($retval as $key => $value){
                    $fArray[$value['campaign_id']]['campaign_id'] = $value['campaign_id'];
                    $fArray[$value['campaign_id']]['campaign_title'] = $value['campaign_title'];
                }
		return $fArray;	
	} */
	
	//function to update campaign
	function update_landing($input_array,$conditions_array){
		$this->db->update('red_landing',$input_array,$conditions_array);
		# echo $this->db->last_query();
		return $this->db->affected_rows();
	}
	
	//Delete function, actually updating 'is_deleted' status of table to 1
	function delete_landing($conditions_array){		
		$this->db->update('red_landing',array('is_deleted'=>1),$conditions_array);
		return $this->db->affected_rows();
	}
	function delete($conditions_array){		
		$this->db->delete('red_landing',$conditions_array);
		return $this->db->affected_rows();
	}
	
	function getPersonalization(&$message=""){
		$arrContactPersonalization = $this->getPersonalizationToken();
		$message = $this->personalizeNow($message, $arrContactPersonalization);
		$message		= str_replace($fallback_search_arr, $fallback_replace_arr, $message);
		return $message;
	}
	
	function personalizeNow($strtoPersonalize, $arrContactPersonalizationDetail){
		$fallback_search_arr = array();
		$fallback_replace_arr = array();
		
		$pattren="/\{([a-zA-Z0-9_\s-!#&(),.@])*\}/";
		preg_match_all($pattren,$strtoPersonalize,$arr_personalization );
		
		foreach($arr_personalization[0] as $personalization_str){	
			
			$this_personalization_arr =explode(",",$personalization_str,2);			
			$token = trim(trim($this_personalization_arr[0], '{}'));			
			$token_fallback = trim(rtrim($this_personalization_arr[1], '}'));
			
			$fallback_search_arr[]=$personalization_str;
			if(strtoupper($token) == 'UNSUBSCRIBE'){				
				$unsubscribe_text =	($token_fallback =='')?'Unsubscribe': $token_fallback;				 
				$fallback_replace_arr[]="<a href='".base_url()."newsletter/unsubscribe_mail/unsubscribe/[campaign_id]/{$schedule_subscriber}'>{$unsubscribe_text}</a>";
			}elseif(strtoupper($token) == 'FORWARD'){				
				$forward_text =	($token_fallback =='')?'Forward': $token_fallback;				 
				$fallback_replace_arr[]="<a href='".base_url()."newsletter/forward_to_friend/index/[campaign_id]/[subscriber_id]/'>{$forward_text}</a>";
			}else{				
				$fallback_replace_arr[]= (isset($arrContactPersonalizationDetail[$token]) and trim($arrContactPersonalizationDetail[$token]) !='')?trim($arrContactPersonalizationDetail[$token]): $token_fallback;				 
			}			
		}		 
		return str_replace($fallback_search_arr, $fallback_replace_arr, $strtoPersonalize);		
	}
	function getPersonalizationToken(){
		$signupdata = $this->signup_Model->get_signup_data(array('member_id' => $this->session->userdata('member_id'),'is_deleted' => 0));
		
		$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<title></title>
<!-- ------------Load js----------------------------- -->
<script type="text/javascript" src="https://www.redcappi.us/webappassets/js/jquery-1.4.4.min.js?v=6-20-13"></script>
<script type="text/javascript" src="https://www.redcappi.us/webappassets/js/jquery.validate.js?v=6-20-13"></script>
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>
<!-- ------------Load Css----------------------------- -->
<link rel="shortcut icon" href="https://www.redcappi.us/favicon.ico">
<link rel="stylesheet" type="text/css" href="https://www.redcappi.us/webappassets/css/signup_form.css?v=6-20-13" />
<script type="text/javascript">
	$(document).ready(function() {
    $("#signupform").validate({ 
rules: {
"email": { required: true, email: true}
 },
messages: {
"email": { required: "Required", email: "Require a valid email address."}
 },	highlight: function(element) {
			$(element).css("border", "2px solid red");
		},
		success: function(element) {
			$(element).css("border", "1px solid #ebebeb");
		}, 

		submitHandler: function( form ) {   
			var ajax_url = ($( "#signupform" ).attr( "action" ));
			var x=$("#signupform").serialize();
	
			$.ajax({
				url : ajax_url,
				data : x,
				type: "POST",				
				success : function(data){  
					var msg = data.split("|");					
					if(msg[0] == "ok"){
						window.location.href= msg[1];					
					}else if(msg[0] == "err"){
						$("#validation-error").html(msg[1]);
					}else{
						//$("#signupform").hide("slow");
						$("#validation-error").html(data);
					}
				}
				})
			return false;
			}
		});

    (function() {
      var color;
      var style = $("body").attr("style").split(";");

      for (var x = 0; x < style.length; x++) {
        style[x] = style[x].replace(/\s/g,"");

        if (style[x].indexOf("background-color") === 0) {
          color = style[x].replace("background-color:","");
        }
      }

      if (color) {
        if (color.indexOf("#") === 0) {
          var c = color.substring(1);      // strip #
          var rgb = parseInt(c, 16);   // convert rrggbb to decimal
          var r = (rgb >> 16) & 0xff;  // extract red
          var g = (rgb >>  8) & 0xff;  // extract green
          var b = (rgb >>  0) & 0xff;  // extract blue
        } else if (color.indexOf("rgb") === 0) {
          color = color.replace("rgb(","");
          color = color.replace(")","");
          color = color.split(",");

          var r = parseInt(color[0]);
          var g = parseInt(color[1]);
          var b = parseInt(color[2]);
        }

        var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

        if (luma > 60) {
          luma = Math.floor(Math.abs(luma - Math.floor(luma/2)));

          $(".footlink").css("color","rgb(" + luma + "," + luma + "," + luma + ")");
          $(".footlink a").css("color","rgb(" + luma + "," + luma + "," + luma + ")");
        } else {
          luma = Math.floor(luma + 180);

          $(".footlink").css("color","rgb(" + luma + "," + luma + "," + luma + ")");
          $(".footlink a").css("color","rgb(" + luma + "," + luma + "," + luma + ")");
        }
      }
    })();
	});
</script>
</head>

<body  style="background-color:#F0F1F3;background-image:none;background-repeat:repeat;">
	<div style="margin:  auto;text-align: center;background-color:;"><span class="form_title" style="font-weight:bold;font-size:27px;text-align:left;"></span></div><form action="" method="post" class="signupform" id="signupform" accept-charset="UTF-8">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formTable"><tr><td class="form_title" style="font-weight:bold;font-size:27px;text-align:center;"><div class="header-txt" style="padding:20px 0 15px;background-color:#FFFFFF;color:#454545;">Join our mailing list</div></td></tr><tr><td><p></p></td></tr><tr><td><div id="validation-error"></div></td></tr><tr><td><label><span class="form-label">Email</span><span>*</span></label><br/><input type="text" id="signup_email" name="email" /></td></tr>
 <tr><td><input type="submit" name="listing_submit" value="Subscribe" id="btnSubmit" class="submit_button" style="background-color:;"   content="Submit"></td></tr></table></form>	<div class="footlink">Powered by <a href="https://www.redcappi.us/" target="_blank">RedCappi</a><img src="http://www.redcappi.us/newsletter/signup/showpblogo/26" alt="" title="" border="0"></div>
</body>
</html>';


$arrPersonalizationToken[26] = $html;
		/* foreach($signupdata as $key => $val){
			
			
			$arrPersonalizationToken[$val['id']] = json_decode($val['site_html']);
		}//exit; */
		
		//print_r($arrPersonalizationToken);exit;
		return 	$arrPersonalizationToken;
	}
	
	
	
	
}
?>