<?php
/**
  *	Model class for subscriptions 
  *	It have  functions for interaction with database.
 */
class Subscription_Model extends CI_Model
{
	//Constructor class with parent constructor
	function Subscription_Model(){
		parent::__construct();
	}
	
	/**
	 *	Function create_subscription
	 *
	 *	Function to create new subscription
	 *
	 *	@param (array) (input_array)  values to insert into database
	 *	@return (int)	return inserted subscription id
	 */
	function create_subscription($input_array){
		$this->db->insert('red_email_subscriptions',$input_array);
		return $this->db->insert_id();
	}
	
	/**
	 *	Function update_subscription
	 *
	 *	Function to update existing subscription
	 *
	 *	@param (array) (input_array)  values to update into database
	 *
	 *	@param (array) (conditions_array)  conditions to checked with database with conditions
	 *
	 *	@return (int)	return updated subscription id
	 */
	function update_subscription($input_array,$conditions_array){
		$this->db->update('red_email_subscriptions',$input_array,$conditions_array);
		return $this->db->affected_rows();
	}
	
	/**
	 *	Function delete_subscription
	 *
	 *	Function to delete existing subscription
	 *
	 *	@param (array) (conditions_array)  conditions to checked with database with conditions
	 *
	 *	@return (int)	return deleted subscription id
	 */
	function delete_subscription($conditions_array){
		$this->db->update('red_email_subscriptions',array('is_deleted'=>1),$conditions_array);
		return $this->db->affected_rows();
	}
	
	/**
	 *	Function get_subscription_data
	 *
	 *	Function to fetch subscription data
	 *
	 *	@param (array) (conditions_array)  conditions to checked with database with conditions
	 *
	 *	@param (int) (rows_per_page)  number of record per page
	 *
	 *	@param (int) (start)  These determine which number to start the record
	 *
	 *	@return (array)	return fetch records
	 */
	function get_subscription_data($conditions_array=array(),$rows_per_page=10,$start=0){
		$rows=array();
		$this->db->order_by('subscription_id','asc');	// define order by		
		$result=$this->db->get_where('red_email_subscriptions',$conditions_array); //execute query		
		foreach($result->result_array() as $row){
			$rows[]=$row;
		}
		$result->free_result();
		return $rows;
	}
	
	/**
	 *	Function get_subscription_count
	 *
	 *	Function to fetch subscription count
	 *
	 *	@param (array) (conditions_array)  conditions to checked with database with conditions
	 *
	 *	@return (int)	return total number of records
	 */
	function get_subscription_count($conditions_array=array()){
		$this->db->where($conditions_array);
		return $this->db->count_all_results('red_email_subscriptions');		
	}
	/**
		function get_subscription_list to fetch subscription list from red_email_subscription_subscriber
	*/
	function get_subscription_list($conditions_array=array()){
		$rows=array();
		$this->db->select('*');
		$this->db->from('red_email_subscription_subscriber as ress');
		$this->db->join('red_email_subscriptions as res','res.subscription_id =ress.subscription_id');
		$this->db->where($conditions_array); //execute query
		$result=$this->db->get();		
		foreach($result->result_array() as $row){
			$rows[]=$row;
		}
		$result->free_result();
		return $rows;
	}
	
	/****
	
	Get segmentation criteria
	
	****/
	
	function get_segmentation($conditions_array=array(),$rows_per_page = 10, $start = 0, $bounce = false){
		$rows=array();
		$this->db->select('*');
		$this->db->from('red_email_subscriptions as res');
		$this->db->join('red_segmentation as sg','res.subscription_id = sg.sg_subscription_id','left');
		$this->db->where($conditions_array); //execute query
		$result=$this->db->get();		
		foreach($result->result_array() as $row => $val){
			
			$rows[$val['subscription_id']]['subscription_id']=$val['subscription_id'];
			$rows[$val['subscription_id']]['subscription_title']=$val['subscription_title'];
			$rows[$val['subscription_id']]['subscription_created_by']=$val['subscription_created_by'];
			$rows[$val['subscription_id']]['subscription_type']=$val['subscription_type'];
			if(count($val['sg_ref_id']) >0 ){
				$rows[$val['subscription_id']]['segmentation'][$val['sg_ref_id']]['sg_ref_id']=$val['sg_ref_id'];
				$rows[$val['subscription_id']]['segmentation'][$val['sg_ref_id']]['sg_segment_key']=$val['sg_segment_key'];
				$rows[$val['subscription_id']]['segmentation'][$val['sg_ref_id']]['sg_segment_val']=$val['sg_segment_val'];
				$rows[$val['subscription_id']]['segmentation'][$val['sg_ref_id']]['sg_segment_condition']=$val['sg_segment_condition'];
			}
			
		}
		$result->free_result();
		
		return $rows;
	}
	
	/****
	
	Get segmentation data according to criteria
	
	****/
	
	function get_segmentation_data($conditions_array=array(),$mid,$rows_per_page = 10, $start = 0, $bounce = false){
		$rows=array();
		$this->db->select('*');
		$this->db->from('red_email_subscriptions as res');
		$this->db->join('red_segmentation as sg','res.subscription_id = sg.sg_subscription_id','left');
		$this->db->where($conditions_array); //execute query
		$result=$this->db->get();		
		foreach($result->result_array() as $row => $val){
			
			$rows[$val['sg_ref_id']]['sg_ref_id']=$val['sg_ref_id'];
			$rows[$val['sg_ref_id']]['sg_segment_key']=$val['sg_segment_key'];
			$rows[$val['sg_ref_id']]['sg_segment_val']=$val['sg_segment_val'];
			$rows[$val['sg_ref_id']]['sg_segment_condition']=$val['sg_segment_condition'];
			$rows[$val['sg_ref_id']]['subscription_type']=$val['subscription_type'];
		}
		
		
		$swhere = "res.subscriber_created_by = ".$mid." and res.subscriber_status = 1 and res.is_deleted = 0";
		$i = 1;
		foreach($rows as $key => $s_val){
			if($s_val['subscription_type'] == 5){
	
				if($s_val['sg_segment_condition'] == 'is'){
					$s_val['sg_segment_condition'] = 'LIKE';
				}else{
					$s_val['sg_segment_condition'] = 'NOT LIKE';
				}
				$finalfields = $s_val['sg_segment_key'];
				$fieldnameArray = explode('_',$s_val['sg_segment_key']);
				
				$operator = $s_val['sg_segment_condition'];
				$fieldvalue = $s_val['sg_segment_val'];
				if($fieldnameArray[0] == 'subscriber'){
					$whereArray[$i] = ' AND '.$finalfields .' '.$operator .' "%'.$fieldvalue.'%"';
				}
				if($fieldnameArray[0] == 'global'){
					if(count($fieldnameArray) > 2){
						array_shift($fieldnameArray);
						$fieldnameArray[1] =  implode('_',$fieldnameArray);
					}
					$whereArray[$i] = ' AND gl.global_key = "'.$fieldnameArray[1].'" AND ref.value  '. $operator  .' "%'.$fieldvalue.'%"';
				}
				if($fieldnameArray[0] == 'cust'){
					if(count($fieldnameArray) > 2){
						array_shift($fieldnameArray);
						$fieldnameArray[1] =  implode('_',$fieldnameArray);
					}
					$whereArray[$i] = ' AND ress.key = "'.$fieldnameArray[1].'" AND eref.value  '. $operator  .' "%'.$fieldvalue.'%" ';
				}
				$i++;
				foreach ($whereArray as $wh){
					$swhere .= $wh;
				}

			}else{
				if($s_val['sg_segment_key'] == 'recently_opened'){
					$swhere .= ' AND trk.email_track_read_date >= ( CURDATE() - INTERVAL '.$s_val['sg_segment_val'].' DAY ) AND trk.email_track_read = 1';
				}elseif($s_val['sg_segment_key'] == 'recently_clicked'){
					$swhere .= ' AND trk.date_click >= ( CURDATE() - INTERVAL '.$s_val['sg_segment_val'].' DAY ) AND trk.email_track_click = 1 ';
				}else{
					$swhere .= ' AND trk.email_sent_date >= ( CURDATE() - INTERVAL '.$s_val['sg_segment_val'].' DAY ) AND trk.email_track_read = 0';
				}
			}
		}
		
		return $swhere;
	}
	
}
?>