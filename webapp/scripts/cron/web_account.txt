MAILTO="alec@redcappi.com"
*/1 * * * * /usr/local/bin/php /peakhost/redcappi/public_html/index.php newsletter/cronjob send
*/15 * * * * /usr/local/bin/php /peakhost/redcappi/public_html/index.php newsletter/pmta create
0 0,12 * * * /usr/local/bin/php /peakhost/redcappi/public_html/index.php payment_cronjob index
0 0 * * * /usr/local/bin/php /peakhost/redcappi/public_html/index.php delete_user_account index
0 0 * * * /usr/local/bin/php /peakhost/redcappi/public_html/index.php newsletter/campaign_email_track_restorage index
0,30 * * * * /usr/local/bin/php /peakhost/redcappi/public_html/index.php newsletter/cronjob send_autoresponder