#!/bin/bash

if [ "$(id -un)" != "redcappi" ]; then
    echo "This script must be run as redcappi" 1>&2
    exit 1
fi

DOMAIN="redcappi.us"
WEBAPP_REPO_DIR="/opt/redcappi/deploy/RedCappiWebApp"
ASSETS_REPO_DIR="/opt/redcappi/deploy/RedCappiWebAppAssets"
TARGET_DIR="/var/www/redcappi"


echo "==[Updating WebApp from Git]=================="
cd $WEBAPP_REPO_DIR
git pull


echo "==[Updating Assets from Git]=================="
cd $ASSETS_REPO_DIR
git pull


echo "==[Deploying WebApp]=========================="
rsync -avh --progress --delete $WEBAPP_REPO_DIR/docroot       $TARGET_DIR
mv -f $TARGET_DIR/docroot/webapp/config/config.php.$DOMAIN    $TARGET_DIR/docroot/webapp/config/config.php
mv -f $TARGET_DIR/docroot/webapp/config/database.php.$DOMAIN  $TARGET_DIR/docroot/webapp/config/database.php
mv -f $TARGET_DIR/docroot/webapp/config/constants.php.$DOMAIN $TARGET_DIR/docroot/webapp/config/constants.php
rm -f $TARGET_DIR/docroot/webapp/config/config.php.*
rm -f $TARGET_DIR/docroot/webapp/config/database.php.*
rm -f $TARGET_DIR/docroot/webapp/config/constants.php.*


echo "==[Deploying Assets]=========================="
rsync -avh --progress --delete --exclude='.git*' --delete-excluded $ASSETS_REPO_DIR/* $TARGET_DIR/docroot/webappassets


