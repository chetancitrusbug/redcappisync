<?php
/**
  *	Controller class for landng page
  *	It have controller functions for campaign management.
 */
class Landing extends CI_Controller
{
	
	/**
	  *	Contructor for controller.
	  *	It checks user session and redirects user if not logged in
	 */
	private $confg_arr = array(); 
	function __construct(){
        parent::__construct();
		// check via common model
		if(!$this->is_authorized->check_user())
			redirect('user/index');
		// Create user's folders
		$this->is_authorized->createUserFiles();
		
		// Load libraries, models and helpers
		$this->load->library('upload');
		$this->load->model('newsletter/Page_Model');
		$this->load->model('newsletter/Campaign_Model');
		$this->load->model('newsletter/Landing_Model');
		$this->load->model('newsletter/contact_model');
		$this->load->model('newsletter/subscription_Model');
		$this->load->model('newsletter/subscriber_Model');
		$this->load->model('newsletter/signup_Model');
		$this->load->model('UserModel');
		$this->load->model('ConfigurationModel');
		$this->load->model('BeeModel');
		$this->confg_arr=$this->ConfigurationModel->get_site_configuration_data_as_array();
		if($this->confg_arr['maintenance_mode'] !='no'){
			redirect ("/site_under_maintenance/");
			exit;
		}
		
		$this->output->enable_profiler(false);

		// Get absolute path for uploading
		$user_dir = $this->session->userdata('member_id') % 1000;
		$this->upload_path= $this->config->item('user_public').$user_dir .'/'.$this->session->userdata('member_id');
		// Force SSL
		force_ssl();
	}
	/*
    This function is being called for creating the landing page
     **/
	
	function create(){
		/*Get default json from db config*/
        $site_configuration_array = $this->BeeModel->get_bee_configuration_data(array('config_name' => 'default-json'));
		$landing_json = $site_configuration_array[0]['config_value'];
         /*Get default html from db config*/
        $default_html = $this->BeeModel->get_bee_configuration_data(array('config_name' => 'default-html'));
        $landing_html = $default_html[0]['config_value'];
       
        $input_array = array(
			'lan_member_id'=> $this->session->userdata('member_id'),
			'lan_html' => $landing_html,
			'lan_json' => $landing_json,
			'is_deleted' => 0,
			'created_date'=> date('Y-m-d H:i:s', now()),
			'updated_date' => date('Y-m-d H:i:s', now())
		);	
		
		
        $lan_id = $this->Landing_Model->create_landing($input_array);
		
        redirect('newsletter/landing/editor/'.$lan_id);
	}
	
	/*
    This function is being called for edit the landing page
     **/
	
	function editor($lan_id){
		/*Get Landing Title*/
		
        $sql = "Select `lan_title` from `red_landing` where `lan_id` = $lan_id";
		$josn_qry = $this->db->query($sql); #execute query
        $json_data_array = $josn_qry->result_array(); #Fetch resut
        if (count($json_data_array) > 0) {
            $lan_title = $json_data_array[0]['lan_title'];
        }
		
		
		$signupdata = $this->signup_Model->get_signup_data(array('member_id' => $this->session->userdata('member_id'),'is_deleted' => 0));
		foreach($signupdata as $signup){
			$signupArray[$signup['id']] = $signup['form_name'];
		}
		echo $this->load->view('newsletter/landing_editor', array('member_id' => $this->session->userdata('member_id'),'lan_id' =>$lan_id,'lan_title' => $lan_title,'signup' => $signupArray),true)	;
	}
	
	/*
    This function is being called for edit the landing page
     **/
	function landing_editor($lan_id){
		
		$sql = "Select `lan_json` from `red_landing` where `lan_id` = $lan_id";
        $josn_qry = $this->db->query($sql); #execute query
        $json_data_array = $josn_qry->result_array(); #Fetch resut
	  
		if (count($json_data_array) > 0) {
			$json_data = $json_data_array[0]['lan_json'];
			echo $json_data;
			
		}else{
			$site_configuration_array = $this->BeeModel->get_bee_configuration_data(array('config_name' => 'default-json'));
			$campaign_json = $site_configuration_array[0]['config_value'];
			echo $campaign_json;
		}
	}
	
	
	 /*
    This function is being called for the save of the landing page
     **/
	
	function save($lan_id){
		$mid = $this->session->userdata('member_id');  
        $jsondata = $_POST['json'];
        $htmldata = $_POST['html'];
        $lan_id = $_POST['lan_id'];
        $lan_title = $_POST['lan_title'];
		
		
		$html = $this->Landing_Model->getPersonalization($htmldata);
		
		$input_array = array(
			'lan_title' => $lan_title,
			'lan_member_id' => $mid,
			'lan_html' => $html,
			'lan_json' => $jsondata, 
			'updated_date' => date('Y-m-d H:i:s', now())
		);
		$this->Landing_Model->update_landing($input_array, array('lan_id' => $lan_id, 'lan_member_id' => $mid));
		
        $jsonArray['lan_id'] = $lan_id;
       
        $jsonArray['status'] = 'success';

        echo json_encode($jsonArray);
        exit;
	}
	
	 /*
    This function is being called for the preview of the landing page
     **/
     function view($lan_id){   
         
       // $id= $_GET['id'];
        
        $sql = "Select `lan_html`,`lan_title`,`lan_member_id` from `red_landing` where `lan_id` = $lan_id";
        
        $html_qry = $this->db->query($sql); #execute query
        $html_array = $html_qry->result_array(); #Fetch resut
		$subject=$html_array[0]['lan_title'];
		$subject=str_replace('$', '&#36;',$subject);
        $user=$this->UserModel->get_user_data(array('member_id'=>$html_array[0]['lan_member_id']));
        $rc_logo = $user[0]['rc_logo'];
        $campaign_content = $html_array[0]['lan_html'];
		
		//echo $campaign_content;exit;
		
        $title="<title>$subject</title>\n";
        
		$css='<link href="'.$this->config->item("webappassets").'/css/email_preview.css?v=6-20-13" rel="stylesheet"></link>'."\n";
        $js='<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>'."\n".
			'<script type="text/javascript">stLight.options({publisher: "ur-eca47de6-bbd8-292f-ea06-d74b8874e989", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>';
		/* 	
            $topBar = '	<div id="diy-header-container" class="rcemail">
							<div id="diy-header" class="rcemail">
								<strong>'.$subject.'</strong>
							</div>
						</div>'; */
            
            if (count($html_array) > 0) {
                $lan_data = $html_array[0]['lan_html'];
                $html_data =str_replace('tabindex="-1"','',$lan_data);
                if (stripos($html_data, '<head>') !== false) {
					$html_data=str_replace("<head>	","<head>".$title . $meta . $fb_meta_data . $css . $js,$html_data);	
                }else{		
                    $html_data = preg_replace('/<body(\s[^>]*)?>/i', '<head>'.$title . $meta . $fb_meta_data . $css . $js.'</head><body\\1>', $campaign_content);  
                }
                $html_data=str_replace("<title>RedCappi Landing Page</title>","", $html_data);	
                $html_data = preg_replace('/<body(\s[^>]*)?>/i', '<body\\1>'.$topBar, $html_data, 1);
                echo $html_data;
				if($rc_logo==1){ 
		          echo '<div id="footer-logo" class="rcemail"><a href="'.site_url("/").'"> <img src="'. $this->config->item('webappassets').'images-front/thanks-logo.png?v=6-20-13" alt="logo" title="logo" border="0" /></a></div>';
                }
            }else{
                
                $site_configuration_array = $this->BeeModel->get_bee_configuration_data(array('config_name' => 'default-html'));
                $campaign_html = $site_configuration_array[0]['config_value'];
                echo $campaign_html;
            }
    }
	

	
	
}