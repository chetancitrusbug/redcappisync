<html lang="en">
  <head>
  <title>Redcappi - Responsive Email Editor</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
      * {
        margin:0px;
        padding:0px;
      }
      body {
        overflow:hidden;
        background-color: #CCCCCC;
        color:#000000;
      }
      #bee-plugin-container {
        position: absolute;
        top:5px;
        bottom:30px;
        left:100px;
        right:100px;
      }
      #integrator-bottom-bar {
        position: absolute;
        height: 25px;
        bottom:0px;
        left:5px;
        right:0px;
      }
    </style>
  </head>
  <body>

      <div id="bee-plugin-container">
        <strong><span> Campaign Name <input type="text" name="campaign_title" class="campaign_title" value="<?php echo $campaign_title;?>"></span></strong>
        <span><a href="<?php echo base_url() ?>newsletter/autoresponder/responsiveview/?id=<?php echo $campaign_id;?>" target="_blank">Preview</a></span>
        <span><a href="<?php echo base_url() ?>newsletter/campaign_email_setting/autoresponder/<?php echo $campaign_id;?>" target="_blank">Next Step</a></span>
        
      </div>
      
      
    <div id="integrator-bottom-bar">
      <!-- You can change the download function to get the JSON and use this input to load it -->
     <input id="choose-template" type="hidden"  />
     
    </div>

  </body>
  <script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/Blob.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/fileSaver.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/BeePlugin.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>css/inner_red.css"></script>
<script type="text/javascript">

jQuery('.title').hide();
var request = function(method, url, data, type, callback) {
      var req = new XMLHttpRequest();
	 
		//console.log(type);
      req.onreadystatechange = function() {
        if (req.readyState === 4 && req.status === 200) {
        var response = JSON.parse(req.responseText);
          //var response = req.responseText;
          callback(response);
        }
      };

      req.open(method, url, true);
      if (data && type) {
        if(type === 'multipart/form-data') {
          var formData = new FormData();
          for (var key in data) {
            formData.append(key, data[key]);
          }
          data = formData;          
        }
        else {
          req.setRequestHeader('Content-type', type);
        }
      }

      req.send(data);
    };

    var save = function(filename, content) {
      saveAs(
        new Blob([content], {type: 'text/plain;charset=utf-8'}), 
        filename
      ); 
    };

    var specialLinks = [{
        type: 'unsubscribe',
        label: 'Unsubscribe',
        link: 'http://[unsubscribe]/'
    }, {
        type: 'subscribe',
        label: 'Subscribe',
        link: 'http://[subscribe]/'
    }];

    var mergeTags = [{
      name: 'Tag1',
      value: '[Tag1]'
    }, {
      name: 'Tag2',
      value: '[Tag2]'
    }];

    var mergeContents = [{
      name: 'content 1',
      value: '[content1]'
    }, {
      name: 'content 2',
      value: '[content2]'
    }];

    var beeConfig = {  
      uid: '<?php echo $member_id;?>',
      container: 'bee-plugin-container',
      autosave: 15, 
      language: 'en-US',
      specialLinks: specialLinks,
      mergeTags: mergeTags,
      mergeContents: mergeContents,
      onSave: function(jsonFile, htmlFile) { 
	var title = jQuery('.campaign_title').val(); 
        
        //save('newsletter.html', htmlFile);
		var postdata = {};
			postdata['json'] = jsonFile;
			postdata['html'] = htmlFile;
                        postdata['campaign_id'] = <?php echo $campaign_id;?>;
                        postdata['campaign_title'] = title;
                       
			console.log(postdata);
			 jQuery.post('<?php echo base_url() ?>newsletter/autoresponder/beefreesave/', postdata, function (result) {
				var response = jQuery.parseJSON(result);
                                console.log(response);
                                
                                if(response.status == 'success'){
						submit_frm();						
                                            }
			 });
		 
	
      },
      onSaveAsTemplate: function(jsonFile) { // + thumbnail? 
       // save('newsletter-template.json', jsonFile);
		/* 
			var postdata = {};
			postdata['json'] = jsonFile;
			console.log(postdata);
			 jQuery.post('ajaxhtml.php/', postdata, function (result) {
				
			 }); */
		
      },
      onAutoSave: function(jsonFile) { // + thumbnail? 
        console.log(new Date().toISOString() + ' autosaving...');
        window.localStorage.setItem('newsletter.autosave', jsonFile);
      },
      onSend: function(htmlFile) {
        //write your send test function here
      },
      onError: function(errorMessage) { 
        console.log('onError ', errorMessage);
      }
    };

    var bee = null;

    var loadTemplate = function(e) {
      var templateFile = e.target.files[0];
	 
      var reader = new FileReader();

      reader.onload = function() {
        var templateString = reader.result;
        var template = JSON.parse(templateString);
        bee.load(template);
      };

      reader.readAsText(templateFile);
    };
 
    document.getElementById('choose-template').addEventListener('change', loadTemplate, true);
console.log(beeConfig);
    request(
      'POST', 
      'https://auth.getbee.io/apiauth',
      'grant_type=password&client_id=ca2ef0e3-cf4b-4e79-84e1-eda0a560051a&client_secret=8Gla59jBi5IYY2KKQzSOvlLyLbsJB3lsf9S6FlYWWhbRlthFTG7',
      'application/x-www-form-urlencoded',
	 
      function(token) {
        BeePlugin.create(token, beeConfig, function(beePluginInstance) {
          bee = beePluginInstance;
          request(
            'GET', 
                    //'https://rsrc.getbee.io/api/templates/m-bee',
                   '<?php echo base_url() ?>newsletter/autoresponder/fetchresponsive?id=<?php echo $campaign_id;?>',
                
            null,
				null,
				function(template) {
              bee.start(template);
              bee.toggleStructure();
            });
        });
      });
</script>
</html>