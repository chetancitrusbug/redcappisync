<?php
echo "here";exit;
/**
* A Custom class
*
* This class is for Custom listing
*
* @version 1.0
* @author Pravin Jha <pravinjha@gmail.com>
* @project Redcappi
*/

class Custom extends CI_Controller
{
	
	function __construct() {
        parent::__construct();
        $this->load->helper('cookie');

        # check via common model
        if (!$this->is_authorized->check_user())
            redirect('user/index');

        # Create user's folders
        $this->is_authorized->createUserFiles();

        $this->load->model('ConfigurationModel');
        $this->load->model('newsletter/Subscription_Model');
        $this->load->model('newsletter/Subscriber_Model');
        $this->load->model('newsletter/Emailreport_Model');
        $this->load->model('newsletter/contact_model');
        $this->load->model('Activity_Model');

        $this->load->library('upload'); // Load upload library class for uploading files
        $this->load->helper('notification');
        #Check if folder with modulo of User ID exists on server
        $user_dir = $this->session->userdata('member_id') % 1000;

        #Get absolute path for uploading
        $this->upload_path = $this->config->item('user_private') . $user_dir . '/' . $this->session->userdata('member_id');
        $this->output->enable_profiler(false);
        // Force SSL
        force_ssl();
    }
	
	function index(){
		 
		
			$memeber_id = $this->session->userdata('member_id')
            $subscribers =  $this->Subscriber_Model->get_subscriber(array('subscriber_created_by'=>$memeber_id));
			echo "<pre>";print_r($subscribers);exit;
	}
	
}