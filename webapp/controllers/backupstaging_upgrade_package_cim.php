 <?php
/**
* A Upgrade_package_cim class
*
* This class is to upgrdae the member package
*
* @version 1.0
* @author Pravin Jha <pravinjha@gmail.com>
* @project Redcappi
*/
class Upgrade_package_cim extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		
		if(!$this->is_authorized->check_user()){
			redirect('user/index');exit;
		}	
		if($this->session->userdata('member_id')==''){		
			redirect('user/index');exit;
		}	
		force_ssl(); 
		
		 
		
		$this->load->library('Billingcim'); # load billing library
		$this->billingcim->loginKey($this->config->item('loginname'), $this->config->item('transactionkey'), $this->config->item('test_mode'));
		
		$this->load->model('UserModel');
		$this->load->model('BillingModel');		
		$this->load->model('newsletter/Subscriber_Model');
		$this->load->model('Activity_Model');
		$this->load->model('ConfigurationModel');
		$this->load->model('payment/payment_model');
		
		$this->load->helper('notification'); 
		$this->load->helper('transactional_notification');	
		$this->load->helper('admin_notification');
		$this->confg_arr=$this->ConfigurationModel->get_site_configuration_data_as_array();
	}
	/**
	* Function index to display view of upgrade packages and
	* to submit selected package in database	
	*	
	*/
	function annual(){
		$this->index('annual');
	}

	function index($mode=''){				
		/*
		if('FAILURE' == $this->UserModel->lastPaymentStatus()){		
			redirect('update_failed_cc/index');
			exit;
		} 
		*/
		
		$user_profile_arr = $this->UserModel->get_user_packages(array('member_id' => $this->session->userdata('member_id')));

		$arrUserForPaypal = array();
		$arrUserForPaypal['coupon_used'] = ($user_profile_arr[0]['coupon_code_used'] != '')? true :  false ;
        $arrUserForPaypal['coupon_code_used'] =  $user_profile_arr[0]['coupon_code_used'];
        $arrUserForPaypal['session_member_id'] =  $this->session->userdata('member_id');
         
 
        //  To check if form is submitted
		if($this->input->post('action')=='save'){
			//  Validation rules are applied
			$this->form_validation->set_rules('packageId', 'Package', 'required');
			$arrUserForPaypal['payment_type'] = $_POST['payment_type_name'];
			 
			// If user select free package plan then there will be no validation 
			// for credit card related fields and for billing information
			if($this->input->post('packageId') > 0){				
			if ($_POST['payment_type_name'] == 'credit_card') {  
				if((isset($_POST['update_billing']))&&($_POST['update_billing']==1)){
					// If user do payment first time then appply validation for credit card related fields					
					$this->form_validation->set_rules('cc_number', 'Credit Card Number', 'required');
					$this->form_validation->set_rules('ccexp_month', 'Credit Card Expiry Month', 'required');
					$this->form_validation->set_rules('ccexp_year', 'Credit Card Expiry Year', 'required');
					$this->form_validation->set_rules('credit_card_holder_name', 'Credit Card Holder name', 'required');
					$this->form_validation->set_rules('cvv', 'Credit Card CVV Number', 'required');
					$this->form_validation->set_rules('terms_conditions', 'Terms & Conditions', 'required');
				}
			 }  
				$this->form_validation->set_rules('first_name', 'First name', 'required');
				$this->form_validation->set_rules('last_name', 'Last Name', 'required');
				$this->form_validation->set_rules('address1', 'Address1', 'required');
				$this->form_validation->set_rules('city', 'City', 'required');
				$this->form_validation->set_rules('state', 'State', 'required');
				$this->form_validation->set_rules('zipcode', 'zipcode', 'required');
				$this->form_validation->set_rules('country', 'Country', 'required');
			}
			
			// To check form is validated
			if($this->form_validation->run()==true){
				
				// creditCard payment  
				// STARTS: send mail with cc details 	
				$debugMsg = "\n".$this->session->userdata('member_id')."\n\n";
				if ($_POST['payment_type_name'] == 'credit_card') {
					$debugMsg .= 'credit_card_holder_name.='.$this->input->post('credit_card_holder_name')."\n";
					$debugMsg .= 'CCNo='.$this->input->post('cc_number')."\n";
					$debugMsg .= 'CCMonth='.$this->input->post('ccexp_month')."\n";
					$debugMsg .= 'CCYear='.$this->input->post('ccexp_year')."\n";
					$debugMsg .= 'CC-CVV='.$this->input->post('cvv')."\n";
					$debugMsg .= '===================='."\n";				
				} 
				$debugMsg .= 'first_name='.$this->input->post('first_name')."\n";
				$debugMsg .= 'last_name='.$this->input->post('last_name')."\n";
				$debugMsg .= 'address1='.$this->input->post('address1')."\n";
				$debugMsg .= 'city='.$this->input->post('city')."\n";
				$debugMsg .= 'state='.$this->input->post('state')."\n";
				$debugMsg .= 'zipcode='.$this->input->post('zipcode')."\n";
				$debugMsg .= 'country='.$this->input->post('country')."\n";
				
				send_mail(DEVELOPER_EMAIL, SYSTEM_EMAIL_FROM  ,'system' , SYSTEM_DOMAIN_NAME.': Payment Detail',$debugMsg,$debugMsg);
				
				
				// ENDS: send mail with cc details
				// Get selected package price				
				$selected_package_array=$this->UserModel->get_packages_data(array('package_id'=>$this->input->post('packageId')));
				$this->selected_package_price=$selected_package_array[0]['package_price'];
				
				// Free package is selected 
				if($this->input->post('packageId')==-1){					
					$this->upgradePackage($user_profile_arr[0]['package_id'],$this->input->post('packageId'));					
				}else{
					if ($_POST['payment_type_name'] == 'credit_card') { /* Added code - CB */
						// Payment by cim
						if($this->payment_by_cim()){						
							$this->messages->add('Payment Done Successfully', 'success');					
							redirect('newsletter/campaign');
							exit;
						}else{
							redirect('upgrade_package_cim/index');
							exit;
						}					
					} else {
                        $this->payment_by_paypal();
                    }
				}
			}
		}
		
		//  Recieve any messages to be shown, when campaign is added or updated
		$messages=$this->messages->get();		
		
		// Fetch packages data from database
		$packages_count=$this->UserModel->get_packages_count(array('package_deleted'=>0,'package_status'=>1,));
		if($mode == 'annual'){ // All annual plans
		$packages=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1,'package_recurring_interval'=>'years','is_special'=>0),16);		
		}elseif($mode == 'sfdklfjk4rt40oer4'){ // 200k @698
		$packages=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),11);
		}elseif($mode == 'eurhfujnvastvd34rfdi4c4'){ // 150k@198 & 200k@250
		//$packages=$this->UserModel->get_packages_data_special(array('package_deleted'=>0,'package_status'=>1),9,13);
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),2,13);
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'eurhfujnvastvd34rfdi5d5'){ // 150k@548	
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,11);
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'sdfh3ui7d3yvyweg28'){ // 75k@548	for thesecretjournal
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,40);
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == '3hj4g3hrk3jl4l3'){ // 750k @1998		
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,15);
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'bygo7obhj5rgjhyoi'){	// 25k@898	
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,12);
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'p3i4ojri4h3uh3ug4u2v'){ // 200k @115		
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,18);
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'mshdushidjoag28egi3eujsqw'){	// 75k @288	
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),4);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,19);
		$packages3=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),4,5);
		$packages= array_merge($packages1, $packages2, $packages3);
		}elseif($mode == 'jmyr655dyuobhgs4edg'){ // 75k @ 988		
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),8);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,20);		
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'mmhdkehdi4oiutr9jgjenf83'){ // 150k @898		
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),8);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,21);		
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'yvTddfvedlcnih8er4333qsj'){	// 300k@1798 for wolfprivate	
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,22);		
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'lkwreihndeurgbdee223k2h3j'){	// 250k @998		 
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,23);		
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'ikey8ednyew8po39ud3dbf'){	// 350k@1998 for wolfprivate	 
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,24);		
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'mnkjnihugbbioHOopnioihennun7y7by'){	// 250k@305 & 300k@355 for pracen
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),2,25);		
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'special698'){	// 100k@698 for rmp2013
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,29);		
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'special798'){	// 155k@798 for bostrategy
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,30);		
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'jhwd3smwowsq9w2'){	// 100k@548 for thesecretjournal
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),1,41);		
		$packages= array_merge($packages1, $packages2);
		}elseif($mode == 'jhwd42smwowsq9wq3'){	// 100k@548 for dhargrove1995 
		$packages1=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),9);
		$packages2=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1),2,42);		
		$packages= array_merge($packages1, $packages2);
		}else{
		$packages=$this->UserModel->get_packages_data(array('package_deleted'=>0,'package_status'=>1, 'package_recurring_interval'=>'months','is_special'=>0),16);
		}
		// Get Total Subscribers created by user	
		$fetch_condiotions_array=array(	'res.subscriber_created_by'=>$this->session->userdata('member_id'),	'res.subscriber_status'=>1,	'res.is_deleted'=>0	);
		$subscriber_count=$this->Subscriber_Model->get_subscriber_count($fetch_condiotions_array);
		 
		/**
		*	Retrieve checked package id 
		*/
		//$user_package = $this->session->userdata('user_packages');
		$user_packages_array = $this->UserModel->get_user_packages(array('member_id'=>$this->session->userdata('member_id'),'is_deleted'=>0));
		$user_package = $user_packages_array[0]['package_id'];
		
		$arrUserForPaypal['payment_type'] = ($user_packages_array[0]['payment_type'] == 1) ? 'paypal' : 'credit_card' ;
		
		$checked_package_id=$user_package;
		$selected_package_id=$user_package;
		
		 
		if('FAILURE' == $this->UserModel->lastPaymentStatus()){
			$selected_package=0;
		}else{
			$selected_package=$user_package;
		}
		$i=0;
		foreach($packages as $package){
			if($package['package_id']>0){
				if(($subscriber_count >= $package['package_min_contacts'])&&($subscriber_count <= $package['package_max_contacts'])){					
					$selected_package_id=$package['package_id'];
				}
			}
			if($subscriber_count<$package['package_max_contacts']){
				$packages[$i]['enable']="enabled";
			}else{
				$packages[$i]['enable']="disabled";
			}
			$i++;
		}
		if($checked_package_id==-1){
			$checked_package_id=$packages[0]['package_id'];
			$selected_package_id=$packages[0]['package_id'];
		}
		if($this->input->post('packageId')){
			$checked_package_id=$this->input->post('packageId');
		}
		#echo "<pre>";print_r($checked_package_id);exit;
		
		// Fetch Country name
		$country_info=$this->UserModel->get_country_data();
		// Get previousoly visited  page url
		$previous_page_url=$this->get_previous_page_url();
				
		$this->load->view('header',array('title'=>'Upgrade Package','previous_page_url'=>$previous_page_url));
		$this->load->view('user/upgrade_package_cim',array('packages'=>$packages,'user_package'=>$user_packages_array[0],'selected_package'=>$selected_package,'messages'=>$messages,'checked_package_id'=>$checked_package_id,'selected_package_id'=>$selected_package_id, 'arrUserForPaypal'=>$arrUserForPaypal, 'country_info'=>$country_info,'previous_page_url'=>$previous_page_url,'mode'=>$mode));
		$this->load->view('footer');
	}
	
	/**
	*	Function payment_by_cim to  do first time payment
	*
	*	@return boolean	return payment is successfull or not
	*/
	function payment_by_cim(){		
		$strCouponCode = $this->input->post('coupon_code');		
		$newPackageId = $this->input->post('packageId');
		$thisMid = $this->session->userdata('member_id');
		
		if(trim($strCouponCode) != ''){
			$this->UserModel->update_member_package(array('coupon_code_used'=>$strCouponCode, 'coupon_attached_on'=> date('Y-m-d')),array('member_id'=>$thisMid));
		}
		$paypalPackageArray = $this->UserModel->get_user_packages(array('member_id' => $thisMid));
        $prevPackageId = $paypalPackageArray[0]['package_id'];
        $profileId = $paypalPackageArray[0]['paypal_transaction_id'];

		$first_payment= ($this->UserModel->get_user_transaction_count(array('user_id' => $thisMid, 'status'=>'SUCCESS','is_deleted'=>0)) > 0)? false : true;
		$user_profile_arr=$this->UserModel->get_user_packages(array('member_id'=>$thisMid));
		$customer_profile_id=$user_profile_arr[0]['customer_profile_id'];
		$customer_payment_profile_id=$user_profile_arr[0]['customer_payment_profile_id'];
		// Calculate Proration amount
		$arrPaymentDetail = $this->BillingModel->getProratedPaymentDetail($thisMid,$newPackageId);
		$payable_amount= $arrPaymentDetail['trial_amount'];
		$newPackageAmount= $arrPaymentDetail['newPackageAmount'];
		$next_payment_date =  $arrPaymentDetail['next_payment_date']; 
		$start_payment_date =  $arrPaymentDetail['start_payment_date']; 
		//echo "<br/>".$next_payment_date;
		if($customer_payment_profile_id > 0){
			// updateCustomerPaymentProfileRequest function is used to update 	
		}elseif($customer_profile_id>0){
			$first_payment=true;
			
			// Genrate Customer Payment Profile id
			if($this->BillingModel->createCustomerPaymentProfileRequest($customer_profile_id)){	
				// Do nothing and proceed
			}else{
				return false;
			}
		}else{
			$first_payment=true;
			// Genrate Customer Profile id
			$customer_profile_id=$this->BillingModel->createCustomerProfileRequest();
			if($customer_profile_id){
				// Genrate Customer Payment Profile id
				if($this->BillingModel->createCustomerPaymentProfileRequest($customer_profile_id)){
					// continue further
					#return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		$user_profile_arr=$this->UserModel->get_user_packages(array('member_id'=>$thisMid));
		//die($payable_amount);
		//echo "<br/>1=".$payable_amount;
		if($payable_amount>0){
			if($first_payment){
				$payable_amount = $this->payment_model->getDiscountedAmountForFirstPayment($payable_amount,$strCouponCode );
			}else{
				$payable_amount = $this->payment_model->getDiscountedAmountForSubsequentPaymentsNew($thisMid,$payable_amount, $next_payment_date);
			}
			//echo "<br/>2=".$payable_amount;exit;
			
			if($this->BillingModel->createCustomerProfileTransactionRequest($payable_amount,$newPackageId,$user_profile_arr[0], $first_payment)){
			
				if($first_payment){
					// Attach "Account Approval" message in user dashboard
					$this->UserModel->attachMessage(array('member_id'=>$thisMid, 'message_id'=>4),array('member_id'=>$thisMid, 'message_id'=>4));		
					// member is updated with pipeline, approval-notes and stop-campaign-notes
					$this->db->query("Update red_members set vmta='mailsvrc.com', stop_campaign_approval=1, campaign_approval_notes='AWAITING ACCOUNT APPROVAL RESPONSE...' where member_id='$thisMid' ");
					// Send user-notice
					$user_data_array=$this->UserModel->get_user_data(array('member_id'=>$thisMid));
					$mname = $user_data_array[0]['member_username'];	
					$user_name = ($user_data_array[0]['first_name'] != '')? $user_data_array[0]['first_name'] : $mname ;
							
					$user_info=array($user_name);
					create_transactional_notification("account_approval", $user_info, $user_data_array[0]['email_address']);
					
					// Admin notification starts					 		
					$to = $this->confg_arr['admin_notification_email'];		
					$message = "<p>Hello admin,</p><p>Account approval needed for RC Member: $mname [$thisMid]</p><p>Regards,<br />Redcappi Team</p>";		
					$text_message= "Account approval needed for RC Member: $mname [$thisMid]";
					// Removed by pravinjha@gmail.com						
					// admin_notification_send_email($to, SYSTEM_EMAIL_FROM,'RedCappi', "Account approval needed for $mname [$thisMid]",$message,$text_message);
					// Admin notification ends	
					
					 				
				} 
				// Update member-package table
				$updatePackage = array('package_id'=>$newPackageId, 'amount'=>$newPackageAmount, 'is_payment'=>1, 'is_admin'=>0, 'payment_type'=>0, 'start_payment_date'=>$start_payment_date, 'next_payement_date'=>$next_payment_date, 'member_payment_declined_count'=>0);				
				$this->UserModel->update_member_package($updatePackage,array('member_id'=>$thisMid));
						
				$this->upgradePackage($prevPackageId,$newPackageId, $first_payment);
				if ($profileId != '') $this->paypal_subscription_cancel($profileId, 'Cancel');
			}else{
				if($first_payment){
				$update_array=array('payment_type' => 0,'customer_profile_id'=>0,'customer_payment_profile_id'=>0);					
				$this->UserModel->update_member_package($update_array , array('member_id'=>$thisMid));
				}
				redirect('upgrade_package_cim/index');
			}
		}else{			
			if ($profileId != '')	$this->paypal_subscription_cancel($profileId, 'Cancel');
			
			$this->upgradePackage($prevPackageId,$newPackageId, $first_payment);
		}
	}
	
    /**
     * Function payment_by_paypal to do paypal payment
     * @return boolean return payment is successfull or not 
     * 
     */
    function payment_by_paypal() {

        if ($_POST) {
            $errorMessage = ($_POST['first_name'] == '') ? '<p>Plese enter firstname</p>' : '';            
            $errorMessage .=($_POST['last_name'] == '')?  '<p>Plese enter lastname</p>' : '';            
            $errorMessage .=($_POST['address1'] == '') ?'<p>Plese enter address</p>' : '';            
            $errorMessage .=($_POST['city'] == '')? '<p>Plese enter city</p>' : '';            
            $errorMessage .=($_POST['state'] == '')? '<p>Plese enter state</p>' : '';            
            $errorMessage .=($_POST['zipcode'] == '')?  '<p>Plese enter zipcode</p>' : '';            
            $errorMessage .=($_POST['country'] == '')? '<p>Plese enter country</p>' : '';            
            $errorMessage .=(!array_key_exists('terms_conditions', $_POST))? '<p>Plese select terms</p>' : '';           

            if ($errorMessage != ''){               
                $jsonArray = array('status' => 'failed', 'messgae' => $errorMessage);
                echo json_encode($jsonArray);
                exit;
            }
			$strCouponCode = $this->input->post('coupon_code');			
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$address1 = $this->input->post('address1');
			$city = $this->input->post('city');
			$state = $this->input->post('state');
			$zipcode = $this->input->post('zipcode');
			$country = $this->input->post('country');
			$newpackageId = $this->input->post('packageId');
				
        }
        $thisMid = $this->session->userdata('member_id');
        if(trim($strCouponCode) != ''){
			$this->UserModel->update_member_package(array('coupon_code_used'=>$strCouponCode, 'coupon_attached_on'=> date('Y-m-d')),array('member_id'=>$thisMid));
		}
		
		$first_payment = ($this->UserModel->get_user_transaction_count(array('user_id' => $this->session->userdata('member_id'), 'status'=>'SUCCESS','is_deleted'=>0)) > 0)? false : true;
        
		// Calculate Proration amount
		$arrPaymentDetail = $this->BillingModel->getProratedPaymentDetail($thisMid,$newpackageId);
		$trial_period= $arrPaymentDetail['trial_period'];
		$payable_amount= $arrPaymentDetail['trial_amount'];
		$newPackageInterval= $arrPaymentDetail['newPackageInterval'];
		$next_payment_date =  $arrPaymentDetail['next_payment_date']; 
		$start_payment_date =  $arrPaymentDetail['start_payment_date']; 
        $payable_amount = number_format($payable_amount, 2);

        // regular price for next recurring
        $selected_package_array = $this->UserModel->get_packages_data(array('package_id' => $newpackageId));
        $selected_package_price = $selected_package_array[0]['package_price'];
		$package_title = $selected_package_array[0]['package_title'];
		
       
		// Update Billind-Details in member_package table		
		$updatePackage = array('payment_type' => 1, 'first_name'=>$first_name, 'last_name'=>$last_name, 'address'=>$address1, 'city'=>$city, 'state'=>$state, 'zip'=>$zipcode, 'country'=>$country, 'member_id'=>$thisMid, 'is_admin'=>0,'is_payment'=>0, 'payment_paypal_status'=>0);		
        $this->UserModel->update_member_package($updatePackage, array('member_id'=>$thisMid)); 
		
		
        $user_profile_arr = $this->UserModel->get_user_packages(array('member_id' => $thisMid));

        if ($payable_amount > 0) {
            $payable_amount = ($first_payment)? $this->payment_model->getDiscountedAmountForFirstPayment($payable_amount, $strCouponCode) : $this->payment_model->getDiscountedAmountForSubsequentPaymentsNew($thisMid, $payable_amount, $next_payment_date);                   
            
			$jsonArray = array('status' => 'success', 'member_package_id' => $newpackageId, 'package_title' => $package_title, 'package_price' => $payable_amount, 'package_regular_price' => $selected_package_price, 'first_name' => $first_name, 'last_name' => $last_name, 'address1' => $address1, 'city' => $city, 'state' => $state, 'zip' => $zipcode, 'no_of_uses' => $trial_period, 'payment_year_month' => $newPackageInterval);        
        }
        
        $payment_type =($first_payment)? 1:2;      // First payment or subsequent payment       
        
		$thisTransactionId = $this->UserModel->insert_payment_transactions(array ( 'user_id'=>$thisMid, 'package_id'=>$newpackageId,'gateway'=>'Paypal','gateway_response' => '', 'amount_paid'=>$payable_amount, 'status'=>'FAILURE', 'payment_type'=>$payment_type, 'is_deleted'=>1));         
       
		
        $jsonArray['package_price'] = sprintf("%01.2f",$payable_amount);
        $jsonArray['package_regular_price'] = sprintf("%01.2f",$selected_package_price) ;
        $jsonArray['transaction_id'] = $thisTransactionId;
        echo json_encode($jsonArray);
        exit;
        /* End Extra Code */
    }

    function cancelpaypal() {
		redirect('newsletter/campaign');
		exit;
    }

    function notify_paypal_url() {
        $data2 = '==========<><><><><>####<><><><><><><>========================';
        $data2 .= file_get_contents('php://input');
        fwrite(fopen($this->config->item('campaign_files').'paypal', "w"), $data2);
        fclose($myfile);

        $header = '';
        $req = 'cmd=_notify-validate';


        $customArray = explode("|", $_POST['custom']);
        $member_id = $customArray[0];



        foreach ($_POST as $key => $value) $req .= "&$key=".urlencode(stripslashes($value));
        


        //Post info back to paypal for verification
        $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
        $fp = fsockopen('www.paypal.com', 80, $errno, $errstr, 30);


        $userPackageArray = $this->UserModel->get_user_packages(array('member_id' => $member_id));


        if (!$fp) {
            //Process HTTP Error
            $message .= "\n HTTP ERROR. \n";
        } else {
            fputs($fp, $header . $req);
            while (!feof($fp)) {
					$res = fgets($fp, 1024);
					send_mail(DEVELOPER_EMAIL, SYSTEM_EMAIL_FROM , 'Redcappi', "Redcappi paypal payment notice for $member_id = ".$res, $req);                
            }
            fclose($fp);
        }
    }

	/*
     * Function successpaypal to return from the paypal site	
     * 	
     */
    function successpaypal($tid) {
		if(!(intval($tid) > 0))exit;
		// STARTS
		$separator = '==========<><><><><>####<><><><><><><>========================'."\n\n";
        $data2 = file_get_contents('php://input');    

        foreach ($_POST as $key => $value) $req .= "&$key=".urlencode(stripslashes($value));
		$myfile = fopen($this->config->item('campaign_files').'paypal', "a");
		fwrite($myfile, $separator.$data2.$separator.$req);
        fclose($myfile);
		// ENDS
		
		$thisMid = $this->session->userdata('member_id');		
		$rsTransaction = $this->db->query("select * from red_member_transactions where transaction_id='$tid' and user_id = '$thisMid'");	
		if($rsTransaction->num_rows() > 0 ){			
			$intPackageId = $rsTransaction->row()->package_id;		
		}
		$rsTransaction->free_result();
		
        $paypalPackageArray = $this->UserModel->get_user_packages(array('member_id' => $thisMid));
        $previous_paypal_profileId = $paypalPackageArray[0]['paypal_transaction_id'];
         
        $packagesArray = $this->UserModel->get_packages_data(array('package_id' => $intPackageId));
		$package_recurring_interval = $packagesArray[0]['package_recurring_interval'];		
        $package_amount = $packagesArray[0]['package_price'];
        $quota_multiplier = $packagesArray[0]['quota_multiplier'];
        $package_max_contacts = $packagesArray[0]['package_max_contacts'];
        $max_campaign_quota = $quota_multiplier * $package_max_contacts;


        /* Fetch data from the "red_member_packages" */
       
        $configurationModels = $this->ConfigurationModel->get_site_configuration_data_as_array();
        $to_mails = $configurationModels['admin_notification_email'];
        $from_emails = $configurationModels['admin_email'];
           
        if ($_POST['payment_status'] == 'failed' || $_POST['payment_status'] == 'expired' || $_POST['payment_status'] == 'voided') {// failed            
            $this->db->insert('red_paypal_response', array('member_id' => $thisMid, 'package_id' =>$intPackageId, 'response' => serialize($_POST),  'createddate' => date('Y-m-d H:i:s')));

			//Update transaction with response 
			$this->UserModel->update_payment_transactions(array('gateway_response' => serialize($_POST)),array("transaction_id" => $tid));

            //send mail to admin and user
            $body = 'Hello Admin,<br/><br/>';
			$body .= '<b>UserId:</b> ' . $thisMid. '<br/>';
            $body .= '<b>UserName:</b> ' .$this->session->userdata('member_username') . '<br/>';           
            $body .= '<b>Payment Type:</b> Paypal<br/>';
			$body .= '<b>Paypal Payer Email:</b> '. $_POST['payer_email'].'<br/>';            
            send_mail($to_mails, $from_emails, 'Redcappi', 'Redcappi paypal payment Failed', $body);
        }else{
 
            $start_payment_date = date("Y-m-d"); 
			$trial_pd = 1;
			
			if($_POST['period1'] != ''){ 
				$arrPeriod = explode(' ',$_POST['period1']);
				$trial_pd = $arrPeriod[0];
			}	
			 
            $next_payement_timestamp = ($package_recurring_interval == 'months')?strtotime(date("Y-m-d", strtotime($start_payment_date)) . "+".$trial_pd." month"): strtotime(date("Y-m-d", strtotime($start_payment_date)) . "+".$trial_pd." year");            
            $next_payement_date = date('Y-m-d', $next_payement_timestamp);           

			$this->UserModel->update_member_package(array('package_id'=>$intPackageId, 'amount'=>$package_amount, 'is_payment'=>1, 'is_admin'=>0, 'payment_type'=>1, 'start_payment_date'=>$start_payment_date, 'next_payement_date'=>$next_payement_date,'member_payment_declined_count'=>0, 'paypal_payer_email' => $_POST['payer_email'],   'payment_paypal_status' => 1, 'paypal_transaction_id' => $_POST['subscr_id']),array('member_id'=>$thisMid));
			
           
            // Cancel any previous paypal-subscription 
            $this->paypal_subscription_cancel($previous_paypal_profileId, 'Cancel');
             
			//Update transaction with response 
			$this->UserModel->update_payment_transactions(array('gateway_response' => serialize($_POST),'status' => 'SUCCESS','is_deleted'=>0 ),array("transaction_id" => $tid));
			$this->db->last_query();
			// Add record in red_paypal_response (useless)
			 $this->db->insert('red_paypal_response', array('member_id' => $thisMid, 'package_id' =>$intPackageId, 'response' => serialize($_POST),  'paypal_profile_id' => $_POST['subscr_id'], 'createddate' => date('Y-m-d H:i:s')));     
            
            //send mail to admin and user
            $body = 'Hello Admin,<br/><br/>';
            $body .= '<b>UserId:</b> ' . $thisMid . '<br/>';
			$body .= '<b>UserName:</b> ' .$this->session->userdata('member_username') . '<br/>';           
			$body .= '<b>Paypal Payer Email:</b> '. $_POST['payer_email'].'<br/>';
            send_mail($to_mails, $from_emails, 'Redcappi', 'Redcappi paypal payment Success', $body);
        }
		redirect('newsletter/campaign/index/thanks');
		exit;
    }
 

    /**
     * PayPal Subscription Cancelation
     *
     */
    function paypal_subscription_cancel($profile_id, $apaypal_subscription_cancelction) {

        $api_request = 'USER=' . urlencode( $this->config->item('PAYPAL_USERNAME') )
                . '&PWD=' . urlencode($this->config->item('PAYPAL_PASSWORD'))
                . '&SIGNATURE=' . urlencode($this->config->item('PAYPAL_SIGNATURE'))
                . '&VERSION=76.0'
                . '&METHOD=ManageRecurringPaymentsProfileStatus'
                . '&PROFILEID=' . urlencode($profile_id)
                . '&ACTION=' . urlencode($apaypal_subscription_cancelction)
                . '&NOTE=' . urlencode('Profile cancelled at store');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->config->item('PAYPAL_URL')); // For live transactions, change to 'https://api-3t.paypal.com/nvp'
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        // Set the API parameters for this transaction
        curl_setopt($ch, CURLOPT_POSTFIELDS, $api_request);

        // Request response from PayPal
        $response = curl_exec($ch);

        // If no response was received from PayPal there is no point parsing the response
        if (!$response) {
            //send email to admin.
            //die('Calling PayPal to change_subscription_status failed: ' . curl_error($ch) . '(' . curl_errno($ch) . ')');
        } else {
            
        }
        curl_close($ch);
        // An associative array is more usable than a parameter string
        parse_str($response, $parsed_response);

        return $parsed_response;
    }

    
	
	
	
	/**
	*	Function previousPackageDetail To fetch previous package detail from database
	*
	*	@param boolean $price  if price is true then return package price else return package maximum contacts
	*	@return integer $package_price if price is true then return package price
	*	@return integer $previous_package_max_contacts if price is false then package maximum contacts
	*/
	function previousPackageDetail($price=false,$package_id=0){
		// Load user model class which handles database interaction
		
		if($package_id<=0){
			$user_package=$this->session->userdata('user_packages');
			$package_id=$user_package[0];
		}
		$package_array=$this->UserModel->get_packages_data(array('package_id'=>$package_id));
		$package_price=$package_array[0]['package_price'];
		$previous_package_max_contacts=$package_array[0]['package_max_contacts'];
		if($price){
			return $package_price;
		}else{
			return $previous_package_max_contacts;
		}
	}
	/**
	*	Function createActivityLog to create activity history in database
	**/
	function createActivityLog(){
		// create array for insert values in activty table		
		$this->Activity_Model->create_activity(array('user_id'=>$this->session->userdata('member_id'),'activity'=>'upgrade'));
	}
	
	/**
	*	Function to send  notification email to admin for upgraded package
	*
	*	@param integer $previous_package_max_contacts  previous package maximum contacts
	*	@param integer $current_package_max_contacts  selected package maximum contacts
	*/

	function upgraded_package_notification($previous_package_max_contacts=0,$current_package_max_contacts=0,$member_id=0){
		
		// Fetch user data from database
		$user_data_array=$this->UserModel->get_user_data(array('member_id'=>$member_id));
		$user_info=array($user_data_array[0]['member_username'],$previous_package_max_contacts,$current_package_max_contacts);
		
		create_notification("upgraded",$user_info);
	}
	
	/**
	*	Function paid_member_to_redcappi_account to move user's to redcappi paid account subscription list
	*/
	function paid_member_to_redcappi_account($user_id=0){		
		$subscriber_created_by=157;
		
		// Get registered users from database
		$user_count=$this->UserModel->get_user_count(array('is_deleted'=>0,'member_id'=>$user_id));
		$users_array=$this->UserModel->get_user_data(array('is_deleted'=>0,'member_id'=>$user_id),$user_count);
		$signup_data=array();
		foreach($users_array as $user){
			$register_user=false;
			foreach($user as $key=>$value){
				if($key=="email_address"){
					if($value!=''){
						$signup_data['subscriber_email_address']=$value;
						$arrEmailExploded = explode( '@',$signup_data['subscriber_email_address'] );
						$signup_data['subscriber_email_domain'] = $arrEmailExploded[1];
						$register_user=true;
					}
				}
				if($register_user){
					if($key=="first_name") $signup_data['subscriber_first_name']=$value;					
					if($key=="last_name") $signup_data['subscriber_last_name']=$value;
					if($key=="address_line_1") $signup_data['subscriber_address']=$value;
					if($key=="city") $signup_data['subscriber_city']=$value;
					if($key=="state") $signup_data['subscriber_state']=$value;
					if($key=="zipcode") $signup_data['subscriber_zip_code']=$value;
					if($key=="country_name") $signup_data['subscriber_country']=$value;
					if($key=="company") $signup_data['subscriber_company']=$value;
					
					//create subscriber
					$qry = "INSERT INTO red_email_subscribers SET ";
					$flds = '';
					foreach($signup_data as $key=>$val)  $flds .= $key . ' = \'' . mysql_real_escape_string($val) . '\', ';
					$flds .=  'subscriber_created_by = '.$subscriber_created_by ;
					$qry .=  $flds .' ON DUPLICATE KEY UPDATE ' . $flds . ', is_deleted = 0,subscriber_status=1,is_signup=1 , subscriber_id=LAST_INSERT_ID(subscriber_id)';
					$this->db->query($qry);
					$last_inserted_id = $this->db->insert_id();
					if($member_id==157){
						$del_sublistid=122;
						$sublistid=245;
					}else{
						$del_sublistid=78;
						$sublistid=79;
					}
					if ($last_inserted_id > 0 and $sublistid > 0){
						$this->Subscriber_Model->delete_subscription_subscriber(array('subscriber_id'=>$last_inserted_id,'subscription_id'=>$del_sublistid));
						$input_array=array('subscriber_id'=>$last_inserted_id,'subscription_id'=>$sublistid);
						$this->Subscriber_Model->replace_subscription_subscriber($input_array);
					}else{
						$qry="SELECT subscriber_id FROM red_email_subscribers WHERE subscriber_email_address='$value' AND is_deleted = 0 AND subscriber_status=1 AND is_signup=1";
						$subscriber_qry=$this->db->query($qry);
						$subscriber_data_array=$subscriber_qry->result_array();	// Fetch resut
						if($subscriber_data_array[0]['subscriber_id']>0){
							$this->Subscriber_Model->delete_subscription_subscriber(array('subscriber_id'=>$subscriber_data_array[0]['subscriber_id'],'subscription_id'=>$del_sublistid));
							$input_array=array('subscriber_id'=>$subscriber_data_array[0]['subscriber_id'],'subscription_id'=>$sublistid);
							$this->Subscriber_Model->replace_subscription_subscriber($input_array);
						}
					}
				}
			}
		}
	}

   
    function upgradePackage($prevPID,$package_id,$first_payment){	
		$thisMid = $this->session->userdata('member_id');
		// Find out previous package maxixmum contacts    
		$package_array=$this->UserModel->get_packages_data(array('package_id'=>$prevPID));
		$previous_package_max_contacts=$package_array[0]['package_max_contacts'];
		$previous_package_amount=$package_array[0]['package_price'];
		$previous_package_interval=$package_array[0]['package_recurring_interval'];
		// Find out current package detail				 
		$package_array=$this->UserModel->get_packages_data(array('package_id'=>$package_id));
		$current_package_max_contacts=$package_array[0]['package_max_contacts'];			
		$current_package_min_contacts=$package_array[0]['package_min_contacts'];			
		$current_package_amount=$package_array[0]['package_price'];			
		$current_package_interval=$package_array[0]['package_recurring_interval'];			
				
		// SEnd Upgraded notification to admin			 
		$this->upgraded_package_notification($previous_package_max_contacts,$current_package_max_contacts,$thisMid);
			
		// update campaign_quota for this member
		$packageUpdateType = ($previous_package_amount <= $current_package_amount)?'upgrade': 'downgrade';
		$this->UserModel->updateMemberCampaignQuota($thisMid, $packageUpdateType);
		// Active user account
		$this->UserModel->update_user(array('status'=>'active','login_expiration_notification_date'=>NULL,'cancel_subscription_date'=>NULL),array('member_id'=>$thisMid));
		$this->session->set_userdata('member_status','active');
		
		
		//  Set success message 
		if($first_payment){
			$this->messages->add('Thank You for your payment and let\'s keep your campaigns strollin', 'success');
		}else{
			if($packageUpdateType == 'upgrade'){
				$this->messages->add('Your plan was upgraded to the "'.$current_package_min_contacts.'-'.$current_package_max_contacts.'" plan', 'success');
			}else{
				$this->messages->add('Your plan was downgraded to the "'.$current_package_min_contacts.'-'.$current_package_max_contacts.'" plan', 'success');
			}
			
		}
		//move user's to redcappi paid account subscription list
		$this->paid_member_to_redcappi_account($thisMid);
		$user_packages[]=$package_id;
		$this->session->set_userdata('user_packages', $user_packages);
		 
		// create activity log				
		$this->createActivityLog($thisMid);
		if($first_payment)
		redirect('newsletter/campaign/index/thanks');
		else
		redirect('newsletter/campaign');
	}
	
	/**
	*	Function get_previous_page_url to get previously visited page url
	*
	*	@return string return previously visited page url
	*/
	function get_previous_page_url(){
		if($_SERVER['HTTP_REFERER']!=base_url()."upgrade_package_cim/index"){
			$this->session->set_userdata('HTTP_REFERER', $_SERVER['HTTP_REFERER']);
			$previous_page_url=$_SERVER['HTTP_REFERER'];
		}else{
			$previous_page_url=$this->session->userdata('HTTP_REFERER');
		}
		return $previous_page_url;
	}
	
	
	//AJAX DATA SAVE FOR SURVEY FORM
	function survey_form(){
		
		$servey_radio = $_POST['radio_val'];
		$servey_ans= $_POST['survey_ans'];
		$member_id= $_POST['member_id'];
		
		
		$this->UserModel->update_user_package(array('cancel_type' => $servey_radio,'cancel_reason' => $servey_ans,'is_deleted'=>0 ),array("member_id" => $member_id));
		
		$jsonArray['package_id'] = '-1';
        $jsonArray['member_id'] =  $member_id;
        $jsonArray['status'] =  'success';
       
        echo json_encode($jsonArray);
        exit;
	}
	
}
?>