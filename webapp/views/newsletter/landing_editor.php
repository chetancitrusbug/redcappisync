<!DOCTYPE html>
<html lang="en">
  <head>
   <title>Redcappi - Landing Page Editor</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('webappassets');?>css/font-awesome.css?v=6-20-13" media="screen" />
   <style type="text/css">
      * {
        margin:0px;
        padding:0px;
      }
      body {
        overflow:hidden;
        background-color: rgb(238,238,238);
        color:#000000;
      }
      #bee-plugin-container {
        position: absolute;
        top:80px;
        bottom:30px;
        left:450px;
        right:500px;
		margin: 0 auto;
      }
      #integrator-bottom-bar {
        position: absolute;
        height: 25px;
        bottom:0px;
        left:5px;
        right:0px;
      }
	    /*====================== aksha=============*/
            #diyy-header {
                text-align: center;
                min-width: 1000px;
                box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.05);
                border-bottom: 1px solid #ddd;
                background-image: linear-gradient(to bottom, #ffffff, #e8e8e8);
                background-repeat: repeat-x;
                background-color: #e8e8e8;
                border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
                height: 40px;
                position: relative;
                z-index: 10;
            }

            #diyy-header #header-logo {
                float: left;
            }
            #diyy-header {
                text-align: center;
            }
            #diyy-header #header-logo img {
                padding: 4px 0 0 13px;
                height: 32px;
            }
            #diyy-header form {
                position: absolute;
                left: 120px;
                right: 165px;
                top: 5px;
            }
            #diyy-header label {
                font-size: 15px;
                text-shadow: 0 1px 1px #fff;
                color: #333;
                font-family: Arial, Helvetica, sans-serif !important;
            }
            #diyy-header input {
                background-color: #fff;
                border: 1px solid #ccc;
                transition: border linear 0.2s, box-shadow linear 0.2s;
                border-radius: 4px;
                height: 26px;
                margin: 0 0 0 5px;
                padding: 1px 8px;
                width: 230px;
                font-size: 14px;
            }
            #campaign_title {
                line-height: 24px;
            }
            #diyy-header .btn.add {
                padding: 3px 19px 4px;
                margin: 0;
            }
            #diy-header .btn {
                display: inline-block;
                margin: 5px 4px 0;
                padding: 3px 13px 4px;
                font-size: 14px;
            }
            .btn.add {
                color: #fff;
                text-shadow: 0 1px 0 rgba(0, 0, 0, 0.65);
                background-image: linear-gradient(to bottom, #ff3019, #cf0404);
                background-repeat: repeat-x;
                background-color: #cf0404;
                border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            }

            #diyy-header .btn i {
                line-height: 20px;
				margin-right: 25px;
            }

            #diyy-header .btn.add {
                padding: 3px 19px 4px;
                margin: 0;
                border-radius: 5px !important;
                border: 1px solid #cf0404  !important;
            }

            #diyy-header .btn.cancel {
                float: right;
                margin: 10px 0 0;
                margin-right: 0px;
                border-radius: 0;
                border: 0;
                background-image: none;
                background-color: transparent !important;
                font-size: 20px;
                box-shadow: none;
                color: #666;
            }
             .btn {
                font-family: Arial, Helvetica, sans-serif !important;
                text-decoration:none !important ;
                vertical-align: middle !important;
                line-height: 20px !important;
                text-align: center !important;
                cursor: pointer !important;

                box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05) !important;

            } 
            .icon-remove::before {
                content: "\f00d";
            }
            .icon-question::before {
                content: "\f128";
            }
            .icon-eye-open::before {
                content: "\f06e";
            }
            [class^="icon-"], [class*=" icon-"] {
                font-family: FontAwesome;
                font-weight: normal;
                font-style: normal;
                text-decoration: inherit;
            }
			#diyy-header input.add.save_campaign_changes {
				margin-top: -5px;
				width: 110px;
			}
			#bee-plugin-container {
				position: absolute;
				top: 80px;
				bottom: 30px;
				left: 20%;
				right: 25%;
				margin: 0 auto;
				width: 50%;
			}
			@media only screen and (max-width: 1700px) { 
				#bee-plugin-container {
					left: 4%;
				}
			}
			@media only screen and (max-width: 1366px) { 
				#bee-plugin-container {
					left: -12%;
					width: 40%;
				}
			}
			
			#fancybox-content{
				font-family: "Frutiger Next W01",Helvetica,Arial,sans-serif;
			}
			
			



            /*====================== aksha=============*/
			
	.custum-template{
		background: #fff none repeat scroll 0 0;
		margin-top: 43px;
		padding: 10px 21px;
		width: 9%;
	}
	.custum-template .new_temp {margin: 10px;}
    </style>
 </head>
  <body>
	<div id="diyy-header">
            <div id="header-logo">
                <a href="https://www.redcappi.us/newsletter/campaign">
                    <img src="https://www.redcappi.us/webappassets/images/redcappi-baby-editor.png?v=6-20-13" alt="" border="0">
                </a>
            </div>
            <form action="" method="post">
                <label class="label">Landing Title</label>
                <input type="text" name="lan_title" class="lan_title" value="<?php echo $lan_title; ?>" onchange="javascript:pagechange = true;">

             
				<!--<input type="button" name="next_step" class="btn add save_campaign_changes next" value="Next Step" id='next_step'/>-->
				<input type='hidden' data-info="" class="redirectlink" value="">
			</form>
			
			
            <a href="<?php echo base_url() ?>newsletter/campaign" title="Close" class="btn cancel inline-block remove-icon"><i class="icon-remove"></i></a>
            <a href="<?php echo base_url() ?>support/index" target="_blank" title="Help" class="btn cancel inline-block"><i class="icon-question"></i></a>
			
           <!-- <a class="btn cancel inline-block preview_alert" title="Preview" href="" id="preview_alert"><!--<i class="icon-eye-open"></i> -->
				<button  name="preview_alert" class="btn cancel inline-block preview" value="Preview" id='preview_alert'> <i class="icon-eye-open"></i> </button>
				<button  name="Save" class="save_landing btn cancel" value="Save" id='save_landing'> <i class="icon-save"></i> </button>
				
			<!--</a>-->
		<!--	<a class="save_campaign_changes btn cancel save" title="Save"><i class="icon-save"></i></a>	
-->
<?php $jsonSignup = json_encode($signup); ?>

   </div>
    <div id="bee-plugin-container"></div>
    <div id="integrator-bottom-bar">
      <input id="choose-template" type="hidden" />
    </div>

  </body>
  
	<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/Blob.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/fileSaver.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/jquery-ui-1.10.2.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/beefree/BeePlugin.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>css/inner_red.css?v=6-20-13"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>css/diy.css?v=6-20-13"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('webappassets');?>js/jquery-1.4.4.min.js?v=6-20-13"></script>
	
	<script type="text/javascript">
  
  

    var request = function(method, url, data, type, callback) {
      var req = new XMLHttpRequest();
	 
      req.onreadystatechange = function() {
        if (req.readyState === 4 && req.status === 200) {
          var response = JSON.parse(req.responseText);
          callback(response);
        }
      };

      req.open(method, url, true);
      if (data && type) {
        if(type === 'multipart/form-data') {
          var formData = new FormData();
          for (var key in data) {
            formData.append(key, data[key]);
          }
          data = formData;          
        }
        else {
          req.setRequestHeader('Content-type', type);
        }
      }

      req.send(data);
    };

    var save = function(filename, content) {
      saveAs(
        new Blob([content], {type: 'text/plain;charset=utf-8'}), 
        filename
      ); 
    };

    var specialLinks = [{
        type: 'unsubscribe',
        label: 'Unsubscribe',
        link: 'http://[unsubscribe]/'
    }, {
        type: 'subscribe',
        label: 'Subscribe',
        link: 'http://[subscribe]/'
    }];

    var mergeTags = [{
      name: 'Jui',
      value: '[Jui]'
    }, {
      name: 'Sahaj',
      value: '[Sahaj]'
    }];
	
	
	

    var mergeContents = [];
	
	var glob = {};
	var jsonSignup = <?php echo $jsonSignup;?>;
	if(jsonSignup){
	 jQuery.each( jsonSignup, function( key, value ) {
			 var jsonkey = key;
			 var jsonvalue = value;
			 var jsonval = 'value';
			 glob={
				name :  jsonvalue,
				value : '{'+jsonkey+'}'
			 }
			  mergeContents.push(glob);
		}); 
	}

    var beeConfig = {  
      uid: 'ca2ef0e3-cf4b-4e79-84e1-eda0a560051a',
      container: 'bee-plugin-container',
      autosave: 15, 
      language: 'en-US',
      specialLinks: specialLinks,
      mergeTags: mergeTags,
      mergeContents: mergeContents,
      onSave: function(jsonFile, htmlFile) { 
		var title = jQuery('.lan_title').val(); 
		var postdata = {};
		postdata['json'] = jsonFile;
		postdata['html'] = htmlFile;
        postdata['lan_id'] = <?php echo $lan_id;?>;
		postdata['lan_title'] = title;
        jQuery.post('<?php echo base_url() ?>newsletter/landing/save/', postdata, function (result) {
			var response = jQuery.parseJSON(result);
            if(response.status = 'success'){
				window.open('<?php echo base_url() ?>newsletter/landing/view/<?php echo $lan_id;?>');
			} 
		 });
		
      },
      onSaveAsTemplate: function(jsonFile) { // + thumbnail? 
      //  save('newsletter-template.json', jsonFile);
		
			
		
      },
      onAutoSave: function(jsonFile) { // + thumbnail? 
        console.log(new Date().toISOString() + ' autosaving...');
        window.localStorage.setItem('newsletter.autosave', jsonFile);
      },
      onSend: function(htmlFile) {
        //write your send test function here
      },
      onError: function(errorMessage) { 
        console.log('onError ', errorMessage);
      }
    };

    var bee = null;

    var loadTemplate = function(e) {
      var templateFile = e.target.files[0];
	 
      var reader = new FileReader();

      reader.onload = function() {
        var templateString = reader.result;
        var template = JSON.parse(templateString);
        bee.load(template);
      };

      reader.readAsText(templateFile);
    };
 
    document.getElementById('choose-template').addEventListener('change', loadTemplate, false);

    request(
      'POST', 
      'https://auth.getbee.io/apiauth',
      'grant_type=password&client_id=ca2ef0e3-cf4b-4e79-84e1-eda0a560051a&client_secret=8Gla59jBi5IYY2KKQzSOvlLyLbsJB3lsf9S6FlYWWhbRlthFTG7',
      'application/x-www-form-urlencoded',
	 
      function(token) {
        BeePlugin.create(token, beeConfig, function(beePluginInstance) {
          bee = beePluginInstance;
          request(
            'GET', 
			//'https://rsrc.getbee.io/api/templates/m-bee',
			'<?php echo base_url() ?>newsletter/landing/landing_editor/<?php echo $lan_id;?>',
            null,
				null,
				function(template) {
              bee.start(template);
            });
        });
      });
	  
	   jQuery('.save_landing').click(function () {
			bee.save();
		});
		
		jQuery("#preview_alert").live('click',function(event){
			bee.save();			
		});

  </script>
</html>
