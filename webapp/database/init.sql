-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: 192.168.0.1:3306
-- Generation Time: Jul 28, 2016 at 10:34 AM
-- Server version: 5.6.30-76.3
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `redcappi`
--

-- --------------------------------------------------------

--
-- Table structure for table `red_abusive_words`
--

DROP TABLE IF EXISTS `red_abusive_words`;
CREATE TABLE IF NOT EXISTS `red_abusive_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abusive_word` varchar(255) NOT NULL,
  `is_status` int(1) NOT NULL DEFAULT '1',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_activity_log`
--

DROP TABLE IF EXISTS `red_activity_log`;
CREATE TABLE IF NOT EXISTS `red_activity_log` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activity` varchar(255) NOT NULL COMMENT '''login'',''logout'',''upgrade'',''update_credit_card'',''contact_add'',''contact_delete'',''contact_tested'',''do_not_mail_list'',''campaign_created'',''campaign_delete'',''campaign_schedule'',''campaign_queue'',''campaign_release'',''campaign_soft_bounce'',''camapign_hard_bounce''',
  `number_of_contacts` int(11) NOT NULL DEFAULT '0',
  `campaign_id` int(11) NOT NULL DEFAULT '0',
  `email_addresses` varchar(255) DEFAULT NULL,
  `contact_list_type` int(1) NOT NULL DEFAULT '0' COMMENT '1=>Individual,2=>Copy&Paste,3=>Imported from file,4=>Signup Form,5=>API',
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1839958 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_affiliate`
--

DROP TABLE IF EXISTS `red_affiliate`;
CREATE TABLE IF NOT EXISTS `red_affiliate` (
  `affiliate_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `product_sku` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_amount` int(11) NOT NULL,
  `ls_unique_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ls_request` text COLLATE utf8_unicode_ci NOT NULL,
  `ls_response` text COLLATE utf8_unicode_ci NOT NULL,
  `commission_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Free-plan,1=setup, 2=subscription',
  `posted_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`affiliate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=260 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_api_log`
--

DROP TABLE IF EXISTS `red_api_log`;
CREATE TABLE IF NOT EXISTS `red_api_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `api_action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_request` text COLLATE utf8_unicode_ci NOT NULL,
  `api_error` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=199165 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_autoresponder_group`
--

DROP TABLE IF EXISTS `red_autoresponder_group`;
CREATE TABLE IF NOT EXISTS `red_autoresponder_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `autoresponder_created_by` bigint(20) NOT NULL DEFAULT '0',
  `autoresponder_subscription_id` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=907 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_autoresponder_scheduled`
--

DROP TABLE IF EXISTS `red_autoresponder_scheduled`;
CREATE TABLE IF NOT EXISTS `red_autoresponder_scheduled` (
  `autoresponder_scheduled_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `autoresponder_id` bigint(20) NOT NULL DEFAULT '0',
  `subscription_ids` varchar(255) NOT NULL,
  `autoresponder_scheduled_interval` varchar(255) DEFAULT NULL,
  `autoresponder_social_share` int(1) NOT NULL DEFAULT '0',
  `autoresponder_scheduled_status` tinyint(4) NOT NULL DEFAULT '0',
  `set_sheduled` int(1) NOT NULL DEFAULT '0',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `scheduled_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`autoresponder_scheduled_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=815 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_autoresponder_signup`
--

DROP TABLE IF EXISTS `red_autoresponder_signup`;
CREATE TABLE IF NOT EXISTS `red_autoresponder_signup` (
  `autoresponder_scheduled_id` int(11) NOT NULL,
  `email_track_subscriber_id` int(11) NOT NULL DEFAULT '0',
  `subscriber_created_by` int(11) NOT NULL DEFAULT '0',
  `subscriber_email` varchar(255) NOT NULL,
  `email_track_bounce` int(1) NOT NULL DEFAULT '0',
  `email_track_complaint` int(1) NOT NULL DEFAULT '0',
  `complaint_date` datetime DEFAULT NULL,
  `bounce_date` datetime DEFAULT NULL,
  `email_track_read` int(1) NOT NULL DEFAULT '0',
  `email_track_click` int(1) NOT NULL DEFAULT '0',
  `email_track_forward` int(1) NOT NULL DEFAULT '0',
  `email_track_unsubscribes` int(1) NOT NULL DEFAULT '0',
  `email_delivered` int(1) NOT NULL DEFAULT '0',
  `email_receive_date` datetime DEFAULT NULL,
  `mail_send` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`autoresponder_scheduled_id`,`subscriber_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `red_blog_banned_words`
--

DROP TABLE IF EXISTS `red_blog_banned_words`;
CREATE TABLE IF NOT EXISTS `red_blog_banned_words` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_word` varchar(255) NOT NULL,
  PRIMARY KEY (`ban_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=231 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_blog_tblcategory`
--

DROP TABLE IF EXISTS `red_blog_tblcategory`;
CREATE TABLE IF NOT EXISTS `red_blog_tblcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `category_status` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_blog_tblcomment`
--

DROP TABLE IF EXISTS `red_blog_tblcomment`;
CREATE TABLE IF NOT EXISTS `red_blog_tblcomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci,
  `name` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `email` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `author` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `added_on` date DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='red_blog_tblcomment' AUTO_INCREMENT=145 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_blog_tblconfigurations`
--

DROP TABLE IF EXISTS `red_blog_tblconfigurations`;
CREATE TABLE IF NOT EXISTS `red_blog_tblconfigurations` (
  `config_name` varchar(255) NOT NULL,
  `config_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `red_blog_tblpost`
--

DROP TABLE IF EXISTS `red_blog_tblpost`;
CREATE TABLE IF NOT EXISTS `red_blog_tblpost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `summary` text CHARACTER SET utf8,
  `desc` text CHARACTER SET utf8 NOT NULL,
  `meta_keywords` text COLLATE latin1_general_ci,
  `meta_description` text COLLATE latin1_general_ci,
  `added_on` datetime DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `post_archives` tinyint(4) NOT NULL DEFAULT '0',
  `add_comment` int(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=131 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_blog_tblpost_images`
--

DROP TABLE IF EXISTS `red_blog_tblpost_images`;
CREATE TABLE IF NOT EXISTS `red_blog_tblpost_images` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `img_name` varchar(50) NOT NULL,
  `img_status` tinyint(4) NOT NULL DEFAULT '0',
  `img_default` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_blog_tblreply`
--

DROP TABLE IF EXISTS `red_blog_tblreply`;
CREATE TABLE IF NOT EXISTS `red_blog_tblreply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `added_on` date NOT NULL,
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_campaign_thread`
--

DROP TABLE IF EXISTS `red_campaign_thread`;
CREATE TABLE IF NOT EXISTS `red_campaign_thread` (
  `thread_id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`thread_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_click_rate`
--

DROP TABLE IF EXISTS `red_click_rate`;
CREATE TABLE IF NOT EXISTS `red_click_rate` (
  `click_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `subscriber_id` int(11) NOT NULL DEFAULT '0',
  `tiny_url` text NOT NULL COMMENT 'alpha-numeric code',
  `actual_url` text,
  `counter` int(11) NOT NULL DEFAULT '0',
  `date_click` datetime DEFAULT NULL,
  `is_autoresponder` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`click_id`),
  KEY `subscriber_id` (`subscriber_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5504796 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_configuration`
--

DROP TABLE IF EXISTS `red_configuration`;
CREATE TABLE IF NOT EXISTS `red_configuration` (
  `email-block-size-to-send` int(11) NOT NULL,
  `send-email-after-time` int(11) NOT NULL,
  `maximum_add_contact` int(11) NOT NULL,
  `maximum_delete_contact` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `red_contact_import_batch`
--

DROP TABLE IF EXISTS `red_contact_import_batch`;
CREATE TABLE IF NOT EXISTS `red_contact_import_batch` (
  `import_batch_id` int(11) NOT NULL AUTO_INCREMENT,
  `import_file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `import_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `import_batch_size` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL DEFAULT '0',
  `is_undone` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`import_batch_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36805 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_cookie`
--

DROP TABLE IF EXISTS `red_cookie`;
CREATE TABLE IF NOT EXISTS `red_cookie` (
  `user_tocken` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `expiry_time` bigint(20) NOT NULL,
  `user_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_tocken`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `red_countries`
--

DROP TABLE IF EXISTS `red_countries`;
CREATE TABLE IF NOT EXISTS `red_countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(25) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `country_name` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `country_state_label` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=248 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_coupons`
--

DROP TABLE IF EXISTS `red_coupons`;
CREATE TABLE IF NOT EXISTS `red_coupons` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `coupon_type` tinyint(4) NOT NULL COMMENT '0=absolute,1=percentage',
  `coupon_value` decimal(5,2) NOT NULL DEFAULT '0.00',
  `max_number_of_members` int(11) NOT NULL DEFAULT '1' COMMENT 'if any user can use it then, we can have 9999 as valid value',
  `usable_number_of_times` int(11) NOT NULL DEFAULT '1' COMMENT 'By default coupon will be valid for one time for each user, but can be made 9999 for life time each payment will get discount',
  `valid_untill` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=inactive,1=active',
  PRIMARY KEY (`coupon_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_disallowed_msg`
--

DROP TABLE IF EXISTS `red_disallowed_msg`;
CREATE TABLE IF NOT EXISTS `red_disallowed_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_code` varchar(255) NOT NULL,
  `msg_detail` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_autoresponders`
--

DROP TABLE IF EXISTS `red_email_autoresponders`;
CREATE TABLE IF NOT EXISTS `red_email_autoresponders` (
  `campaign_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `autoresponder_group_id` bigint(20) NOT NULL DEFAULT '0',
  `autoresponder_scheduled_id` int(11) NOT NULL DEFAULT '0',
  `campaign_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `campaign_template_option` int(1) NOT NULL DEFAULT '0',
  `import_campaign_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign_theme_id` int(11) NOT NULL DEFAULT '0',
  `campaign_template_id` int(11) NOT NULL DEFAULT '0',
  `campaign_color_theme_id` int(11) NOT NULL DEFAULT '0',
  `campaign_content` longtext COLLATE utf8_unicode_ci,
  `campaign_email_content` longtext COLLATE utf8_unicode_ci,
  `campaign_text_content` longtext COLLATE utf8_unicode_ci,
  `campaign_outer_bg` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '#ffffff',
  `campaign_css` text CHARACTER SET latin1,
  `autoresponder_subscription_id` int(11) NOT NULL DEFAULT '0',
  `autoresponder_scheduled_interval` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender_email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `sender_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autoresponder_social_share` tinyint(1) NOT NULL DEFAULT '0',
  `campaign_created_by` bigint(20) NOT NULL DEFAULT '0',
  `campaign_status` tinyint(4) NOT NULL DEFAULT '1',
  `campaign_date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `campaign_sheduled` datetime DEFAULT NULL,
  `test_email` int(11) NOT NULL DEFAULT '0',
  `is_verified` tinyint(4) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `is_status` tinyint(1) NOT NULL DEFAULT '1',
  `click_url` text CHARACTER SET latin1,
  `campaign_after_encode_url` longtext COLLATE utf8_unicode_ci,
  `is_ga_enabled` tinyint(4) NOT NULL DEFAULT '0',
  `is_clicktracking` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`campaign_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1677 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns`
--

DROP TABLE IF EXISTS `red_email_campaigns`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns` (
  `campaign_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campaign_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `campaign_template_option` int(1) NOT NULL COMMENT '1=>"import_url",2=>"import_zip",3=>"select_theme",4=>"paste_code",5=>"text_email"',
  `import_campaign_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign_theme_id` int(11) NOT NULL DEFAULT '0',
  `campaign_template_id` int(11) NOT NULL DEFAULT '0',
  `campaign_color_theme_id` int(11) NOT NULL DEFAULT '0',
  `campaign_content` longtext COLLATE utf8_unicode_ci,
  `campaign_email_content` longtext COLLATE utf8_unicode_ci,
  `campaign_text_content` longtext COLLATE utf8_unicode_ci,
  `campaign_outer_bg` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '#ffffff',
  `campaign_css` text CHARACTER SET latin1,
  `campaign_created_by` bigint(20) NOT NULL DEFAULT '0',
  `campaign_status` enum('draft','active','archived','disallow','ready','active_ready','queueing') CHARACTER SET latin1 NOT NULL DEFAULT 'draft',
  `pipeline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign_date_added` datetime NOT NULL,
  `campaign_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `campaign_sheduled` datetime DEFAULT NULL,
  `campaign_queued` datetime DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender_email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `sender_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reply_to_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscription_list` text COLLATE utf8_unicode_ci,
  `email_send_date` datetime DEFAULT NULL,
  `campaign_social_share` tinyint(1) NOT NULL DEFAULT '0',
  `test_email` int(11) NOT NULL DEFAULT '0',
  `is_segmentation` int(1) NOT NULL DEFAULT '0',
  `number_of_contacts` int(11) NOT NULL DEFAULT '0',
  `segment_interval` int(11) NOT NULL DEFAULT '0',
  `campaign_contacts` int(11) NOT NULL DEFAULT '0',
  `sent_counter` int(11) NOT NULL DEFAULT '0',
  `tobe_campaign_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `is_status` tinyint(1) NOT NULL DEFAULT '1',
  `campaign_priority` tinyint(4) NOT NULL DEFAULT '0',
  `click_url` text CHARACTER SET latin1,
  `campaign_after_encode_url` longtext COLLATE utf8_unicode_ci,
  `is_restore` int(1) NOT NULL DEFAULT '0',
  `is_ga_enabled` int(11) NOT NULL DEFAULT '0',
  `is_clicktracking` tinyint(4) NOT NULL DEFAULT '1',
  `bounces_over_limit` tinyint(4) NOT NULL DEFAULT '0',
  `complaints_over_limit` tinyint(4) NOT NULL DEFAULT '0',
  `unresponsive_gmail` int(11) NOT NULL DEFAULT '0',
  `unresponsive_hotmail` int(11) NOT NULL DEFAULT '0',
  `unresponsive_aol` int(11) NOT NULL DEFAULT '0',
  `unresponsive_yahoo` int(11) NOT NULL DEFAULT '0',
  `unresponsive_others` int(11) NOT NULL DEFAULT '0',
  `spamscore` tinyint(4) NOT NULL DEFAULT '0',
  `spamreport` text COLLATE utf8_unicode_ci,
  `is_preprocessed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`campaign_id`),
  KEY `campaign_status` (`campaign_status`),
  KEY `campaign_created_by` (`campaign_created_by`),
  KEY `idx_campaign_date_added` (`campaign_date_added`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=155896 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns_background_color_blocks`
--

DROP TABLE IF EXISTS `red_email_campaigns_background_color_blocks`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns_background_color_blocks` (
  `red_background_color_id` int(11) NOT NULL AUTO_INCREMENT,
  `red_background_color_template_id` int(11) NOT NULL DEFAULT '0',
  `red_background_color_block_name` varchar(255) NOT NULL,
  `red_background_color_default_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`red_background_color_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=110 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns_background_color_block_content`
--

DROP TABLE IF EXISTS `red_email_campaigns_background_color_block_content`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns_background_color_block_content` (
  `red_background_color_block_content_id` int(11) NOT NULL AUTO_INCREMENT,
  `red_background_color_block_name` varchar(255) NOT NULL,
  `red_background_color_page_id` int(11) NOT NULL DEFAULT '0',
  `red_background_color_block_content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`red_background_color_block_content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=145814 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns_color_themes`
--

DROP TABLE IF EXISTS `red_email_campaigns_color_themes`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns_color_themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_name` varchar(25) NOT NULL,
  `header_bg` varchar(25) DEFAULT NULL,
  `body_bg` varchar(25) DEFAULT NULL,
  `footer_bg` varchar(25) DEFAULT NULL,
  `border_color` varchar(25) DEFAULT NULL,
  `body_font_color` varchar(25) DEFAULT NULL,
  `footer_font_color` varchar(25) DEFAULT NULL,
  `outer_bg` varchar(255) DEFAULT NULL,
  `preheader_font_color` varchar(10) DEFAULT '#777777',
  `member_id` int(11) NOT NULL DEFAULT '0',
  `is_active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=480 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns_pages`
--

DROP TABLE IF EXISTS `red_email_campaigns_pages`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(150) NOT NULL,
  `title` varchar(255) NOT NULL,
  `page_position` int(11) NOT NULL DEFAULT '0',
  `meta_description` mediumtext,
  `meta_keyword` mediumtext,
  `is_published` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `is_deleted` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent_id` bigint(20) NOT NULL DEFAULT '0',
  `position` bigint(20) NOT NULL DEFAULT '0',
  `left` bigint(20) NOT NULL DEFAULT '0',
  `right` bigint(20) NOT NULL DEFAULT '0',
  `level` bigint(20) NOT NULL DEFAULT '0',
  `type` varchar(25) DEFAULT NULL,
  `is_autoresponder` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=157571 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns_scheduled`
--

DROP TABLE IF EXISTS `red_email_campaigns_scheduled`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns_scheduled` (
  `campaign_scheduled_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email_track_released` int(11) NOT NULL DEFAULT '0',
  `email_track_sent` int(11) NOT NULL DEFAULT '0',
  `email_track_delivered` int(11) NOT NULL DEFAULT '0',
  `email_track_bounce` int(1) NOT NULL DEFAULT '0',
  `email_track_read` int(1) NOT NULL DEFAULT '0',
  `email_track_click` int(11) NOT NULL DEFAULT '0',
  `email_track_forward` int(11) NOT NULL DEFAULT '0',
  `email_track_unsubscribes` int(11) NOT NULL DEFAULT '0',
  `email_track_spam` int(11) NOT NULL DEFAULT '0',
  `campaign_id` bigint(20) NOT NULL DEFAULT '0',
  `subscription_id` int(11) NOT NULL DEFAULT '0',
  `campaign_scheduled_date` datetime DEFAULT NULL,
  `campaign_social_share` int(1) NOT NULL DEFAULT '0',
  `campaign_scheduled_status` tinyint(4) NOT NULL DEFAULT '1',
  `set_sheduled` int(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`campaign_scheduled_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=113462 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns_templates`
--

DROP TABLE IF EXISTS `red_email_campaigns_templates`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) NOT NULL,
  `template_theme_id` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `show_on_dashboard` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=494 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns_template_blocks`
--

DROP TABLE IF EXISTS `red_email_campaigns_template_blocks`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns_template_blocks` (
  `block_id` int(11) NOT NULL AUTO_INCREMENT,
  `block_template_id` int(11) NOT NULL,
  `block_name` varchar(250) NOT NULL,
  `block_default_content` mediumtext NOT NULL,
  PRIMARY KEY (`block_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns_template_block_content`
--

DROP TABLE IF EXISTS `red_email_campaigns_template_block_content`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns_template_block_content` (
  `block_content_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `block_name` varchar(250) NOT NULL,
  `page_id` bigint(20) NOT NULL DEFAULT '0',
  `block_content` mediumtext,
  `block_display_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`block_content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=877 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_campaigns_template_category`
--

DROP TABLE IF EXISTS `red_email_campaigns_template_category`;
CREATE TABLE IF NOT EXISTS `red_email_campaigns_template_category` (
  `red_theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `red_theme_name` varchar(255) NOT NULL,
  `red_is_active` int(1) NOT NULL DEFAULT '0',
  `red_is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`red_theme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_personalization`
--

DROP TABLE IF EXISTS `red_email_personalization`;
CREATE TABLE IF NOT EXISTS `red_email_personalization` (
  `name` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_queue`
--

DROP TABLE IF EXISTS `red_email_queue`;
CREATE TABLE IF NOT EXISTS `red_email_queue` (
  `campaign_id` int(11) NOT NULL DEFAULT '0',
  `subscriber_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `subscriber_email_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `email_sent` tinyint(1) NOT NULL DEFAULT '0',
  `email_sent_date` datetime DEFAULT NULL,
  `not_sent_reason` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`campaign_id`,`subscriber_id`),
  KEY `user_id` (`user_id`),
  KEY `subscriber_id` (`subscriber_id`),
  KEY `email_sent` (`email_sent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_subscribers`
--

DROP TABLE IF EXISTS `red_email_subscribers`;
CREATE TABLE IF NOT EXISTS `red_email_subscribers` (
  `subscriber_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subscriber_created_by` int(11) DEFAULT NULL,
  `subscriber_first_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_last_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_email_address` varchar(250) CHARACTER SET utf8 NOT NULL,
  `subscriber_email_domain` varchar(255) DEFAULT NULL,
  `subscriber_vmta` varchar(255) DEFAULT NULL,
  `subscriber_state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_zip_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_country` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_company` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_dob` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subscriber_address` text CHARACTER SET utf8,
  `subscriber_extra_fields` text CHARACTER SET utf8,
  `subscriber_ip` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `subscription_id` bigint(20) NOT NULL DEFAULT '0',
  `subscriber_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=>unsubscribed,1=>subscribed, 2=>complaint, 3=>Hard Bounce,4=>Soft Bounce Unsubscribed, 5=> suppressed',
  `status_change_date` datetime DEFAULT NULL,
  `subscrber_bounce` int(1) NOT NULL DEFAULT '0' COMMENT '0=>''Not Bounce'',1=>''Soft Bounce'',2=>''Hard Bounce''',
  `soft_bounce` int(11) NOT NULL DEFAULT '0',
  `bounce_status` varchar(255) DEFAULT NULL,
  `bounce_detail` text,
  `subscriber_date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `import_batch_id` int(11) NOT NULL DEFAULT '0',
  `is_signup` int(1) NOT NULL DEFAULT '0',
  `signup_form_id` int(11) NOT NULL DEFAULT '0',
  `is_single_optin` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `ignore` tinyint(4) NOT NULL DEFAULT '0',
  `sent` int(11) NOT NULL DEFAULT '0',
  `last_sent_date` datetime DEFAULT NULL,
  `release_count` int(11) NOT NULL DEFAULT '0',
  `last_release_date` datetime DEFAULT NULL,
  `read` int(11) NOT NULL DEFAULT '0',
  `last_read_date` datetime DEFAULT NULL,
  `clicked` int(11) NOT NULL DEFAULT '0',
  `last_clicked_date` datetime DEFAULT NULL,
  `forwarded` int(11) NOT NULL DEFAULT '0',
  `last_forwarded_date` datetime DEFAULT NULL,
  `unsubscribed` int(11) NOT NULL DEFAULT '0',
  `last_unsubscribed_date` datetime DEFAULT NULL,
  `bounced` int(11) NOT NULL DEFAULT '0',
  `last_bounced_date` datetime DEFAULT NULL,
  `complaint` int(11) NOT NULL DEFAULT '0',
  `last_complaint_date` datetime DEFAULT NULL,
  `is_analysed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscriber_id`),
  UNIQUE KEY `subscriber_created_by` (`subscriber_created_by`,`subscriber_email_address`),
  KEY `subscrber_bounce` (`subscrber_bounce`,`soft_bounce`),
  KEY `subscriber_created_by_2` (`subscriber_created_by`),
  KEY `subscriber_email_address` (`subscriber_email_address`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=124994673 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_subscriptions`
--

DROP TABLE IF EXISTS `red_email_subscriptions`;
CREATE TABLE IF NOT EXISTS `red_email_subscriptions` (
  `subscription_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subscription_title` varchar(250) NOT NULL,
  `subscription_status` tinyint(4) NOT NULL DEFAULT '1',
  `subscription_created_by` bigint(20) NOT NULL DEFAULT '0',
  `subscription_date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscription_is_email` int(1) NOT NULL DEFAULT '1',
  `subscription_is_name` int(1) NOT NULL DEFAULT '0',
  `subscription_is_phone` int(1) NOT NULL DEFAULT '0',
  `subscription_is_address` int(1) NOT NULL DEFAULT '0',
  `subscription_is_dob` int(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscription_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11144 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_subscription_subscriber`
--

DROP TABLE IF EXISTS `red_email_subscription_subscriber`;
CREATE TABLE IF NOT EXISTS `red_email_subscription_subscriber` (
  `subscriber_id` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  PRIMARY KEY (`subscriber_id`,`subscription_id`),
  KEY `subscription_id` (`subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_track`
--

DROP TABLE IF EXISTS `red_email_track`;
CREATE TABLE IF NOT EXISTS `red_email_track` (
  `queue_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `subscriber_id` int(11) NOT NULL DEFAULT '0',
  `subscriber_email_address` varchar(255) NOT NULL,
  `subscriber_email_domain` varchar(255) DEFAULT NULL,
  `email_receive_date` datetime DEFAULT NULL,
  `email_track_bounce` tinyint(1) NOT NULL DEFAULT '0',
  `bounce_date` datetime DEFAULT NULL,
  `email_track_complaint` tinyint(1) NOT NULL DEFAULT '0',
  `complaint_date` datetime DEFAULT NULL,
  `email_track_read` tinyint(1) NOT NULL DEFAULT '0',
  `email_track_read_date` datetime DEFAULT NULL,
  `email_track_click` int(1) NOT NULL DEFAULT '0',
  `date_click` datetime DEFAULT NULL,
  `email_track_forward` tinyint(1) NOT NULL DEFAULT '0',
  `date_forward` datetime DEFAULT NULL,
  `email_track_unsubscribes` tinyint(1) NOT NULL DEFAULT '0',
  `date_unsubscribe` datetime DEFAULT NULL,
  `email_sent` tinyint(1) NOT NULL DEFAULT '0',
  `email_sent_date` datetime DEFAULT NULL,
  `email_delivered` tinyint(1) NOT NULL DEFAULT '0',
  `not_sent_reason` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`queue_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `subscriber_id` (`subscriber_id`),
  KEY `user_id` (`user_id`),
  KEY `email_sent_date` (`email_sent_date`),
  KEY `subscriber_email_address` (`subscriber_email_address`),
  KEY `subscriber_email_domain` (`subscriber_email_domain`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2837150283 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_email_track_freezed`
--

DROP TABLE IF EXISTS `red_email_track_freezed`;
CREATE TABLE IF NOT EXISTS `red_email_track_freezed` (
  `campaign_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `send_email_count` int(11) NOT NULL DEFAULT '0',
  `delivered_count` int(11) NOT NULL DEFAULT '0',
  `read_email_count` int(11) NOT NULL DEFAULT '0',
  `click_link_count` int(11) NOT NULL DEFAULT '0',
  `bounce_email_count` int(11) NOT NULL DEFAULT '0',
  `complaint_email_count` int(11) NOT NULL DEFAULT '0',
  `unsubscribes_email_count` int(11) NOT NULL DEFAULT '0',
  `forward_email_count` int(11) NOT NULL DEFAULT '0',
  `ipr` text CHARACTER SET utf8,
  `freezing_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `red_feedback`
--

DROP TABLE IF EXISTS `red_feedback`;
CREATE TABLE IF NOT EXISTS `red_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=372 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_forward_friend`
--

DROP TABLE IF EXISTS `red_forward_friend`;
CREATE TABLE IF NOT EXISTS `red_forward_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL DEFAULT '0',
  `subscriber_id` int(11) NOT NULL DEFAULT '0',
  `forward_friend` varchar(255) NOT NULL,
  `is_autoresponder` int(1) NOT NULL DEFAULT '0',
  `date_forward` datetime DEFAULT NULL,
  `ip_address` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=650251 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_ga_domains`
--

DROP TABLE IF EXISTS `red_ga_domains`;
CREATE TABLE IF NOT EXISTS `red_ga_domains` (
  `member_id` int(11) NOT NULL DEFAULT '0',
  `domain_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`,`domain_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_global_dnm`
--

DROP TABLE IF EXISTS `red_global_dnm`;
CREATE TABLE IF NOT EXISTS `red_global_dnm` (
  `email_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `dnm_type` tinyint(4) NOT NULL COMMENT '1=hardbounce,2=softbounce,3=complaints,4=unsubscribes',
  `dnm_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`email_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_global_fbl`
--

DROP TABLE IF EXISTS `red_global_fbl`;
CREATE TABLE IF NOT EXISTS `red_global_fbl` (
  `email_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `added_via` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=webmails-fbl, 1= redcappi-report-abuse',
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`email_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_global_fbl_old`
--

DROP TABLE IF EXISTS `red_global_fbl_old`;
CREATE TABLE IF NOT EXISTS `red_global_fbl_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=108898 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_global_ipr_daily`
--

DROP TABLE IF EXISTS `red_global_ipr_daily`;
CREATE TABLE IF NOT EXISTS `red_global_ipr_daily` (
  `mail_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `log_date` date NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `pipeline` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_sent` int(11) NOT NULL DEFAULT '0',
  `total_released` int(11) NOT NULL DEFAULT '0',
  `total_delivered` int(11) NOT NULL DEFAULT '0',
  `total_opened` int(11) NOT NULL DEFAULT '0',
  `total_bounced` int(11) NOT NULL DEFAULT '0',
  `total_unsubscribed` int(11) NOT NULL DEFAULT '0',
  `total_complaint` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mail_domain`,`log_date`,`user_id`,`pipeline`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_image_bank`
--

DROP TABLE IF EXISTS `red_image_bank`;
CREATE TABLE IF NOT EXISTS `red_image_bank` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `img_user_id` int(11) NOT NULL DEFAULT '0',
  `img_name` varchar(255) DEFAULT NULL,
  `img_is_delete` int(1) NOT NULL DEFAULT '0',
  `img_is_status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55673 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_language`
--

DROP TABLE IF EXISTS `red_language`;
CREATE TABLE IF NOT EXISTS `red_language` (
  `language_code` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`language_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_members`
--

DROP TABLE IF EXISTS `red_members`;
CREATE TABLE IF NOT EXISTS `red_members` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `member_username` varchar(50) CHARACTER SET utf8 NOT NULL,
  `member_password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `vmta` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'mailsvrc.com',
  `ip_address` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `last_login_time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `phone_number` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address_line_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address_line_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(25) DEFAULT NULL,
  `country` int(11) NOT NULL DEFAULT '225',
  `country_custom` varchar(50) DEFAULT NULL,
  `company` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` enum('unconfirmed','active','inactive','failed-cc') NOT NULL DEFAULT 'unconfirmed',
  `status_inactive_description` varchar(255) DEFAULT NULL COMMENT '"policy_related","unconfirmed","failed cc"',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `autoresponder_status` int(1) NOT NULL DEFAULT '0',
  `sign_up_form_status` int(1) NOT NULL DEFAULT '0',
  `google_analytics_status` int(11) NOT NULL DEFAULT '0',
  `clicktracking_status` tinyint(4) NOT NULL DEFAULT '0',
  `contact_import` int(11) NOT NULL DEFAULT '0',
  `package_id` int(11) NOT NULL DEFAULT '0',
  `is_authentic` int(1) NOT NULL DEFAULT '0' COMMENT '0=>''Not Authentic'', ''1''=>''Authentic''',
  `campaign_priority` tinyint(4) NOT NULL DEFAULT '0',
  `authenticated_on` datetime DEFAULT NULL,
  `unauthentic_contacts` bigint(20) NOT NULL DEFAULT '0',
  `apply_unauthentication_message` tinyint(4) NOT NULL DEFAULT '1',
  `is_pausable` tinyint(4) NOT NULL DEFAULT '1',
  `quick_autoreponder` tinyint(4) NOT NULL DEFAULT '0',
  `campaign_approval_notes` text,
  `is_automatic_segmentation` tinyint(4) NOT NULL DEFAULT '0',
  `segment_size` int(11) NOT NULL DEFAULT '0',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_date` datetime DEFAULT NULL,
  `rc_logo` int(1) NOT NULL DEFAULT '1',
  `login_expiration_notification_date` datetime DEFAULT NULL,
  `cancel_subscription_date` datetime DEFAULT NULL,
  `language` varchar(255) NOT NULL DEFAULT 'en',
  `ls_site_id` varchar(255) DEFAULT NULL,
  `ls_added_on` varchar(255) DEFAULT NULL,
  `affiliate_tracking_status` tinyint(4) NOT NULL DEFAULT '0',
  `contact_import_progress` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=>not in progress,1=>in progress',
  `attach_seedlist` tinyint(4) NOT NULL DEFAULT '0',
  `is_risky` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=Risky,0=Normal',
  `is_disclaimer` tinyint(4) NOT NULL DEFAULT '0',
  `member_dnm` text,
  `member_unresponsive` text,
  `unresponsive_release_count` int(11) NOT NULL DEFAULT '0',
  `apply_unresponsive_filter` tinyint(4) NOT NULL DEFAULT '0',
  `stop_campaign_approval` tinyint(4) NOT NULL DEFAULT '0',
  `always_slow_release` tinyint(1) NOT NULL DEFAULT '0',
  `show_sent_counter` tinyint(4) NOT NULL DEFAULT '0',
  `reply_to_enabled` tinyint(4) NOT NULL DEFAULT '0',
  `member_time_zone` varchar(255) NOT NULL DEFAULT 'America/Los_Angeles',
  `first_failed_login` timestamp NULL DEFAULT NULL,
  `failed_login_count` tinyint(4) NOT NULL DEFAULT '0',
  `manage_campaigns` tinyint(4) NOT NULL DEFAULT '1',
  `manage_contacts` tinyint(4) NOT NULL DEFAULT '1',
  `manage_stats` tinyint(4) NOT NULL DEFAULT '1',
  `manage_autoresponders` tinyint(4) NOT NULL DEFAULT '1',
  `manage_signupforms` tinyint(4) NOT NULL DEFAULT '1',
  `manage_extra` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11550 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_api`
--

DROP TABLE IF EXISTS `red_member_api`;
CREATE TABLE IF NOT EXISTS `red_member_api` (
  `member_id` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `public_key` varchar(255) DEFAULT NULL,
  `private_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_contact_detail`
--

DROP TABLE IF EXISTS `red_member_contact_detail`;
CREATE TABLE IF NOT EXISTS `red_member_contact_detail` (
  `member_id` int(11) NOT NULL,
  `analysis_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `yahoo_total` int(11) NOT NULL DEFAULT '0',
  `yahoo_unsent` int(11) NOT NULL DEFAULT '0',
  `yahoo_responsive` int(11) NOT NULL DEFAULT '0',
  `yahoo_unresponsive` int(11) NOT NULL DEFAULT '0',
  `gmail_total` int(11) NOT NULL DEFAULT '0',
  `gmail_unsent` int(11) NOT NULL DEFAULT '0',
  `gmail_responsive` int(11) NOT NULL DEFAULT '0',
  `gmail_unresponsive` int(11) NOT NULL DEFAULT '0',
  `hotmail_total` int(11) NOT NULL DEFAULT '0',
  `hotmail_unsent` int(11) NOT NULL DEFAULT '0',
  `hotmail_responsive` int(11) NOT NULL DEFAULT '0',
  `hotmail_unresponsive` int(11) NOT NULL DEFAULT '0',
  `aol_total` int(11) NOT NULL DEFAULT '0',
  `aol_unsent` int(11) NOT NULL DEFAULT '0',
  `aol_responsive` int(11) NOT NULL DEFAULT '0',
  `aol_unresponsive` int(11) NOT NULL DEFAULT '0',
  `msn_total` int(11) NOT NULL DEFAULT '0',
  `msn_unsent` int(11) NOT NULL DEFAULT '0',
  `msn_responsive` int(11) NOT NULL DEFAULT '0',
  `msn_unresponsive` int(11) NOT NULL DEFAULT '0',
  `all_total` int(11) NOT NULL DEFAULT '0',
  `all_unsent` int(11) NOT NULL DEFAULT '0',
  `all_responsive` int(11) NOT NULL DEFAULT '0',
  `all_unresponsive` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_from_email`
--

DROP TABLE IF EXISTS `red_member_from_email`;
CREATE TABLE IF NOT EXISTS `red_member_from_email` (
  `member_id` int(11) NOT NULL,
  `email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unique_string` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain_reason` text COLLATE utf8_unicode_ci,
  `is_verified` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`,`email_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_message`
--

DROP TABLE IF EXISTS `red_member_message`;
CREATE TABLE IF NOT EXISTS `red_member_message` (
  `member_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `assigned_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`,`message_id`,`assigned_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_packages`
--

DROP TABLE IF EXISTS `red_member_packages`;
CREATE TABLE IF NOT EXISTS `red_member_packages` (
  `red_member_package_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL DEFAULT '0',
  `package_id` int(11) NOT NULL DEFAULT '0',
  `payment_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=>credit_card 1=> paypal',
  `payment_paypal_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'save status of paypal payment 0=>inactive 1=> success 2=>failed 3=> invalid',
  `paypal_transaction_id` text COMMENT 'profile id of paypal transaction',
  `paypal_payer_email` varchar(255) DEFAULT NULL,
  `max_campaign_quota` int(11) NOT NULL DEFAULT '0' COMMENT 'package capacity calculated from max_contact * quota_multiplier from red_packages',
  `campaign_sent_counter` int(11) NOT NULL DEFAULT '0' COMMENT 'incremented when campaigns are sent and reset when payment is done',
  `user_quota_multiplier` float NOT NULL DEFAULT '1',
  `package_number` int(11) NOT NULL DEFAULT '1',
  `credit_card_last_digit` varchar(4) DEFAULT NULL,
  `expiration_date` varchar(255) DEFAULT NULL,
  `card_holder_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `address` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `subscription_id` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `package_status` tinyint(4) NOT NULL DEFAULT '1',
  `date_inactivated` datetime DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_profile_id` int(20) NOT NULL DEFAULT '0',
  `customer_payment_profile_id` int(20) NOT NULL DEFAULT '0',
  `selected_package_id` int(11) NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `amount_to_member` float NOT NULL DEFAULT '0',
  `start_payment_date` date DEFAULT NULL,
  `next_payement_date` date DEFAULT NULL,
  `is_payment` int(1) NOT NULL DEFAULT '0',
  `is_admin` int(1) NOT NULL DEFAULT '0',
  `is_status` int(1) NOT NULL DEFAULT '1',
  `is_first_campaign_send` int(1) NOT NULL DEFAULT '0',
  `member_payment_declined_count` int(11) NOT NULL DEFAULT '0',
  `coupon_code_used` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`red_member_package_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11554 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_referral`
--

DROP TABLE IF EXISTS `red_member_referral`;
CREATE TABLE IF NOT EXISTS `red_member_referral` (
  `ip_logged` int(11) NOT NULL,
  `referer_logged` tinyint(4) NOT NULL DEFAULT '1',
  `time_logged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `referer_url` text COLLATE utf8_unicode_ci NOT NULL,
  `vistor_detail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ip_logged`,`referer_logged`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_referrer`
--

DROP TABLE IF EXISTS `red_member_referrer`;
CREATE TABLE IF NOT EXISTS `red_member_referrer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referrer_name` varchar(255) NOT NULL,
  `referrer_email` varchar(255) NOT NULL,
  `referrer_string` varchar(255) NOT NULL,
  `commission` double(5,2) NOT NULL,
  `commission_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 for absolute, 1 for percent amount',
  `commission_months` smallint(4) NOT NULL DEFAULT '1' COMMENT '999 for lifetime',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_track`
--

DROP TABLE IF EXISTS `red_member_track`;
CREATE TABLE IF NOT EXISTS `red_member_track` (
  `member_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `signup_ip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_source` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_medium` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_campaign` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_transactions`
--

DROP TABLE IF EXISTS `red_member_transactions`;
CREATE TABLE IF NOT EXISTS `red_member_transactions` (
  `transaction_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `package_id` int(11) NOT NULL DEFAULT '0',
  `amount_paid` float NOT NULL DEFAULT '0',
  `transaction_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gateway` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `payment_type` tinyint(4) NOT NULL DEFAULT '0',
  `gateway_response` mediumtext,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12746 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_member_transactions1`
--

DROP TABLE IF EXISTS `red_member_transactions1`;
CREATE TABLE IF NOT EXISTS `red_member_transactions1` (
  `transaction_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `package_id` int(11) NOT NULL DEFAULT '0',
  `amount_paid` float NOT NULL DEFAULT '0',
  `transaction_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gateway` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `payment_type` tinyint(4) NOT NULL DEFAULT '0',
  `gateway_response` mediumtext,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6438 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_messages`
--

DROP TABLE IF EXISTS `red_messages`;
CREATE TABLE IF NOT EXISTS `red_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=system, 1= admin',
  `user_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=bulk, 1=individual',
  `message_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message_body_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=dashboard, 1= global, 2= on page',
  `message_body` text COLLATE utf8_unicode_ci NOT NULL,
  `is_mail_notification` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `email_subject` text COLLATE utf8_unicode_ci,
  `email_body` text COLLATE utf8_unicode_ci,
  `message_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_onboarding_mails`
--

DROP TABLE IF EXISTS `red_onboarding_mails`;
CREATE TABLE IF NOT EXISTS `red_onboarding_mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `mail_id` int(11) NOT NULL,
  `sent_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1804 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_ongoing_segmentation`
--

DROP TABLE IF EXISTS `red_ongoing_segmentation`;
CREATE TABLE IF NOT EXISTS `red_ongoing_segmentation` (
  `campaign_id` int(11) NOT NULL,
  `segment_size` int(11) NOT NULL,
  `segment_interval` int(11) NOT NULL DEFAULT '30',
  `interval_variance` int(11) NOT NULL DEFAULT '0',
  `last_released_on` datetime DEFAULT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_packages`
--

DROP TABLE IF EXISTS `red_packages`;
CREATE TABLE IF NOT EXISTS `red_packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_type` enum('combo','newsletter','sitebuilder','free') NOT NULL DEFAULT 'newsletter',
  `package_title` varchar(250) NOT NULL,
  `package_summary` text NOT NULL,
  `package_price` decimal(7,2) NOT NULL DEFAULT '0.00',
  `package_recurring_interval` enum('months','years') NOT NULL DEFAULT 'months',
  `package_price_summary` text NOT NULL,
  `package_min_contacts` int(255) NOT NULL DEFAULT '0',
  `package_max_contacts` int(11) NOT NULL DEFAULT '0',
  `quota_multiplier` int(11) NOT NULL DEFAULT '10',
  `is_special` tinyint(1) NOT NULL DEFAULT '1',
  `package_status` tinyint(4) NOT NULL DEFAULT '1',
  `package_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_paypal_response`
--

DROP TABLE IF EXISTS `red_paypal_response`;
CREATE TABLE IF NOT EXISTS `red_paypal_response` (
  `red_paypal_response_id` int(11) NOT NULL AUTO_INCREMENT,
  `red_member_package_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk of red_member_packages',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk of red_members',
  `package_id` int(11) NOT NULL DEFAULT '0' COMMENT 'fk of red_packages ',
  `paypal_profile_id` text COLLATE utf8_unicode_ci,
  `response` text COLLATE utf8_unicode_ci,
  `createddate` datetime DEFAULT NULL COMMENT 'Date of create order',
  PRIMARY KEY (`red_paypal_response_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_pmtalog`
--

DROP TABLE IF EXISTS `red_pmtalog`;
CREATE TABLE IF NOT EXISTS `red_pmtalog` (
  `logid` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `timeLogged` varchar(255) DEFAULT NULL,
  `orig` varchar(255) DEFAULT NULL,
  `rcpt` varchar(255) DEFAULT NULL,
  `dsnAction` varchar(255) DEFAULT NULL,
  `dsnStatus` varchar(255) DEFAULT NULL,
  `dsnDiag` text,
  `dsnMta` varchar(255) DEFAULT NULL,
  `bounceCat` varchar(255) DEFAULT NULL,
  `srcType` varchar(255) DEFAULT NULL,
  `srcMta` varchar(255) DEFAULT NULL,
  `dlvType` varchar(255) DEFAULT NULL,
  `dlvSourceIp` varchar(255) DEFAULT NULL,
  `dlvDestinationIp` varchar(255) DEFAULT NULL,
  `dlvSize` varchar(255) DEFAULT NULL,
  `vmta` varchar(255) DEFAULT NULL,
  `jobId` varchar(255) DEFAULT NULL,
  `envId` varchar(255) DEFAULT NULL,
  `header_cid` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`logid`),
  KEY `envId` (`envId`),
  KEY `jobId` (`jobId`),
  KEY `timeLogged` (`timeLogged`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=115004943 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_pmtalog_blocked`
--

DROP TABLE IF EXISTS `red_pmtalog_blocked`;
CREATE TABLE IF NOT EXISTS `red_pmtalog_blocked` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timeLogged` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orig` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rcpt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dsnAction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dsnStatus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dsnDiag` text COLLATE utf8_unicode_ci,
  `dsnMta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bounceCat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `srcType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `srcMta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dlvType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dlvSourceIp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dlvDestinationIp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dlvSize` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vmta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `envId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_cid` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `envId` (`envId`),
  KEY `jobId` (`jobId`),
  KEY `timeLogged` (`timeLogged`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1364439 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_seo`
--

DROP TABLE IF EXISTS `red_seo`;
CREATE TABLE IF NOT EXISTS `red_seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` text,
  `title` text,
  `keyword` text,
  `description` text,
  `h1` text,
  `content` longtext,
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_signup_form`
--

DROP TABLE IF EXISTS `red_signup_form`;
CREATE TABLE IF NOT EXISTS `red_signup_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL DEFAULT '0',
  `form_language` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `form_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `form_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `form_background_color` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '#EAE9E9',
  `form_button_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Subscribe to list',
  `form_button_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_background_color` varchar(55) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#000000',
  `header_text_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#454545',
  `header_background_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background_background_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background_background_tile_image` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscription_id` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `is_email` int(1) NOT NULL DEFAULT '1',
  `is_name` int(1) NOT NULL DEFAULT '0',
  `is_first_name` int(1) NOT NULL DEFAULT '0',
  `is_last_name` int(1) NOT NULL DEFAULT '0',
  `is_company` int(1) NOT NULL DEFAULT '0',
  `is_address` int(1) NOT NULL DEFAULT '0',
  `is_city` int(1) NOT NULL DEFAULT '0',
  `is_state` int(1) NOT NULL DEFAULT '0',
  `is_zip_code` int(1) NOT NULL DEFAULT '0',
  `is_country` int(1) NOT NULL DEFAULT '0',
  `custom_field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_sequence` text COLLATE utf8_unicode_ci,
  `fld_sequence` text COLLATE utf8_unicode_ci,
  `from_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_thanks_you_message_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `singup_thank_you_message_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_emai_message` text COLLATE utf8_unicode_ci,
  `form_submission_counter` int(11) NOT NULL DEFAULT '0',
  `contact_confirmation_counter` int(11) NOT NULL DEFAULT '0',
  `single_opt_in` tinyint(4) NOT NULL DEFAULT '0',
  `is_verified` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `is_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `is_stats` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1196 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_signup_form_stats`
--

DROP TABLE IF EXISTS `red_signup_form_stats`;
CREATE TABLE IF NOT EXISTS `red_signup_form_stats` (
  `form_id` int(11) NOT NULL,
  `ip_address` bigint(20) NOT NULL,
  `activity` tinyint(1) NOT NULL COMMENT '1=view, 2=submission,3=confirmation',
  `subscriber_id` int(50) NOT NULL DEFAULT '0',
  `activity_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`form_id`,`ip_address`,`activity`,`subscriber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_site_configurations`
--

DROP TABLE IF EXISTS `red_site_configurations`;
CREATE TABLE IF NOT EXISTS `red_site_configurations` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(255) NOT NULL,
  `config_value` text NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_subscriber_analysis`
--

DROP TABLE IF EXISTS `red_subscriber_analysis`;
CREATE TABLE IF NOT EXISTS `red_subscriber_analysis` (
  `member_id` int(11) NOT NULL,
  `analysis_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reanalyse_it` int(11) NOT NULL DEFAULT '0',
  `yahoo_total` int(11) NOT NULL DEFAULT '0',
  `yahoo_new` int(11) NOT NULL DEFAULT '0',
  `yahoo_existing` int(11) NOT NULL DEFAULT '0',
  `yahoo_responsive` int(11) NOT NULL DEFAULT '0',
  `yahoo_unresponsive` int(11) NOT NULL DEFAULT '0',
  `yahoo_bounce` int(11) NOT NULL DEFAULT '0',
  `yahoo_complaint` int(11) NOT NULL DEFAULT '0',
  `yahoo_unsubscribe` int(11) NOT NULL DEFAULT '0',
  `yahoo_spam` int(11) NOT NULL DEFAULT '0',
  `gmail_total` int(11) NOT NULL DEFAULT '0',
  `gmail_new` int(11) NOT NULL DEFAULT '0',
  `gmail_existing` int(11) NOT NULL DEFAULT '0',
  `gmail_responsive` int(11) NOT NULL DEFAULT '0',
  `gmail_unresponsive` int(11) NOT NULL DEFAULT '0',
  `gmail_bounce` int(11) NOT NULL DEFAULT '0',
  `gmail_complaint` int(11) NOT NULL DEFAULT '0',
  `gmail_unsubscribe` int(11) NOT NULL DEFAULT '0',
  `gmail_spam` int(11) NOT NULL DEFAULT '0',
  `hotmail_total` int(11) NOT NULL DEFAULT '0',
  `hotmail_new` int(11) NOT NULL DEFAULT '0',
  `hotmail_existing` int(11) NOT NULL DEFAULT '0',
  `hotmail_responsive` int(11) NOT NULL DEFAULT '0',
  `hotmail_unresponsive` int(11) NOT NULL DEFAULT '0',
  `hotmail_bounce` int(11) NOT NULL DEFAULT '0',
  `hotmail_complaint` int(11) NOT NULL DEFAULT '0',
  `hotmail_unsubscribe` int(11) NOT NULL DEFAULT '0',
  `hotmail_spam` int(11) NOT NULL DEFAULT '0',
  `aol_total` int(11) NOT NULL DEFAULT '0',
  `aol_new` int(11) NOT NULL DEFAULT '0',
  `aol_existing` int(11) NOT NULL DEFAULT '0',
  `aol_responsive` int(11) NOT NULL DEFAULT '0',
  `aol_unresponsive` int(11) NOT NULL DEFAULT '0',
  `aol_bounce` int(11) NOT NULL DEFAULT '0',
  `aol_complaint` int(11) NOT NULL DEFAULT '0',
  `aol_unsubscribe` int(11) NOT NULL DEFAULT '0',
  `aol_spam` int(11) NOT NULL DEFAULT '0',
  `msn_total` int(11) NOT NULL DEFAULT '0',
  `msn_new` int(11) NOT NULL DEFAULT '0',
  `msn_existing` int(11) NOT NULL DEFAULT '0',
  `msn_responsive` int(11) NOT NULL DEFAULT '0',
  `msn_unresponsive` int(11) NOT NULL DEFAULT '0',
  `msn_bounce` int(11) NOT NULL DEFAULT '0',
  `msn_complaint` int(11) NOT NULL DEFAULT '0',
  `msn_unsubscribe` int(11) NOT NULL DEFAULT '0',
  `msn_spam` int(11) NOT NULL DEFAULT '0',
  `all_total` int(11) NOT NULL DEFAULT '0',
  `all_new` int(11) NOT NULL DEFAULT '0',
  `all_existing` int(11) NOT NULL DEFAULT '0',
  `all_responsive` int(11) NOT NULL DEFAULT '0',
  `all_unresponsive` int(11) NOT NULL DEFAULT '0',
  `all_bounce` int(11) NOT NULL DEFAULT '0',
  `all_complaint` int(11) NOT NULL DEFAULT '0',
  `all_unsubscribe` int(11) NOT NULL DEFAULT '0',
  `all_spam` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_support_category`
--

DROP TABLE IF EXISTS `red_support_category`;
CREATE TABLE IF NOT EXISTS `red_support_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_support_product`
--

DROP TABLE IF EXISTS `red_support_product`;
CREATE TABLE IF NOT EXISTS `red_support_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

-- --------------------------------------------------------

--
-- Table structure for table `red_text_translation_languages`
--

DROP TABLE IF EXISTS `red_text_translation_languages`;
CREATE TABLE IF NOT EXISTS `red_text_translation_languages` (
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_unsubscribe_feedback`
--

DROP TABLE IF EXISTS `red_unsubscribe_feedback`;
CREATE TABLE IF NOT EXISTS `red_unsubscribe_feedback` (
  `subscriber_id` int(11) NOT NULL DEFAULT '0',
  `campaign_id` int(11) NOT NULL DEFAULT '0',
  `member_id` int(11) NOT NULL DEFAULT '0',
  `vmta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `feedback_id` int(11) NOT NULL DEFAULT '1',
  `feedback_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'I no longer want to receive these emails',
  `feedback_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`subscriber_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `red_webmaster_account`
--

DROP TABLE IF EXISTS `red_webmaster_account`;
CREATE TABLE IF NOT EXISTS `red_webmaster_account` (
  `webmaster_id` int(11) NOT NULL AUTO_INCREMENT,
  `webmaster_username` varchar(250) NOT NULL,
  `webmaster_password` varchar(250) NOT NULL,
  `webmaster_email_address` varchar(250) NOT NULL,
  `webmaster_status` tinyint(4) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`webmaster_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
