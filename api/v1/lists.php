<?php
require_once './config.php';
/**
* Contact List Management function.
* function name postadd with "List Name" as parameter.
* function name putrename with "List-ID" and "New List Name" as parameter.
* function name deleteremove with "List-ID" as parameter.
*/
class Lists {

	private $db;

    function __construct(){
        try {
            //Make sure you are using UTF-8
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

            //Update the dbname username and password to suit your server
            // $this->db = new PDO('mysql:host=localhost;dbname=redcappi', 'redcappi', 'redcappi', $options );
           // $this->db = new PDO('mysql:host=localhost;dbname=test', 'pravinjha', 'pravinjha', $options );
			$this->db = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_NAME, DB_USER, DB_PASSWORD, $options );
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            //If you are using older version of PHP and having issues with Unicode
            //uncomment the following line
            //$this->db->exec("SET NAMES utf8");

        } catch (PDOException $e) {
            throw new RestException(500, 'MySQL: ' . $e->getMessage());
        }
    }
	/**
	* Get Contact List(s).
	* If ID {id} is provided, response will be the list name
	* else, response will be all lists in json-encoded format.
	* @return array
	* param int $id  {@from body}
	* @url GET
	*/
	function get($id=0){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		if($id > 0)$individualListClause = " and `subscription_id`='$id' ";else $individualListClause ='';
		$sqlShowLists = "select `subscription_id`,`subscription_title` from `red_email_subscriptions` where `subscription_created_by` = '$mid' and `is_deleted`=0 $individualListClause";
			$rsShowLists = $this->db->query($sqlShowLists);

			if($rsShowLists->rowCount() <= 0){
				$this->log_api_event($mid, 'view_list', json_encode($id), "No List exists");
				throw new RestException(404, "No List exists");
			}else{
				foreach($rsShowLists as $row) {
					$strLid = $row['subscription_id'];
					$strLName = $row['subscription_title'];
					$retVal[$strLid] = $strLName;
				}
			}
			return  ($retVal);

	}
	/**
	* Add New Contact List.
	* Create a new contact list by passing the name {name} parameter.
	* @return array
	* @url POST /{name}/	{@from body}
	*/
	public function post($name) {
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");


		if($name ==''){
			$this->log_api_event($mid, 'add_list', json_encode($name), "Not a valid request");
			throw new RestException(400, "Not a valid request");
		}



		if(trim($name) == ''){
			$this->log_api_event($mid, 'add_list', json_encode($content), "List-name is required");
			throw new RestException(400, "List-name is required");
		}else{
			$sqlListExistsOrNot = "select `subscription_id` from `red_email_subscriptions` where `subscription_created_by` = '$mid' and `subscription_title`='$name' and `is_deleted`=0";
			$rsListExistsOrNot = $this->db->query($sqlListExistsOrNot);

			if($rsListExistsOrNot->rowCount() > 0){
				$this->log_api_event($mid, 'add_list', json_encode($name), "List-name already exists");
				throw new RestException(409, "List-name already exists");
			}else{
				try{
					$this->db->exec("insert into `red_email_subscriptions`(`subscription_title`,`subscription_created_by`) value('$name','$mid')");
				} catch(PDOException $ex) {
					$err = "An Error occured: " . $ex->getMessage();
					$this->log_api_event($mid, 'add_list', json_encode($name), $err);
					throw new RestException(500, $err);
				}
			}
			return array("code"=>201,"message"=>"List {$name} created with list-id ".$this->db->lastInsertId());
		}

	}
	/**
	* Rename Contact List.
	* Requires list ID {id} and new list name {name} as parameters.
	* param int $id
	* param string $name
	* @url POST
	*/
	function postRenameList($id=0, $name=''){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");

		if($id < 0 or $name==''){
			$this->log_api_event($mid, 'rename_list', json_encode(array($id,$name)), "Not a valid request");
			throw new RestException(400, "Not a valid request");
		}


		if(trim($name) == ''){
			$this->log_api_event($mid, 'rename_list', json_encode(array($id,$name)), "List-name is required");
			throw new RestException(400, "List-name is required");
		}else{
			if(!$this->validate_lists_ownership($id)){
				$this->log_api_event($mid, 'rename_list', json_encode(array($id,$name)), "Contact list(s) does not exist");
				throw new RestException(403, "Contact list(s) does not exist");
			}
			$sqlListExistsOrNot = "select `subscription_id` from `red_email_subscriptions` where `subscription_created_by` = '$mid' and `subscription_title`='$name' and `is_deleted`=0";
			$rsListExistsOrNot = $this->db->query($sqlListExistsOrNot);

			if($rsListExistsOrNot->rowCount() > 0){
				$this->log_api_event($mid, 'rename_list',  json_encode(array($id,$name)), "List-name already exists");
				throw new RestException(409, "List-name already exists");
			}else{
				try{
					$this->db->exec("update `red_email_subscriptions` set `subscription_title` = '$name' where `subscription_created_by`='$mid' and `subscription_id`='$id'");
				} catch(PDOException $ex) {
					$err = "An Error occured: " . $ex->getMessage();
					$this->log_api_event($mid, 'rename_list', json_encode($name), $err);
					throw new RestException(500, $err);
				}


			return array("code"=>200,"message"=>"List renamed to {$name}");
			}
		}
	}

	/**
	* Delete Contact List.
	* Enter the ID {id} of the list to be deleted. "All My Contacts" list cannot be deleted. Contacts from lists will remain upon list deletion.
	* param int $id  {@from body}
	* @return mixed
	*
	*/

	function delete($id) {
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");

		if(trim($id) == ''){
			$this->log_api_event($mid, 'remove_list', json_encode($id), "List-ID is required");
			throw new RestException(400, "List-ID is required");
		}else{
			if(!$this->validate_lists_ownership($id)){
				$this->log_api_event($mid, 'remove_list', json_encode($id), "Contact list(s) does not exist");
				throw new RestException(403, "Contact list does not exist");
			}
			$sqlListExistsOrNot = "select `subscription_id` from `red_email_subscriptions` where `subscription_created_by` = '$mid' and `subscription_id`='$id' and `is_deleted`=0";
			$rsListExistsOrNot = $this->db->query($sqlListExistsOrNot);
			if($rsListExistsOrNot->rowCount() <= 0){
				$this->log_api_event($mid, 'remove_list', json_encode($id), "List does not exist");
				throw new RestException(412, "List does not exist");
			}else{
				try{
					$this->db->exec("update `red_email_subscriptions` set `is_deleted` = '1' where `subscription_created_by`='$mid' and `subscription_id`='$id'");
				} catch(PDOException $ex) {
					$err = "An Error occured: " . $ex->getMessage();
					$this->log_api_event($mid, 'remove_list', json_encode($id), $err);
					throw new RestException(500, $err);
				}

			return array("code"=>200,"message"=>"List removed");
			}
		}
	}


	/**
	 *	Function Email_check
	 *
	 *	'email_check' controller function supporting email validation for import csv file
	 *
	 *	@param (string) (email)  contains email address of subscriber
	 *
	 *	@return (bool)  return true if email address validate true otherwise false
	 */




	private function log_api_event($mid, $action, $request, $error){
		$qryApiLog = "insert into `red_api_log` set `member_id`='$mid', `date_added`=now(), `api_action`='$action', `api_request`='$request', `api_error`='$error'";
		$this->db->exec($qryApiLog);

	}
	private function validate_lists_ownership($lists){
		$retval = true;
		$mid = AccessControl::$member_id;
		if(is_array($lists))$strLists = @implode(',',$lists);else $strLists = $lists;
		if($strLists !=''){
			$sqlValidateList = "select `subscription_id` from `red_email_subscriptions` where `subscription_id` in($strLists) and `subscription_created_by` = '$mid' and `is_deleted`=0";
			$stmt = $this->db->query($sqlValidateList);
			$row_count = $stmt->rowCount();

			if(count(explode(',',$strLists)) !== $row_count)
			$retval = false;
		}
		return $retval;

	}

}
