<?php
session_start();
require_once '../config.php';
?>
<html>
<head>
  <title>RedCappi API Explorer</title>
  <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'/>
  <link href='css/screen.css' media='screen' rel='stylesheet' type='text/css'/>
  <link href='css/style.css' media='screen' rel='stylesheet' type='text/css' />
  <script src='lib/jquery-1.8.0.min.js' type='text/javascript'></script>
  <script src='lib/jquery.slideto.min.js' type='text/javascript'></script>
  <script src='lib/jquery.wiggle.min.js' type='text/javascript'></script>
  <script src='lib/jquery.ba-bbq.min.js' type='text/javascript'></script>
  <script src='lib/handlebars-1.0.rc.1.js' type='text/javascript'></script>
  <script src='lib/underscore-min.js' type='text/javascript'></script>
  <script src='lib/backbone-min.js' type='text/javascript'></script>
  <script src='lib/swagger.js' type='text/javascript'></script>
  <script src="lib/hmac-sha256.js"></script>
  <script src="lib/enc-base64-min.js"></script>
  <script src='swagger-ui.js' type='text/javascript'></script>
  <script type="text/javascript">
    $(function () {
      window.swaggerUi = new SwaggerUi({
        discoveryUrl:"../resources.json",
        apiKey:"",
        dom_id:"swagger-ui-container",
        supportHeaderParams: true,
				headers: { Authorization: "RCWS <?php if(isset($_SESSION['public_key']))echo $_SESSION['public_key'];?>:"},
        supportedSubmitMethods: ['get', 'post', 'put', 'patch', 'delete'],
        onComplete: function(swaggerApi, swaggerUi){
			
        	if(console) {
            console.log("Loaded SwaggerUI")
            console.log(swaggerApi);
            console.log(swaggerUi);
          }
        },
        onFailure: function(data) {
        	if(console) {
            console.log("Unable to Load SwaggerUI");
            console.log(data);
          }
        },
        docExpansion: "none"
      });

      window.swaggerUi.load();
    });
function saveKeys(){
var pub_key = $('#pub_key').val();
var priv_key = $('#priv_key').val();

 $.ajax({
  url: "ajax.php",
  type:"POST",
  data:"mode=savekeys&pub_key="+pub_key+"&priv_key="+priv_key,
  success: function(data) {
		var arrData = data.split(':');
    if(arrData[0] =='error') {
		$('#user_keys').show();
		$('#show_user_keys').hide();
    $('#debug').html('<font color="red">'+arrData[1]+'</font>');
		setTimeout(function(){$('#debug').html('');} , 4000);
    }else{
		$('#user_keys').hide();
		$('#show_user_keys').show();
	  }
  }
  });
}
  </script>
</head>

<body>
  <div id='header'>
    <div class="swagger-ui-wrap"><nav>
      <a id="logo" href="#"><span id="logo-img"></span>RedCappi API Explorer</a>
      <?php if(isset($_SESSION['public_key']) and isset($_SESSION['private_key'])){?>
        <a href="./ajax.php?mode=logout" class="btn submit" id="logout">Logout</a>
      <?php } ?>
    </nav></div>
  </div>
  <div id="debug"></div>
  <?php if(!isset($_SESSION['public_key']) or !isset($_SESSION['private_key'])){?>
    <form action="./ajax.php" method="post" name="frmLogin">
      <input type="hidden" name="mode" value="savekeys" />
      <div id="user_keys_container">
        <h3 class="input-title">Enter your Public &amp; Private Keys:</h3>
        <div id="user_keys">
          <strong>Public Key: </strong><input type="text" name="pub_key" id="pub_key" size="100" value="<?php if(isset($_SESSION['public_key']))echo $_SESSION['public_key'];?>" /><br/>
          <strong>Private Key: </strong><input type="text" name="priv_key" id="priv_key" size="100" value="<?php if(isset($_SESSION['private_key']))echo $_SESSION['private_key'];?>" /><br/>
          <input type="submit" name="btnSaveKeys" value="Save Keys" class="btn submit" />
          <p>
            To generate API key, login in to your RedCappi account and go to the "Extra" section. <br/>Don't have an account? <a href='http://www.<?php echo SYSTEM_DOMAIN_NAME;?>/signup'>Signup here.</a>
          </p>
        </div>
      </div>
    </form>
  <?php } ?>
  <h3 class="input-title" style="padding-bottom:0">Output:</h3>
  <div id="message-bar" class="swagger-ui-wrap">&nbsp;</div>
  <div id="swagger-ui-container" class="swagger-ui-wrap"></div>
</body>
</html>
