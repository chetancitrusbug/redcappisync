<?php
session_start();
require_once '../config.php';
try {
	//Make sure you are using UTF-8
	$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');	
	$db = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_NAME, DB_USER, DB_PASSWORD, $options );
	$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {
	die('MySQL: ' . $e->getMessage());
}


if(isset($_POST['mode']) and $_POST['mode'] == 'savekeys'){
	$pub_key = $_POST['pub_key'];
	$priv_key = $_POST['priv_key'];
	
	#echo $pub_key;exit;
	$sqlMid = "select `member_id` from `red_member_api` where `public_key`='$pub_key' and `private_key`='$priv_key'";
	$rsMid = $db->query($sqlMid);		
	if($rsMid->rowCount() > 0){
		$_SESSION['public_key'] = $pub_key;
		$_SESSION['private_key'] = $priv_key;
		//echo 'success:ok'	;
		//exit;
		
	}else{	
		header("location:index.php?m=no");
	exit;	
	}	
	header("location:index.php?mode=no");
	//echo 'error:Invalid Keys'	;
	exit;		
}elseif(isset($_POST['mode']) and $_POST['mode'] == 'hash'){
	if(isset($_SESSION)){
		$httpMethod = $_POST['httpMethod'];
		$url = $_POST['url'];
		if(isset($_POST['posted_data']) and !is_null($_POST['posted_data']))
		$posted_data = json_decode($_POST['posted_data'], true);
		else
		$posted_data = array();
		$signature = createSignature($httpMethod, $url, $posted_data);
		if(!mb_check_encoding($signature, 'UTF-8')) $signature = utf8_encode($signature);
			$hash = hash_hmac('sha256', $signature, $_SESSION['private_key']);
		echo $hash;
	}else{
		echo 'error';
	}

exit;
}elseif(isset($_GET['mode']) and $_GET['mode'] == 'logout'){
	unset($_SESSION['public_key']);
	unset($_SESSION['private_key']);
	header("location:index.php?mode=no");	
	exit;
}


function createSignature($method, $url,$posted_data=array()){

		$arr_url = parse_url($url);
		if(isset($arr_url['query'])){
			$sortedQueryString = '';		
			if(trim($arr_url['query']) != ''){
				parse_str($arr_url['query'],$arrTemp);
				if(!empty($arrTemp)){
					@uksort($arrTemp, 'strcasecmp');
					$posted_data = array_merge((array)$posted_data, $arrTemp);
					$sortedQueryString =  http_build_query($arrTemp, '', '&');
				}			
			}		
			$api_url = $arr_url['path'].'?'.$sortedQueryString;		
		}else{
			$api_url = $arr_url['path'];
		} 
		
		if(!empty($posted_data)){
			@uksort($posted_data, "strcasecmp");
			//@uksort($posted_data, "strnatcasecmp");
 
			$signature = strtolower($method.'::'.$api_url.'::'.json_encode($posted_data));
		}else{
			$signature = strtolower($method.'::'.$api_url.'::null');
		}
		return $signature;
	
	}
?>