<?php
session_start();
?>
<html>
<head>
    <title>RedCappi API Explorer</title>
    
	<link rel="stylesheet" type="text/css" href="http://www.redcappi.com/webappassets/css/style.css?v=7-10-13"  media="screen" />
    <link href='css/screen.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='css/style.css' media='screen' rel='stylesheet' type='text/css' />
	
    <script src='lib/jquery-1.8.0.min.js' type='text/javascript'></script>
    <script src='lib/jquery.slideto.min.js' type='text/javascript'></script>
    <script src='lib/jquery.wiggle.min.js' type='text/javascript'></script>
    <script src='lib/jquery.ba-bbq.min.js' type='text/javascript'></script>
    <script src='lib/handlebars-1.0.rc.1.js' type='text/javascript'></script>
    <script src='lib/underscore-min.js' type='text/javascript'></script>
    <script src='lib/backbone-min.js' type='text/javascript'></script>
    <script src='lib/swagger.js' type='text/javascript'></script>
    <script src="lib/hmac-sha256.js"></script>
    <script src="lib/enc-base64-min.js"></script>
    <script src='swagger-ui.js' type='text/javascript'></script>
    

    <script type="text/javascript">
        $(function () {
            window.swaggerUi = new SwaggerUi({
                discoveryUrl:"../resources.json",
                apiKey:"",
                dom_id:"swagger-ui-container",
                supportHeaderParams: true,
				headers: { Authorization: "RCWS <?php if(isset($_SESSION['public_key']))echo $_SESSION['public_key'];?>:"},
                supportedSubmitMethods: ['get', 'post', 'put', 'patch', 'delete'],
                onComplete: function(swaggerApi, swaggerUi){
                	if(console) {
                        console.log("Loaded SwaggerUI")
                        console.log(swaggerApi);
                        console.log(swaggerUi);
                    }
                },
                onFailure: function(data) {
                	if(console) {
                        console.log("Unable to Load SwaggerUI");
                        console.log(data);
                    }
                },
                docExpansion: "none"
            });

            window.swaggerUi.load();
        });

    </script>
 
</head>


<body class="" itemscope itemtype="http://schema.org/WebApplication">
<!--[page html]-->
    <header>
      <div id="header-content">
        <a href="http://www.redcappi.com/" title="redcappi" id="logo"><img src="http://www.redcappi.com/webappassets/images/redesign/logo.png?v=6-20-13" alt="RedCappi" class="animated bounceInDown" /><span itemprop="author">RedCappi</span></a>    
		<?php if(isset($_SESSION['public_key']) and isset($_SESSION['private_key'])){?>
		<nav>
			<ul>            
            <li style="float:right;"><a href="./ajax.php?mode=logout" id="Logout" style="color:#ff0000;">Logout</a></li>
          </ul>
		</nav>
		<?php }?>
      </div>
    </header>
  <!--[/header]-->
  <section role="main" class="main-container content-page" itemscope>
  <h2 itemprop="headline">API Explorer</h2>
  <div class="content key-points">

 
 
<?php if(isset($_GET['m'])){?>
<div id="debug" onload="javascript:setTimeout(function(){$('#debug').html('');} , 4000);"><font color="red">Invalid Keys</font></div>
<?php } ?>
<?php if(!isset($_SESSION['public_key']) or !isset($_SESSION['private_key'])){?>
<form action="./ajax.php" method="post" name="frmLogin">
<h4 class="api_login_head">Enter your Public &amp; Private Keys</h4>
<input type="hidden" name="mode" value="savekeys" />
<div id="user_keys_container">
<div id="user_keys"  style="text-align:left;">
<strong>Public Key: </strong><input type="text" name="pub_key" id="pub_key" size="100" value="<?php if(isset($_SESSION['public_key']))echo $_SESSION['public_key'];?>" /><br/>
<strong>Private Key: </strong><input type="text" name="priv_key" id="priv_key" size="100" value="<?php if(isset($_SESSION['private_key']))echo $_SESSION['private_key'];?>" /><br/>
<input type="submit" name="btnSaveKeys" value="Save Keys" class="btn submit" />
</div>
<p class="api_login_text">To generate API key, login in to your RedCappi account and go to the "Extra" section. <br/>Don't have an account? <a href='http://www.redcappi.com/signup'>Signup here.</a></p>
<hr />
</div>
</form>
<?php } ?>
<div id="message-bar" class="swagger-ui-wrap">&nbsp;</div>
<div id="swagger-ui-container" class="swagger-ui-wrap"></div>
</div>
</section>
</body>
</html>
