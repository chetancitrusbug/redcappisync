<?php

class Home
{
    public function index()
    {
        return array(
            'success' => array(
                'code'    => 200,
                'message' => 'Restler is up and running!',
            ),
        );
    }

    /**
     * @return string
     *
     * @format HtmlFormat
     */
    public function create()
    {
        return '';
    }

    /**
     * @param array $file {@field file}{@type associative}
     *
     * @return array
     *
     * @format JsonFormat, UploadFormat
     */
    public function postUpload(array $file)
    {
        return $file;
    }

    /**
     * @param array $file {@field file}{@type associative}
     *
     * @return array
     *
     * @format JsonFormat, UploadFormat
     */
    public function putUpload(array $file)
    {
        return $file+['method'=>'put'];
    }

    /**
     * @param array $file {@field file}{@type associative}
     *
     * @return array
     *
     * @format JsonFormat, UploadFormat
     */
    public function patchUpload(array $file)
    {
        return $file+['method'=>'patch'];
    }
}