<?php
// Help from
// http://stackoverflow.com/questions/7818867/check-header-authorization-on-restler-api-framework
// http://s3.amazonaws.com/doc/s3-developer-guide/RESTAuthentication.html
require_once 'config.php';
use Luracast\Restler\iAuthenticate;
class AccessControl implements iAuthenticate{
	public static $member_id = 0;
	public static $posted_data = '';
	private $db;
	function __construct(){
        try {            
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');           
			$this->db = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_NAME, DB_USER, DB_PASSWORD, $options );
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new RestException(500, 'MySQL: ' . $e->getMessage());
        }
    }
    function __isAllowed(){	 
	
		$jsonVal = json_decode(file_get_contents('php://input'), true);
		if(empty($jsonVal)){
			$jsonVal = (is_array($this->restler->requestData))?$this->restler->requestData:json_decode($this->restler->requestData);
		}	
		//print_r($jsonVal['posted_data']);
		$headers = apache_request_headers();
 
		//echo "<pre>".print_r($_SERVER,true)."</pre>";
		//echo "<pre>".print_r($headers,true)."</pre>";
		//echo trim($headers['Authorization']);
		// echo 'pravin='.$_SERVER['REQUEST_METHOD'];
		$arrHash = @explode(':',trim($headers['Authorization']));	 
	 
		$strPublicKey = substr($arrHash[0],5);
		$strPrivateKey = $this->getPrivateKey($strPublicKey);
		//$strServerHash = $this->getServerHash($strPrivateKey,json_encode($jsonVal['posted_data']));		
		$strServerHash = $this->getServerHash($strPrivateKey,json_encode($jsonVal));		
		//echo ($strServerHash); 
	    if ($strServerHash == $arrHash[1] ) {
			static::$member_id = $this->getMemberId($strPublicKey);
			static::$posted_data = $jsonVal['posted_data'];
			return TRUE;            
        }else{
			throw new RestException(401, "Invalid Request");
			return false;
		}		
    }
	
	
	
	private function getPrivateKey($publickey){
		$sqlPvtKey = "select `private_key` from `red_member_api` where `public_key`='$publickey'"; 
		$rsPvtKey = $this->db->query($sqlPvtKey);		
		if($rsPvtKey->rowCount() > 0){
			foreach($rsPvtKey as $row) {
			return $row['private_key'];
			}
		}else{
			return '';
		}		
	}
	private function getMemberId($publickey){
		$sqlMid = "select `member_id` from `red_member_api` where `public_key`='$publickey'";
		$rsMid = $this->db->query($sqlMid);		
		if($rsMid->rowCount() > 0){
			foreach($rsMid as $row) {
			return $row['member_id'];
			}
		}else{
			return 0;
		}
	}
	
	private function getServerHash($privatekey, $posted_data){
	
		$arrPostedData = json_decode($posted_data,true);
	
		if(!empty($arrPostedData)){
			@uksort($arrPostedData, 'strcasecmp');
		}
		$strQueryString = $_SERVER['QUERY_STRING'];
		if(trim($strQueryString) != ''){
			parse_str($strQueryString,$arrTemp);
			if(!empty($arrTemp)){
				uksort($arrTemp, 'strcasecmp');
				$strQueryString = http_build_query($arrTemp, '', '&');
			}
		}
		//echo "<br/>====================================================================<br/>";
		if(empty($arrPostedData)) 
			$signature = strtolower($_SERVER['REQUEST_METHOD'].'::'.str_replace($_SERVER['QUERY_STRING'], $strQueryString, $_SERVER['REQUEST_URI']).'::null');
		else
			$signature = strtolower($_SERVER['REQUEST_METHOD'].'::'.str_replace($_SERVER['QUERY_STRING'], $strQueryString, $_SERVER['REQUEST_URI']).'::'.json_encode($arrPostedData)) ;	
		// echo "<br/>====================================================================<br/>";
		#$hash = base64_encode(hash_hmac('sha256', $signature, $privatekey));
		 $hash = (hash_hmac('sha256', $signature, $privatekey));
		return $hash;
	}
}