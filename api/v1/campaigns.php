<?php
require_once './config.php';

/**
* Auto Responder Management function.
* function name postadd with "Campaign Name" as parameter.
* function name putrename with "Campaign ID" and "Campaign Name" as parameter.
* function name deleteremove with "Campaign ID" as parameter.
*/
class Campaigns {

	private $db;

    function __construct(){
        try {
            //Make sure you are using UTF-8
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

            //Update the dbname username and password to suit your server
            // $this->db = new PDO('mysql:host=localhost;dbname=redcappi', 'redcappi', 'redcappi', $options );
           // $this->db = new PDO('mysql:host=localhost;dbname=test', 'pravinjha', 'pravinjha', $options );
			$this->db = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_NAME, DB_USER, DB_PASSWORD, $options );
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            //If you are using older version of PHP and having issues with Unicode
            //uncomment the following line
            //$this->db->exec("SET NAMES utf8");

        } catch (PDOException $e) {
			echo $e->getMessage();
            throw new RestException(500, 'MySQL: ' . $e->getMessage());
        }
    }
	
	
	/**
	* 	Send Campaigns.
	* 	Requires list ID {listid} to send campaigns, Email Address {senderEmail},and string of campaign{campaign} .
	
	* 	@param string $listid entered comma seprated value like 11600,115533122.
	*	@param string $campaignTitle Please enter campaign title {@from body}
	*	@param string $subject Please enter subject {@from body}
	*	@param string $senderEmail Please enter senderEmail {@type email} {@from body}
	*	@param string $senderName Please enter senderName {@from body}
	*	@param string $scheduleDate Please enter schedule date with format yyyy-mm-dd{@from body}
	*	@param string $scheduleTime Please enter schedule time h:i:s{@from body}
	*	@param string $campaign Please insert html with entity encode {@from body}
	* 	@url POST {listid}/campaign/
    */
	
	

	function sendcampaign($listid,$campaignTitle,$subject,$senderEmail,$senderName,$scheduleDate= '',$scheduleTime='',$campaign){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();
		
		$content = compact('campaignTitle','subject','senderEmail','senderName','campaign','scheduleDate','scheduleTime');
		if(empty($content)){
			$this->log_api_event($mid, 'send campaign', json_encode($content), "Not a valid request");
			throw new RestException(400, "Not a valid request");
		} 
		//print_r($content);
		$selectUser = "SELECT * FROM `red_members` as rm JOIN `red_countries` as rc ON `rm`.`country`=`rc`.`country_id` WHERE `member_id` = '$mid' limit 1 ";
		$stmt = $this->db->query($selectUser);
		$user = '';
		foreach($stmt as $value){
			$user = $value;
		}
		$subscription_ids =$listid;
		if(count($subscription_ids)>0){
			if(!(in_array('-'.$mid,$subscription_ids))){			
				$where = "and find_in_set(ress.subscription_id, $subscription_ids)";
			}
		}
		$select_subscriber = "SELECT count(distinct res.subscriber_id) as totContact FROM (`red_email_subscription_subscriber` as ress) RIGHT JOIN `red_email_subscribers` as res ON `res`.`subscriber_id` =`ress`.`subscriber_id` WHERE `res`.`subscriber_created_by` = '$mid' AND `res`.`subscriber_status` = 1 AND `res`.`is_deleted` = 0".$where;
		$stmt = $this->db->query($select_subscriber);
		foreach ($stmt as $value){
			$subscriber_count = $value['totContact'];
		}
		
		//$subscriber_count = 0;
		//$create_date = date('Y-m-d H:i:s');//,local_to_gmt(time()));
		if($content['scheduleDate'] != '' && $content['scheduleTime'] != ''){
			$date_schedule_send = $content['scheduleDate'].' '.$content['scheduleTime'];
			
		}else{
			$date_schedule_send = date('Y-m-d H:i:s');
			
		}
		$date_schedule = date('Y-m-d H:i:s');
		$input_array['campaign_status'] = 'queueing';
		$user_array['user_is_authentic']=$user['is_authentic'];
		$user_array['clicktracking_status']=$user['clicktracking_status'];
		$user_array['is_automatic_segmentation']=$user['is_automatic_segmentation'];
		$user_array['segment_size']=$user['segment_size'];
		$user_array['campaign_priority']=$user['campaign_priority'];
		$input_array['campaign_title'] = $content['campaignTitle'];
		$input_array['campaign_created_by']= $mid;
		$input_array['campaign_theme_id']= '-1';
		$input_array['campaign_template_id'] = '-1';
		$input_array['campaign_template_option'] = '4';
		$input_array['campaign_color_theme_id'] = '-1'; 
		$input_array['campaign_date_added'] = date('Y-m-d H:i:s');
		$input_array['campaign_content'] = str_replace("'","&#x27;",htmlentities($content['campaign']));
		$input_array['campaign_email_content'] = 'NULL';
		$input_array['campaign_text_content'] = 'NULL';	
		$input_array['email_subject'] = $content['subject'];
		$input_array['sender_email'] = $content['senderEmail'];
		$input_array['sender_name'] = $content['senderName'];
		$input_array['subscription_list'] = $listid;
		$input_array['click_url']= 'NULL';
		$input_array['campaign_after_encode_url'] = 'NULL';
		$input_array['campaign_sheduled'] = $date_schedule; 
		$input_array['campaign_queued'] = $date_schedule; 
		$input_array['email_send_date'] = $date_schedule_send; 
		$input_array['campaign_contacts'] = $subscriber_count;
		$input_array['is_status'] = '0';
		$input_array['campaign_priority'] = $user_array['campaign_priority']; 
		$input_array['is_clicktracking'] = $user_array['clicktracking_status'];
		$input_array['is_segmentation'] = '1';
		$input_array['number_of_contacts'] = $user_array['segment_size'];
		if($user_array['user_is_authentic']){
			$input_array['tobe_campaign_status'] = 'archived';
		}else{
			$input_array['tobe_campaign_status'] = 'ready';
		}

		//$input_array['segment_interval'] = '30';
		
		//print_r($input_array);exit;
		$columnName = '(';
		$columnValue = '(';
		foreach ($input_array as $key => $val){
			$columnName .= '`'.$key.'`,';
			$columnValue .= "'".$val."',";
		}
		$columnName .= '`segment_interval`)';
		$columnValue .= "'30')";
		
		$sql = "INSERT INTO `red_email_campaigns` ".$columnName.' values '.$columnValue.' '; 
		if(!$this->db->exec($sql)){
					
			$arrErr = $this->db->errorInfo();
			$err = "An Error occured: " . $arrErr[2];
			$this->log_api_event($mid, 'Add Campaign', json_encode($content), $err);
			return $arrErr;
			//throw new RestException(500, 'posted data is not in proper format.');
			//throw new RestException(403, 'Error while adding campaign.' );
		} 
		$last_inserted_id = $this->db->lastInsertId();
		//return $input_array;
		// Set segmentaion for campaign
		$sql_segmentation = "insert into `red_ongoing_segmentation` set campaign_id='$last_inserted_id', segment_size='".$user_array['segment_size']."', segment_interval='30' ON DUPLICATE KEY UPDATE `segment_size`='".$user_array['segment_size']."', `segment_interval`='30' ";
		if(!$this->db->exec($sql_segmentation)){
			return array("code"=>403,"message"=>"segmentaion for campaign");
		}
		//create_scheduled_campaign
		$sql_scheduled_campaign = "INSERT INTO `red_email_campaigns_scheduled` (`campaign_id`,`campaign_scheduled_date`) VALUES ('$last_inserted_id','$date_schedule_send')";
		//array(	'campaign_id'=>$campaign_id,'campaign_scheduled_date'=>$scheduled_datetime)
		if(!$this->db->exec($sql_scheduled_campaign)){
			return array("code"=>403,"message"=>"create_scheduled_campaign");
		}
		return array("code"=>200,"message"=>"Campaign Added","campaignId"=>$last_inserted_id);
		
	}
	

	/**
	* 	Edit Campaigns.
	* 	Requires campaignId to edit, Email Address {senderEmail},and string of campaign .
	* 	@param int $campaignId Enter Campaign Id.
	* 	@param string $listId entered comma seprated value like 11600,115533122.
	
	*	@param string $campaignTitle Please enter campaign title {@from body}
	*	@param string $subject Please enter subject {@from body}
	*	@param string $senderEmail Please enter senderEmail {@from body}
	*	@param string $senderName Please enter senderName {@from body}
	*	@param string $schedule_date Please enter schedule date with format yyyy-mm-dd{@from body}
	*	@param string $schedule_time Please enter schedule time h:i:s{@from body}
	*	@param string $campaign Please insert html with entity encode {@from body}
	* 	@url POST {campaignId}/editcampaign
    */
	
	

	function editCampaign($campaignId,$listId='',$campaignTitle='',$subject='',$senderEmail='',$senderName='',$schedule_date= '',$schedule_time='',$campaign=''){
		if($campaignId != ''){
			$mid = AccessControl::$member_id;
			if($mid < 1) throw new RestException(401, "Invalid Request");
			$campaignSql =  "select * from `red_email_campaigns` where `campaign_id` = '$campaignId' and campaign_created_by = '$mid' ";
			$stmt = $this->db->query($campaignSql);
			if(!$stmt){
				$arrErr = $this->db->errorInfo();
				$err = "An Error occured: " . $arrErr[2];
				$this->log_api_event($mid, 'Edit Campaign', json_encode($content), $err);
				//throw new RestException(500, 'posted data is not in proper format.');
				throw new RestException(403, 'Error while updating campaign.' );
			}
			$campaignData = array();
			foreach ($stmt as $key => $val){
				$campaignData = $val;
			} 
			if($campaignData['campaign_status'] != 'active'){
				$retVal = array();
			
				$content = compact('campaignTitle','subject','senderEmail','senderName','campaign','schedule_date','schedule_time');
			
				if(empty($content)){
					$this->log_api_event($mid, 'Edit campaign', json_encode($content), "Not a valid request");
					throw new RestException(400, "Not a valid request");
				} 
				
				//print_r($content);
				/*$selectUser = "SELECT * FROM `red_members` as rm JOIN `red_countries` as rc ON `rm`.`country`=`rc`.`country_id` WHERE `member_id` = '$mid' limit 1 ";
				$stmt = $this->db->query($selectUser);
				$user = '';
				foreach($stmt as $value){
					$user = $value;
				}*/
				if($listId != ''){
					$subscription_ids =implode(',',$listId);
					if(count($subscription_ids)>0){
						if(!(in_array('-'.$mid,$subscription_ids))){			
							$where = "and find_in_set(ress.subscription_id, $subscription_ids)";
						}
					}
					$select_subscriber = "SELECT count(distinct res.subscriber_id) as totContact FROM (`red_email_subscription_subscriber` as ress) RIGHT JOIN `red_email_subscribers` as res ON `res`.`subscriber_id` =`ress`.`subscriber_id` WHERE `res`.`subscriber_created_by` = '$mid' AND `res`.`subscriber_status` = 1 AND `res`.`is_deleted` = 0".$where;
					$stmt = $this->db->query($select_subscriber);
					foreach ($stmt as $value){
						$subscriber_count = $value['totContact'];
						$input_array['campaign_contacts'] = $subscriber_count;
					}
					$input_array['subscription_list'] = $listId;
				}
				if($content['schedule_date'] != '' && $content['schedule_time'] != ''){
					$date_schedule_send = $content['schedule_date'].' '.$content['schedule_time'];
					
				}else{
					$date_schedule_send = date('Y-m-d H:i:s');
					
				}
				$input_array['campaign_status'] = 'queueing';
				$date_schedule = date('Y-m-d H:i:s');
				/*$user_array['user_is_authentic']=$user['is_authentic'];
				$user_array['clicktracking_status']=$user['clicktracking_status'];
				$user_array['is_automatic_segmentation']=$user['is_automatic_segmentation'];
				$user_array['segment_size']=$user['segment_size'];
				$user_array['campaign_priority']=$user['campaign_priority'];
				$input_array['is_clicktracking'] = $user_array['clicktracking_status'];
				$input_array['number_of_contacts'] = $user_array['segment_size'];
				$input_array['campaign_priority'] = $user_array['campaign_priority']; 
				$input_array['campaign_sheduled'] = $date_schedule;
				$input_array['campaign_theme_id']= '-1';
				$input_array['campaign_template_id'] = '-1';
				$input_array['campaign_template_option'] = '4';
				$input_array['campaign_color_theme_id'] = '-1'; 
				$input_array['campaign_date_added'] = date('Y-m-d H:i:s');
				$input_array['campaign_email_content'] = 'NULL';
				$input_array['campaign_text_content'] = 'NULL';	
				$input_array['click_url']= 'NULL';
				$input_array['campaign_after_encode_url'] = 'NULL';
				$input_array['is_status'] = '0';
				$input_array['is_segmentation'] = '1';
				
				*/
				
				$input_array['campaign_title'] = $content['campaignTitle'];
				$input_array['campaign_created_by']= $mid;
				$input_array['campaign_content'] = str_replace("'","&#x27;",htmlentities($content['campaign']));
				$input_array['email_subject'] = $content['subject'];
				$input_array['sender_email'] = $content['senderEmail'];
				$input_array['sender_name'] = $content['senderName'];
				
				$input_array['campaign_queued'] = $date_schedule; 
				$input_array['email_send_date'] = $date_schedule_send; 
				//$input_array['segment_interval'] = '30';
				//print_r($input_array);die();
				$qry = '';
				foreach ($input_array as $key => $val){
					if($val != '')
					$qry .= "`".$key."` = '".str_replace("'","&#x27;",$val)."' ,";  
				}
				$qry .= " `segment_interval` = '30'";
				$sqlUpdate = "update `red_email_campaigns` set ".$qry." where campaign_id = '$campaignId'";
				if(!$this->db->exec($sqlUpdate)){
						
					$arrErr = $this->db->errorInfo();
					$err = "An Error occured: " . $arrErr[2];
					$this->log_api_event($mid, 'Edit Campaign', json_encode($content), $err);
					return $arrErr;
					//throw new RestException(500, 'posted data is not in proper format.');
					//throw new RestException(403, 'Error while adding campaign.' );
				} 
				//return $input_array;
				// Set segmentaion for campaign
				//create_scheduled_campaign
				$sql_scheduled_campaign = "update `red_email_campaigns_scheduled` set `campaign_scheduled_date` = '$date_schedule_send' where campaign_id = '$campaignId' ";
				
				$this->db->exec($sql_scheduled_campaign);
				//array(	'campaign_id'=>$campaign_id,'campaign_scheduled_date'=>$scheduled_datetime)*/
			
				return array("code"=>200,"message"=>"Campaign Updated");				
			}else{
				throw new RestException(403, 'Campaign already sent.' );
			}
		}else{
			throw new RestException(403, 'Error while updating campaign.' );
		}
	}
	/**
	* Get Campaign(s).
	* If ID {id} is provided, response will be the campaign name,campaign date
	* else, response will be all lists in json-encoded format.
	* @return array
	* param int $id  {@from body}
	* @url GET
	*/
	function get($id=0){
		
		$mid = AccessControl::$member_id;
		
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();
		echo $mid;
		if($id > 0)$individualListClause = " and `campaign_id`='$id' ";else $individualListClause ='';
		$sqlShowLists = "SELECT `campaign_id`,`campaign_title`,`campaign_date_added`,`campaign_created_by`,`is_deleted` FROM `red_email_campaigns` where `campaign_created_by`='$mid' and `is_deleted`=0 $individualListClause order by `campaign_id` desc";
		#echo $sqlShowLists;exit;
			$rsShowLists = $this->db->query($sqlShowLists);

			if($rsShowLists->rowCount() <= 0){
				$this->log_api_event($mid, 'view_Campaign', json_encode($id), "No Campaign exists");
				throw new RestException(404, "No Campaign exists");
			}else{
				foreach($rsShowLists as $row => $value) {
					
					$strLid = $value['campaign_id'];
					$camp_name= $value['campaign_title'];
					$camp_date= $value['campaign_date_added'];
					
					$retVal[$row]['campaign_id'] = $strLid;
					$retVal[$row]['campaign_name'] = $camp_name;
					$retVal[$row]['campaign_date_added'] = $camp_date;
				}
			}
			#echo "<pre>";print_r(array_values($retVal));exit;
			return  ($retVal);

	}	
	/**
	* Get Open date Campaigns.
	* Requires Campaign ID {cid}.
	* @return array
    * param int $id  {@from body}
    * @url GET {cid}/getopendate
    */
	function getopendate($cid){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		
		if($cid > 0)$campaignClause = "and `campaign_id`=$cid";else $campaignClause='';

		$sqlShowContacts = "SELECT `campaign_id`,`user_id`,`subscriber_id`,`subscriber_email_address`,`email_track_read_date`
		FROM `red_email_track` WHERE `user_id`=$mid $campaignClause order by `campaign_id` desc";
		#echo $sqlShowContacts;exit;
			$rsShowContacts = $this->db->query($sqlShowContacts);

			if($rsShowContacts->rowCount() <= 0){
				$this->log_api_event($mid, 'Search_Campaigns', json_encode($cid), "No Campaign exists");
				throw new RestException(404, "No Campaign exists");
			}else{
				foreach($rsShowContacts as $row => $val) {
				#	echo "<pre>";print_r($row);
					$strCid = $val['subscriber_id'];
					if($val['email_track_read_date'] != NULL){
						$email_track_read_date = $val['email_track_read_date'];
					}else{
						$email_track_read_date = "";
					}
					
					$retVal[$row]['open_date'] = $email_track_read_date;
					$retVal[$row]['contact_id'] = $val['subscriber_id'];
					$retVal[$row]['contact_email_address'] = $val['subscriber_email_address']; 
				}
			}
			
			//print_r($retVal);exit;
			return ($retVal);

	}
	
	/**
	* Get Click date Campaigns.
	* Requires Campaign ID {cid}.
	* @return array
    * param int $id  {@from body}
    * @url GET {cid}/getclickdate
    */
	function getclickdate($cid){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		
		if($cid > 0)$campaignClause = "and `campaign_id`=$cid";else $campaignClause='';

		$sqlShowContacts = "SELECT `campaign_id`,`user_id`,`subscriber_id`,`subscriber_email_address`,`date_click` FROM `red_email_track` WHERE `user_id`=$mid $campaignClause order by `campaign_id` desc";
		#echo $sqlShowContacts;
			$rsShowContacts = $this->db->query($sqlShowContacts);
			if($rsShowContacts->rowCount() <= 0){
				$this->log_api_event($mid, 'Search_Campaigns', json_encode($cid), "No Campaign exists");
				throw new RestException(404, "No Campaign exists");
			}else{
				foreach($rsShowContacts as $row => $val) {
					//echo "<pre>";print_r($val);
					$strCid = $val['subscriber_id'];
					if($val['date_click'] != NULL){
						$click = $val['date_click'];
						$retVal[$row]['click_date'] = $click;
						$retVal[$row]['contact_id'] = $val['subscriber_id'];
						$retVal[$row]['contact_email_address'] = $val['subscriber_email_address']; 
					}else{
						//throw new RestException(200, "Not Found");
					} 
					/* $retVal[$row]['click_date'] = $click;
					$retVal[$row]['contact_id'] = $val['subscriber_id'];
					$retVal[$row]['contact_email_address'] = $val['subscriber_email_address']; */ 
				}
			}
			
			//print_r($retVal);exit;
			return ($retVal);

	}
	
	/**
	* Get Forward date Campaigns.
	* Requires Campaign ID {cid}.
	* @return array
    * param int $id  {@from body}
    * @url GET {cid}/getforwarddate
    */
	function getforwarddate($cid){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		
		if($cid > 0)$campaignClause = "and `campaign_id`=$cid";else $campaignClause='';

		$sqlShowContacts = "SELECT `campaign_id`,`user_id`,`subscriber_id`,`subscriber_email_address`,`date_forward`
		FROM `red_email_track` WHERE `user_id`=$mid $campaignClause order by `campaign_id` desc";
		#echo $sqlShowContacts;exit;
			$rsShowContacts = $this->db->query($sqlShowContacts);

			if($rsShowContacts->rowCount() <= 0){
				$this->log_api_event($mid, 'Search_Campaigns', json_encode($cid), "No Campaign exists");
				throw new RestException(404, "No Campaign exists");
			}else{
				foreach($rsShowContacts as $row => $val) {
				#	echo "<pre>";print_r($row);
					$strCid = $val['subscriber_id'];
					if($val['date_forward'] != NULL){
						$date_forward = $val['date_forward'];
					}else{
						$date_forward = "";
					}
					$retVal[$row]['forward_date'] = $date_forward;
					$retVal[$row]['contact_id'] = $val['subscriber_id'];
					$retVal[$row]['contact_email_address'] = $val['subscriber_email_address']; 
				}
			}
			
			//print_r($retVal);exit;
			return ($retVal);

	}

	
	/**
	* Get Unsubscriber Campaigns.
	* Requires Campaign ID {cid}.
	* @return array
    * param int $id  {@from body}
    * @url GET {cid}/getunsubscriber
    */
	function getunsubscriber($cid){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		
		if($cid > 0)$campaignClause = "and `campaign_id`=$cid";else $campaignClause='';

		$sqlShowContacts = "SELECT `campaign_id`,`user_id`,`subscriber_id`,`subscriber_email_address`,`email_track_read_date`,`email_track_unsubscribes`
		FROM `red_email_track` WHERE `user_id`=$mid and email_track_unsubscribes = 1 $campaignClause order by `subscriber_id` desc";
		#echo $sqlShowContacts;exit;
			$rsShowContacts = $this->db->query($sqlShowContacts);

			if($rsShowContacts->rowCount() <= 0){
				$this->log_api_event($mid, 'Search_Campaigns', json_encode($cid), "No Unsubscriber exists");
				throw new RestException(404, "No Unsubscriber exists");
			}else{
				foreach($rsShowContacts as $row => $val) {
					$strCid = $val['subscriber_id'];
					
					$retVal[$row]['Campaign ID'] = $val['campaign_id'];
					$retVal[$row]['Contact ID'] = $val['subscriber_id'];
					$retVal[$row]['Contact Email Address'] = $val['subscriber_email_address']; 
				}
			}
			
			return  ($retVal);

	}
	
	/**
	* Get Campaign states.
	* Requires Campaign ID {cid}.
	* @return array
    * param int $id  {@from body}
    * @url GET {cid}/getcampaignstate
    */
	function getcampaignstate($cid){
		
		$mid = AccessControl::$member_id;
		
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();
		
		
		if($cid > 0)
		{
			$sqlShowLists = "SELECT `campaign_id` FROM `red_email_campaigns` where `campaign_created_by`='$mid' and `is_deleted`=0  and `campaign_id`='$cid'  order by `campaign_id` desc";
			$rsShowLists = $this->db->query($sqlShowLists);
			if($rsShowLists->rowCount() <= 0){
				throw new RestException(200, "Not Found");
			}else{
				$campaignClause = "and `campaign_id`=$cid";
				// Delivered count
				$total_delivered_emails_sql = "SELECT count(distinct subscriber_id) as totrec FROM (`red_email_track`) WHERE `campaign_id` = $cid" ;
				$total_delivered_emails = $this->db->query($total_delivered_emails_sql);
				foreach ($total_delivered_emails as $key => $sent){
					$total_delivered_emails_count = $sent['totrec'];
				}
				$stateCampaign['Sent'] = $total_delivered_emails_count;
				// Opened count
				$total_read_emails_sql = "SELECT count(distinct subscriber_id) as totrec FROM (`red_email_track`) WHERE `campaign_id` = $cid AND `email_track_read` = 1 ";
				$total_read_emails = $this->db->query($total_read_emails_sql);
				foreach ($total_read_emails as $key => $read){
					$total_read_emails_count = $read['totrec'];
				}
				$stateCampaign['Opened'] = $total_read_emails_count;
				// Complaints count
				$total_complaint_emails_sql = "SELECT count(distinct subscriber_id) as totrec FROM (`red_email_track`) WHERE `campaign_id` = $cid AND `email_track_complaint` = 1 ";
				$total_complaint_emails = $this->db->query($total_complaint_emails_sql);
				foreach ($total_complaint_emails as $key => $complaint){
					$total_complaint_emails_count = $complaint['totrec'];
				}
				$stateCampaign['Complaints'] = $total_complaint_emails_count;
				// Bounced count
				$total_bounce_emails_sql = "SELECT count(distinct subscriber_id) as totrec FROM (`red_email_track`) WHERE `campaign_id` = $cid AND `email_track_bounce` > 0";
				$total_bounce_emails = $this->db->query($total_bounce_emails_sql);
				foreach ($total_bounce_emails as $key => $bounce){
					$total_bounce_emails_count = $bounce['totrec'];
				}
				$stateCampaign['Bounced'] = $total_bounce_emails_count;
				// Forwards count
				$total_forward_emails_sql = "SELECT count(distinct subscriber_id) as totrec FROM (`red_email_track`) WHERE `campaign_id` = $cid AND `email_track_forward` > 0";
				$total_forward_emails = $this->db->query($total_forward_emails_sql);
				foreach ($total_forward_emails as $key => $forward){
					$total_forward_emails_count = $forward['totrec'];
				}
				$stateCampaign['Forwards'] = $total_forward_emails_count;
				// Unopened count
				$stateCampaign['Unopened'] = $total_delivered_emails_count - $total_read_emails_count - $total_bounce_emails_count;
				// Clicks and Unsubscribes count
				$total_click_emails_sql = "SELECT * FROM (`red_email_campaigns_scheduled`) WHERE `campaign_id` = '19619'";
				$total_click_emails = $this->db->query($total_click_emails_sql);
				foreach ($total_click_emails as $key => $value){
					$total_click_emails_count = $value['email_track_click'] != '' ? $value['email_track_click'] : '0' ;
					$Unsubscribes = $value['email_track_unsubscribes'] != '' ? $value['email_track_unsubscribes'] : '0';
					
				}
				$stateCampaign['Clicks'] = $total_click_emails_count;
				$stateCampaign['Unsubscribes'] = $Unsubscribes;
				
				return $stateCampaign;
			}  
		}else{
			throw new RestException(401, "Invalid Request");
		}

		

	}
	
	private function log_api_event($mid, $action, $request, $error){
		$qryApiLog = "insert into `red_api_log` set `member_id`='$mid', `date_added`=now(), `api_action`='$action', `api_request`='$request', `api_error`='$error'";
		$this->db->exec($qryApiLog);

	}
	
	private function validate_lists_ownership($lists){
		$retval = true;
		$mid = AccessControl::$member_id;
		if(is_array($lists))$strLists = @implode(',',$lists);else $strLists = $lists;
		if($strLists !=''){
			$sqlValidateList = "select `subscription_id` from `red_email_subscriptions` where `subscription_id` in($strLists) and `subscription_created_by` = '$mid' and `is_deleted`=0";
			$stmt = $this->db->query($sqlValidateList);
			$row_count = $stmt->rowCount();

			if(count(explode(',',$strLists)) !== $row_count)
			$retval = false;
		}
		return $retval;

	}

}
