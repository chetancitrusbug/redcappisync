<?php
require_once './config.php';
/**
* Contact List Management function.
* function name postadd with "Signupform Name" as parameter.
* function name putrename with "Signup Form ID" and "Signup Form ID" as parameter.
* function name deleteremove with "Signup Form ID" as parameter.
*/
class signupforms	 {

	private $db;

    function __construct(){
        try {
            //Make sure you are using UTF-8
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

            //Update the dbname username and password to suit your server
            // $this->db = new PDO('mysql:host=localhost;dbname=redcappi', 'redcappi', 'redcappi', $options );
           // $this->db = new PDO('mysql:host=localhost;dbname=test', 'pravinjha', 'pravinjha', $options );
			$this->db = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_NAME, DB_USER, DB_PASSWORD, $options );
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            //If you are using older version of PHP and having issues with Unicode
            //uncomment the following line
            //$this->db->exec("SET NAMES utf8");

        } catch (PDOException $e) {
            throw new RestException(500, 'MySQL: ' . $e->getMessage());
        }
    }
	/**
	* Get SignupForm List(s).
	* If ID {id} is provided, response will be the signupform name
	* else, response will be all signupform in json-encoded format.
	* @return array
	* param int $id  {@from body}
	* @url GET
	*/
	function get($id=0){
		
		$mid = AccessControl::$member_id;
		
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		if($id > 0)$individualListClause = " and `id`='$id' ";else $individualListClause ='';
		$sqlShowLists = "select `id`,`form_name` from `red_signup_form` where `member_id` = '$mid' and `is_deleted`=0 $individualListClause order by `id` desc";
			$rsShowLists = $this->db->query($sqlShowLists);

			if($rsShowLists->rowCount() <= 0){
				$this->log_api_event($mid, 'view_signupform', json_encode($id), "No Signupform exists");
				throw new RestException(404, "No Signupform exists");
			}else{
				foreach($rsShowLists as $row => $key) {
					$strLid = $key['id'];
					$strLName = $key['form_name'];
					$retVal[$row]['id'] = $strLid;
					$retVal[$row]['name'] = $strLName;
				}
			}
			
			return  ($retVal);

	}
	
	/**
	* Get SignupForm Submission(s) And value(s).
	* Requires SignupForm ID {id}.
	* @return array
	* param int $id  {@from body}
	* @url GET {id}/getvalue
	*/
	function getvalue($id){
		#echo $id;exit;
		$mid = AccessControl::$member_id;
		
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		if($id > 0)$individualListClause = " and s.id=$id ";else $individualListClause ='';
		$sqlShowLists = "select s.fld_sequence,s.id,s.member_id,e.subscriber_id,e.subscriber_first_name,e.subscriber_last_name,e.subscriber_email_address,e.subscriber_state,e.subscriber_zip_code,e.subscriber_extra_fields,e.subscriber_company,e.subscriber_address,e.subscriber_city,e.subscriber_country,e.subscriber_date_added,e.is_deleted from red_signup_form s
		inner join red_email_subscribers e on e.signup_form_id=s.id
		where s.member_id=$mid and e.is_signup=1 and e.is_deleted=0 $individualListClause";
		
			$rsShowLists = $this->db->query($sqlShowLists);
			
			if($rsShowLists->rowCount() <= 0){
				$this->log_api_event($mid, 'view_signupform', json_encode($id), "No Signupform exists");
				throw new RestException(404, "No Signupform exists");
			}else{
				$i=1;
				foreach($rsShowLists as $row => $value) {
					#echo "<pre>";print_r($value);
					$fieldsequence = unserialize($value['fld_sequence']);
					$fieldvalue = unserialize($value['subscriber_extra_fields']);
					
					$strLid = $value['id'];
					$sub_id = $value['subscriber_id'];
					$retVal[$row]['signup_form_id'] = $strLid;
					$retVal[$row]['subscriber_id'] = $sub_id;
					if($value['subscriber_first_name'] != ""){
						$retVal[$row]['name'] = $value['subscriber_first_name'];
					}
					if($value['subscriber_last_name'] != ""){
						$retVal[$row]['lastname'] = $value['subscriber_last_name'];
					}
					$retVal[$row]['email'] = $value['subscriber_email_address'];
					if($value['subscriber_state']){
						$retVal[$row]['state'] = $value['subscriber_state'];
					}
					if($value['subscriber_zip_code']){
						$retVal[$row]['zip'] = $value['subscriber_zip_code'];
					}
					if($value['subscriber_company']){
						$retVal[$row]['company'] = $value['subscriber_company'];
					}
					if($value['subscriber_address']){
						$retVal[$row]['address'] = $value['subscriber_address'];
					}
					if($value['subscriber_city']){
						$retVal[$row]['country'] = $value['subscriber_country'];
					}
					$retVal[$row]['added_date'] = $value['subscriber_date_added'];
					
					if(count($fieldvalue)>0){
						$retVal[$row]['extra_fields'] = $fieldvalue;
					}
					
					
			$i++;	}
			}
			
			return  ($retVal);

	}
	

	/**
	 *	Function Email_check
	 *
	 *	'email_check' controller function supporting email validation for import csv file
	 *
	 *	@param (string) (email)  contains email address of subscriber
	 *
	 *	@return (bool)  return true if email address validate true otherwise false
	 */




	private function log_api_event($mid, $action, $request, $error){
		$qryApiLog = "insert into `red_api_log` set `member_id`='$mid', `date_added`=now(), `api_action`='$action', `api_request`='$request', `api_error`='$error'";
		$this->db->exec($qryApiLog);

	}
	private function validate_lists_ownership($lists){
		$retval = true;
		$mid = AccessControl::$member_id;
		if(is_array($lists))$strLists = @implode(',',$lists);else $strLists = $lists;
		if($strLists !=''){
			$sqlValidateList = "select `subscription_id` from `red_email_subscriptions` where `subscription_id` in($strLists) and `subscription_created_by` = '$mid' and `is_deleted`=0";
			$stmt = $this->db->query($sqlValidateList);
			$row_count = $stmt->rowCount();

			if(count(explode(',',$strLists)) !== $row_count)
			$retval = false;
		}
		return $retval;

	}

}
