<?php
/**
* Contact  Management function.
* function name postadd with "List Name" as parameter.
* function name putrename with "List-ID" and "New List Name" as parameter.
* function name deleteremove with "List-ID" as parameter.
*/


class Contacts {

	private $db;

    function __construct(){
        try {
            //Make sure you are using UTF-8
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
			$this->db = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_NAME, DB_USER, DB_PASSWORD, $options );
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new RestException(500, 'MySQL: ' . $e->getMessage());
        }
		
    }

	/**
	* GET Contact Detail.
	* Requires contact list ID {lid} and contact ID {cid}.
	* @return array
    * param int $lid  {@from body}
    * param int $cid  {@from body}
    * @url GET {lid}/contact/{cid}
    */
	function getContact($lid=0, $cid){
		
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		if($lid > 0)$individualListClause = " and `subscription_id`='$lid' ";else $individualListClause ='';

		$sqlShowContacts = "select `subscriber_id`,`subscriber_email_address` from `red_email_subscribers` where `subscriber_created_by` = '$mid' and `is_deleted`=0  and `subscriber_id`='$cid' limit 1";
		$rsShowContacts = $this->db->query($sqlShowContacts);

		if($rsShowContacts->rowCount() <= 0){
			$this->log_api_event($mid, 'view_contact', json_encode($lid), "No contact exists");
			throw new RestException(404, "No contact exists");
		}else{
			foreach($rsShowContacts as $row => $val) {
				$strCid = $val['subscriber_id'];
				$strContactEmail = $val['subscriber_email_address'];
				$retVal[$row]['id'] = $strCid;
				$retVal[$row]['email'] = $strContactEmail;
			}
		}
		return  ($retVal);
	}
	
	
	
	/**
	* GET Contacts from a Contact List.
	* Requires list ID {lid}. By default offset will start from 0 and page-size is 20.
	* Page-size {pagesize} can not be more than 100.
	* Offset {offset} can be used to get the next set of contacts from same contact list.
	* @return array
	* param int $lid  {@from body}
    * param int $offset  {@from body}
    * param int $pagesize  {@from body}
    * @url GET {lid}/contacts
	*
    */
	function getContacts($lid, $offset=0, $pagesize=20){
		$mid = AccessControl::$member_id;
		$pagesize = ($pagesize == 0 or $pagesize > 100)?20:$pagesize;

		if($mid < 1) throw new RestException(401, "Invalid Request222");
		$retVal = array();

		if($lid > 0){
		$sqlShowContacts = "select c.`subscriber_id`,c.`subscriber_email_address` from `red_email_subscribers` c inner join `red_email_subscription_subscriber` l ON c.subscriber_id=l.subscriber_id where c.`subscriber_created_by` = '$mid' and c.`is_deleted`=0 and l.subscription_id='$lid' order by c.`subscriber_id` desc limit {$offset},{$pagesize}";
		}else{
		$sqlShowContacts = "select c.`subscriber_id`,c.`subscriber_email_address` from `red_email_subscribers` c where c.`subscriber_created_by` = '$mid' and c.`is_deleted`=0  order by c.`subscriber_id` desc limit {$offset},{$pagesize}";
		}
		
		$rsShowContacts = $this->db->query($sqlShowContacts);

		if($rsShowContacts->rowCount() <= 0){
			$this->log_api_event($mid, 'view_contact', json_encode($lid), "No contact exists");
			throw new RestException(404, "No contact exists");
		}else{
			foreach($rsShowContacts as $row => $val) {
				$strCid = $val['subscriber_id'];
				$strContactEmail = $val['subscriber_email_address'];
				$retVal[$row]['id'] = $strCid;
				$retVal[$row]['email'] = $strContactEmail;
			}
		}
		return  ($retVal);

	}
	
	/**
	* Search Contact.
	* Requires contact list ID {lid} and Email Address {email_address}.
	* @return array
    * param int $lid  {@from body}
    * param int $email_address  {@from body}
    * @url GET {lid}/searchcontact/{email_address}
    */
	function searchContact($lid=0,$email_address){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		if($lid != 0)$individualListClause = " and `subscriber_email_address` LIKE '%$email_address%'";else $individualListClause ='';

		$sqlShowContacts = "select `subscriber_id`,`subscriber_email_address`,`subscriber_created_by` from `red_email_subscribers` where `subscriber_created_by` = '$mid' and `is_deleted`=0 $individualListClause order by `subscriber_id` desc";
		#echo $sqlShowContacts;exit;
		$rsShowContacts = $this->db->query($sqlShowContacts);

		if($rsShowContacts->rowCount() <= 0){
			$this->log_api_event($mid, 'Search_contact', json_encode($lid), "No contact exists");
			throw new RestException(404, "No contact exists");
		}else{
			foreach($rsShowContacts as $row => $val) {
				$strCid = $val['subscriber_id'];
				$strContactEmail = $val['subscriber_email_address'];
				$retVal[$row]['subscriber_id'] = $strCid;
				$retVal[$row]['subscriber_email'] = $strContactEmail;
			}
		}
		return  ($retVal);
	}

	/**
	* Add Contact.
	* Requires list ID {id} to add contacts and json string of the contact array.
	* @param int $id	
	* @param string $email_address  {@type email} {@from body}
	* @param string $first_name  {@from body}
	* @param string $last_name  {@from body}
	* @param string $name  {@from body}
	* @param string $state  {@from body}
	* @param string $zip_code  {@from body}
	* @param string $country  {@from body}
	* @param string $city  {@from body}
	* @param string $company  {@from body}
	* @param string $dob  {@from body}
	* @param string $phone  {@from body}
	* @param string $address  {@from body}	
	* @param array $custom Please insert data like [{"key1":"value1"},{"key2":"value2"}]{@type array} {@from body} 
	* @param array $global Please insert data like [{"key1":"value1"},{"key2":"value2"}]{@type array} {@from body} 
	* @url POST {id}/contacts/
	*/
	public function postaddcontact($id=0,$email_address, $first_name='', $last_name='', $name='', $state='', $zip_code='', $country='', $city='', $company='', $dob='', $phone='', $address='',$custom='',$global='') 
	{
		
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");

		$content = compact('email_address', 'first_name', 'last_name', 'name', 'state', 'zip_code', 'country', 'city', 'company', 'dob', 'phone', 'address','custom','global');
		
	 	if(empty($content)){
			$this->log_api_event($mid, 'add_contacts', json_encode($content), "Not a valid request");
			throw new RestException(400, "Not a valid request");
		}

		if(!$this->validate_lists_ownership($id)){
			$this->log_api_event($mid, 'add_contacts', json_encode($content), "Contact list(s) does not exist");
			throw new RestException(403, "Contact list(s) does not exist");
		} else if(!$this->validate_segment_ownership($id)){
			$this->log_api_event($mid, 'segment_list', json_encode($content), "Invalid List ID: Adding contacts to segmented lists is not allowed");
			throw new RestException(403, "Invalid List ID: Adding contacts to segmented lists is not allowed");
		} else{
			try{
				$qry = "INSERT INTO red_email_subscribers SET ";
				$flds = '';   
				/*foreach ($content as $key=>$val) {
					$flds .= 'subscriber_'.$key . ' = \'' . mysql_real_escape_string($this->fixEncoding(trim($val))) . '\', ';  
				}
				*/
				foreach ($content as $key=>$val) {
					if($key	!= 'custom' && $key != 'global' )
					{
						$flds .= 'subscriber_'.$key . ' = \'' . ($this->fixEncoding(trim($val))) . '\', ';
					}else{
						if($key	== 'custom') {
							$customkey = $key;
							$customval = $val;
						}
						if($key	== 'global'){
							$globalkey = $key;
							$globalval = $val;
						}
					}
				}
				
				$flds .=  'subscriber_created_by = ' . $mid ;
				
				$qry .=  $flds .' ON DUPLICATE KEY UPDATE ' . $flds . ', is_deleted = 0, subscriber_id=LAST_INSERT_ID(subscriber_id)';
				
				
				//$this->db->exec($qry);                                          
				if(!$this->db->exec($qry)){
					
					$arrErr = $this->db->errorInfo();
					$err = "An Error occured: " . $arrErr[2];
					$this->log_api_event($mid, 'update_contact', json_encode($content), $err);
					//throw new RestException(500, 'posted data is not in proper format.');
					throw new RestException(403, 'Email already exists -'.$err );
				} 
				$last_inserted_id = $this->db->lastInsertId(); 
				 

				if ($last_inserted_id > 0){
					if($id >0){
						$queryAddContactToList = "insert ignore into `red_email_subscription_subscriber`(`subscriber_id`,`subscription_id`) value('$last_inserted_id','$id')";
						$this->db->exec($queryAddContactToList);
					}
					
					$segment = $this->get_segment_list('`res`.`subscription_created_by` =  '.$mid.'
AND `res`.`is_deleted` =  0');
					
						foreach($segment as $s_key => $s_val){
							$segmentdatafetchcondition = $this->get_segmentation_data('subscription_id = '.$s_val,$mid);
							
							$segmentdatafetchcondition .= 'AND res.subscriber_id = '.$last_inserted_id;
							
							$subscriptionData =  $this->get_segment_subscription_data($segmentdatafetchcondition);
							
							if(count($subscriptionData) > 0){
								foreach($subscriptionData as $sub => $subval){
									
									$subscriber_id = $subval['subscriber_id'];
									$AddContactToList = "insert into `red_email_subscription_subscriber`(`subscriber_id`,`subscription_id`) VALUES('$subscriber_id','$s_val')";
									
									$this->db->exec($AddContactToList);
									
								}
							} 
									
						} 
					
				} 
				
				
				
				
				
				if($customkey == "custom"){
					foreach($customval as $val_key => $custom_val){
						if(!empty ($custom_val) && $custom_val != ''){
							foreach($custom_val as $value){
								$val_custom = $value;
							}
							if($val_custom != ''){
								$date = date('Y-m-d H:i:s');
								$key_custom = key($custom_val);
								$customquery = "INSERT INTO red_subsciber_extra_fields (`member_id`,`subscriber_id`,`key`,`is_global`,`date_added`) VALUES ($mid,$last_inserted_id,'$key_custom',0,'$date')";
								//echo $customquery;exit;
								$this->db->exec($customquery);
								$ref_id = $this->db->lastInsertId();
								if($ref_id > 0){
									$ref_query = "INSERT INTO red_extra_fields_ref (`ex_ref_id`,`value`) VALUES ($ref_id,'$val_custom')";
									$this->db->exec($ref_query);
								}
							}
						}
					}
				}  
				
               /**Start Code  by CB**/
				if($globalkey == "global"){
					foreach($globalval as $val_key => $global_val){
						if(!empty($global_val) && $global_val != ''){
							foreach($global_val as $value){
								$val_global = $value;
							}
							if($val_global != ''){
								$date = date('Y-m-d H:i:s');
								$key_global = key($global_val);
								
								$globalquery = "INSERT INTO red_global_fields (`member_id`,`global_key`,`date`) VALUES ($mid,'$key_global','$date')";
								//echo $customquery;exit;
								$this->db->exec($globalquery);
								$ref_id = $this->db->lastInsertId();
								if($ref_id > 0){
									$ref_query = "INSERT INTO red_global_fields_ref (`glob_ref_id`,`value`,`subscriber_id`) VALUES ($ref_id,'$val_global',$last_inserted_id)";
									$this->db->exec($ref_query);
								} 
								/*foreach($custom_val as $custom_key){
									echo "<pre>";print_r($custom_val);
								} */
							}
						}
					}
				} 
				/** End Code  by CB**/ 
                $arrEml = explode( '@',$content['email_address']);
				$eml_domain = $arrEml[1];
				$is_signup_sql = "select signup_flag_enabled from red_member_api where member_id = $mid";
				$this->log_api_event($mid, 'add_contact', json_encode($is_signup_sql), $err);
				$rs_is_signup_flag = $this->db->query($is_signup_sql);
				$is_signup_flag = 0;
			 
				foreach($rs_is_signup_flag as $row => $val){
					$is_signup_flag = $val['signup_flag_enabled'];
				}
			 
				$qryupdate = "update red_email_subscribers set is_signup = $is_signup_flag , subscriber_email_domain = '" . $eml_domain . "' where subscriber_id = $last_inserted_id";
				$numrows = $this->db->exec($qryupdate);
                                                                     

			} catch(PDOException $ex) {
				$err = "An Error occured: " . $ex->getMessage();
				$this->log_api_event($mid, 'update_contact', json_encode($content), $err);
				throw new RestException(500, $err);
			}
		}
	return array("contact_id"=>$last_inserted_id,"email_address"=>$email_address);
    }

	/**
	* Modify a Contact.
	* Requires list ID {lid}, contact ID {cid} and json string of contact array as parameter.
	* @param int $lid
	* @param int $cid
	* @param string $email_address  {@type email} {@from body}
	* @param string $first_name  {@from body}
	* @param string $last_name  {@from body}
	* @param string $name  {@from body}
	* @param string $state  {@from body}
	* @param string $zip_code  {@from body}
	* @param string $country  {@from body}
	* @param string $city  {@from body}
	* @param string $company  {@from body}
	* @param string $dob  {@from body}
	* @param string $phone  {@from body}
	* @param string $address  {@from body}
	* @param array $custom Please insert data like [{"id1":"value1"},{"id2":"value2"}]{@type array} {@from body} 
	* @param array $global Please insert data like [{"id1":"value1"},{"id2":"value2"}]{@type array} {@from body} 
	* @url POST {lid}/contacts/{cid}
	*/
	public function postUpdateContact($lid, $cid, $email_address, $first_name, $last_name, $name, $state, $zip_code, $country, $city, $company, $dob, $phone, $address, $custom = '', $global='') {
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$content = compact('email_address', 'first_name', 'last_name', 'name', 'state', 'zip_code', 'country', 'city', 'company', 'dob', 'phone', 'address','custom','global');
		
		//$posted_data = array('address' => $address,'city' => $city,'company' => $company,'country' => $country,'dob' => $dob,'email_address' => $email_address,'first_name' => $first_name,'last_name' => $last_name,'name' => $name,'phone' => $phone,'state' => $state,'zip_code' => $zip_code );
		//$content = json_decode($posted_data, true);

		if(empty($content)){
			$this->log_api_event($mid, 'update_contact', json_encode($content), "Not a valid request");
			throw new RestException(401, "Not a valid request");
		}elseif(!$this->validate_lists_ownership($lid)){
			$this->log_api_event($mid, 'update_contact', json_encode($posted_data), "Contact list(s) does not exist");
			throw new RestException(403, "Contact list(s) does not exist");
		}else{
			try{
				$qry = "UPDATE `red_email_subscribers` SET ";
				$flds = '';
				foreach ($content as $key=>$val){
					if($key != 'custom' && $key != 'global'){
						if($val != '')
							$flds .= 'subscriber_'.$key . ' = \'' . ($this->fixEncoding(trim($val))) . '\', ';
					}else{
						if($key == 'custom'){
							$customkey = $key;
							$customval = $val;
						}
						if($key == 'global'){
							$globalkey = $key;
							$globalval = $val;
						}
					}
				}
				$flds .=  'subscriber_created_by = ' . $mid ;
				$qry .=  $flds ." where subscriber_created_by = '$mid' and subscriber_id='$cid'";
				if(!$this->db->exec($qry)){
				$arrErr = $this->db->errorInfo();
				$err = "An Error occured: " . $arrErr[2];
				$this->log_api_event($mid, 'update_contact', json_encode($content), $err);
				throw new RestException(500, $err);
				}
				
				
				$segment = $this->get_segment_list('`res`.`subscription_created_by` =  '.$mid.'
								AND `res`.`is_deleted` =  0');
					
						foreach($segment as $s_key => $s_val){
							$segmentdatafetchcondition = $this->get_segmentation_data('subscription_id = '.$s_val,$mid);
							
							$segmentdatafetchcondition .= 'AND res.subscriber_id = '.$cid;
							
							$subscriptionData =  $this->get_segment_subscription_data($segmentdatafetchcondition);
							
							if(count($subscriptionData) > 0){
								foreach($subscriptionData as $sub => $subval){
									
									$subscriber_id = $subval['subscriber_id'];
									$AddContactToList = "insert into `red_email_subscription_subscriber`(`subscriber_id`,`subscription_id`) VALUES('$subscriber_id','$s_val')";
									
									$this->db->exec($AddContactToList);
									
								}
							} 
									
						} 
				
				if($customkey == "custom"){
					foreach($customval as $val_key => $custom_val){
						foreach($custom_val as $value){
							$val_custom = $value;
						}
						if($val_custom != ''){
							$date = date('Y-m-d H:i:s');
							$key_custom = key($custom_val);
							$selectCustom = "SELECT `id` from  `red_subsciber_extra_fields` where `id` = '".$key_custom."' and `subscriber_id` = '$cid'";
							$stmt = $this->db->query($selectCustom);
							$row_count = $stmt->rowCount();
							if($row_count > 0){
								$customquery = "Update`red_extra_fields_ref` SET `value` = '".$val_custom."' where `ex_ref_id` = '".$key_custom."'";
								$this->db->exec($customquery);
							}else{
								throw new RestException(403, "Custom field does not exist");
							}
						}
					}
				}
				
				if($globalkey == "global"){
					foreach($globalval as $val_key => $global_val){
						foreach($global_val as $value){
							$val_global = $value;
						}
						if($val_global != ''){
							$date = date('Y-m-d H:i:s');
							$key_global = key($global_val);
							$selectglobal = "SELECT `id` from  `red_global_fields` where `id` = '".$key_global."' and `member_id` = '$mid'";
							$stmt = $this->db->query($selectglobal);
							$row_count = $stmt->rowCount();
							if($row_count > 0){
								$globalquery = "Update`red_global_fields_ref` SET `value` = '".$val_global."' where `glob_ref_id` = '".$key_global."' and `subscriber_id` = '$cid'";
								if(!$this->db->exec($globalquery)){
									$ref_query = "INSERT INTO red_global_fields_ref (`glob_ref_id`,`value`,`subscriber_id`) VALUES ($key_global,'$val_global',$cid)";
									$this->db->exec($ref_query);
								}
							}else{
								throw new RestException(403, "Global field does not exist"); 
							}
						}
					}
				}
				
				
			} catch(PDOException $ex) {
				$err = "An Error occured: " . $ex->getMessage();
				$this->log_api_event($mid, 'update_contact', json_encode($content), $err);
				throw new RestException(500, $err);
			}
		}


	return array("code"=>200,"message"=>"Contact updated");
    }
	
	
	/**
	* GET Custom Fields Detail according to Contact.
	* Requires contact list ID {lid} and contact ID {cid}.
	* @return array
    * param int $lid  {@from body}
    * param int $cid  {@from body}
    * @url GET {lid}/custom/{cid}
    */
	function getContactCustomFields($lid=0, $cid){
		
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
			$retVal = array();
		if('-'.$mid == $lid){
			$selectCustom = "SELECT * from  `red_subsciber_extra_fields` where `subscriber_id` = '$cid' and `member_id` = '$mid'";
			$stmt = $this->db->query($selectCustom);
			$row_count = $stmt->rowCount();
			if($row_count >0 ){
				foreach($stmt as $key=>$val){
					$retval[$key]['custom_id'] = $val['id'];
					$retval[$key]['custom_key'] = $val['key'];
				}
			}else{
				$this->log_api_event($mid, 'view_custom_fileds', json_encode($lid), "No Custom fields exists");
				throw new RestException(404, "No Custom fields exists");
			}
			return $retval;
		}else{
			throw new RestException(404, "Please enter proper list Id");
		}
	}
	
	
	/**
	* GET Global Fields Detail..
	* Requires list ID {lid}.
	* @return array
	* param int $lid  {@from body}
    * @url GET {lid}/global
	*
    */
	function getContactGlobalFields($lid){
		
		$mid = AccessControl::$member_id;
		
		if($mid < 1) throw new RestException(401, "Invalid Request");
		
		$retVal = array();
		if('-'.$mid == $lid){
			$selectglobal = "SELECT * from  `red_global_fields` where `member_id` = '$mid' ";
			$stmt = $this->db->query($selectglobal);
			$row_count = $stmt->rowCount();
			if($row_count >0 ){
				foreach($stmt as $key=>$val){
					$retval[$key]['global_id'] = $val['id'];
					$retval[$key]['global_key'] = $val['global_key'];
				}
			}else{
				$this->log_api_event($mid, 'view_global_fileds', json_encode($lid), "No Global fields exists");
				throw new RestException(404, "No Global fields exists");
			}
			return $retval;
		}else{
			throw new RestException(404, "Please enter proper list Id");
		}
	}
	
	/**
	* Delete a Contact.
	* Requires list ID {lid} and contact ID {cid}.
	* @param int $lid
	* @param int $cid
	* @url DELETE {lid}/contacts/{cid}
	*/
	public function deleteContact($lid, $cid) {
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		if(!$this->validate_lists_ownership($lid)){
			$this->log_api_event($mid, 'remove_contacts', json_encode(array($lid,$cid)), "Contact list(s) does not exist");
			throw new RestException(403, "Contact list(s) does not exist");
		}

		if(intval($lid) > 0 and intval($cid) > 0){
			if($this->db->exec("delete from `red_email_subscription_subscriber` where `subscription_id`='$lid' and `subscriber_id`='$cid'") > 0)
				return 'Contact removed';
			else{
				$this->log_api_event($mid, 'remove_contacts', json_encode(array($lid,$cid)), "Not a valid request");
				throw new RestException(403, "Not authorized");
			}
		}elseif(intval($cid) > 0){
			if($this->db->exec("update `red_email_subscribers` set `is_deleted`=1 where `subscriber_id`='$cid' and `subscriber_created_by`='$mid'") > 0)
				return 'Contact removed';
			else{
				$this->log_api_event($mid, 'remove_contacts', json_encode(array($lid,$cid)), "Not a valid request");
				throw new RestException(403, "Not authorized");
			}
		}else{
		$this->log_api_event($mid, 'remove_contacts', json_encode(array($lid,$cid)), "Not a valid request");
		throw new RestException(400, "Not a valid request");
		}
		return array("code"=>200,"message"=>"Contact deleted");
	}


	/**
	 *	Function Email_check
	 *
	 *	'email_check' controller function supporting email validation for import csv file
	 *
	 *	@param (string) (email)  contains email address of subscriber
	 *
	 *	@return (bool)  return true if email address validate true otherwise false
	 */
	private function checkEmail($email){
		$email = trim($email);
		if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $email)){
			return false;
		}else{
			return true;
		}
	}
	private function fixEncoding($in_str) {
            //$in_str = addslashes($in_str);
            $in_str = str_replace("'","''", $in_str);
            $in_str = str_replace("(","",$in_str);
            $in_str = str_replace(")","",$in_str);
            
        $cur_encoding = mb_detect_encoding($in_str);
        if($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")) {
            return $in_str;
        } else {
            return utf8_encode($in_str);
        }
    }


	private function log_api_event($mid, $action, $request, $error){
		$qryApiLog = "insert into `red_api_log` set `member_id`='$mid', `date_added`=now(), `api_action`='$action', `api_request`='$request', `api_error`='$error'";
		$this->db->exec($qryApiLog);

	}
	private function validate_lists_ownership($lists){
		$retval = true;
		$mid = AccessControl::$member_id;
		if(is_array($lists))$strLists = @implode(',',$lists);else $strLists = $lists;
		if($strLists !=''){
			$sqlValidateList = "select `subscription_id` from `red_email_subscriptions` where `subscription_id` in($strLists) and `subscription_created_by` = '$mid' and `is_deleted`=0";
			$stmt = $this->db->query($sqlValidateList);
			$row_count = $stmt->rowCount();

			if(count(explode(',',$strLists)) !== $row_count)
			$retval = false;
		}
		
		return $retval;

	}
	
	
	private function validate_segment_ownership($lists){
		$retval = true;
		
		$mid = AccessControl::$member_id;
		if(is_array($lists))$strLists = @implode(',',$lists);else $strLists = $lists;
		if($strLists !=''){
			$sqlValidateList = "select `subscription_type` from `red_email_subscriptions` where `subscription_id` in($strLists) and `subscription_created_by` = '$mid' and `is_deleted`=0";
			
			$stmt = $this->db->query($sqlValidateList);
			foreach($stmt as $key=>$val){
				$subscription_type = $val['subscription_type'];
			}
			if($subscription_type != 0 && $subscription_type != 4){
				$retval = false;
			} 
		}
		//echo '##'.$retval;exit;
		return $retval;

	}
	
	
	private function validate_list_name_ownership($lists){
		$retval = true;
		$mid = AccessControl::$member_id;
		
		if(is_array($lists))$strLists = @implode(',',$lists);else $strLists = $lists;
		
		if($strLists !=''){
			$sqlValidateList = "select `subscription_title` from `red_email_subscriptions` where `subscription_title` in('$strLists') and `subscription_created_by` = '$mid' and `is_deleted`=0";
			
			$stmt = $this->db->query($sqlValidateList);
			$row_count = $stmt->rowCount();

			if(count(explode(',',$strLists)) !== $row_count)
			$retval = false;
		}
		
		return $retval;

	}
	
	private function get_segment_list($where){
		$rows=array();
		$sql = 'SELECT DISTINCT ress.sg_subscription_id
			FROM (`red_segmentation` as ress)
			JOIN `red_email_subscriptions` as res ON `res`.`subscription_id` =`ress`.`sg_subscription_id`
			WHERE '.$where;
		
		$result=$this->db->query($sql);		
		foreach($result as $row => $val){
			$finalArray[]= $val['sg_subscription_id'];
		}	
		return $finalArray;
	}
	
	
	private function get_segmentation_data($where,$mid){
		$rows = array();
		$sql = 'SELECT *
				FROM (`red_email_subscriptions` as res)
				LEFT JOIN `red_segmentation` as sg ON `res`.`subscription_id` = `sg`.`sg_subscription_id`
				WHERE '.$where;
			
		$result=$this->db->query($sql);	
		foreach($result as $row => $val){
			$rows[$val['sg_ref_id']]['sg_ref_id']=$val['sg_ref_id'];
			$rows[$val['sg_ref_id']]['sg_segment_key']=$val['sg_segment_key'];
			$rows[$val['sg_ref_id']]['sg_segment_val']=$val['sg_segment_val'];
			$rows[$val['sg_ref_id']]['sg_segment_condition']=$val['sg_segment_condition'];
		}	

		$swhere = "res.subscriber_created_by = ".$mid." and res.subscriber_status = 1 and res.is_deleted = 0";
			$i = 1;
		foreach($rows as $key => $s_val){
			if($s_val['sg_segment_condition'] == 'is'){
				$s_val['sg_segment_condition'] = 'LIKE';
			}else{
				$s_val['sg_segment_condition'] = 'NOT LIKE';
			}
			$finalfields = $s_val['sg_segment_key'];
			$fieldnameArray = explode('_',$s_val['sg_segment_key']);
			
			$operator = $s_val['sg_segment_condition'];
			$fieldvalue = $s_val['sg_segment_val'];
			if($fieldnameArray[0] == 'subscriber'){
				$whereArray[$i] = ' AND '.$finalfields .' '.$operator .' "%'.$fieldvalue.'%"';
			}
			if($fieldnameArray[0] == 'global'){
				if(count($fieldnameArray) > 2){
					array_shift($fieldnameArray);
					$fieldnameArray[1] =  implode('_',$fieldnameArray);
				}
				$whereArray[$i] = ' AND gl.global_key = "'.$fieldnameArray[1].'" AND ref.value  '. $operator  .' "%'.$fieldvalue.'%"';
			}
			if($fieldnameArray[0] == 'cust'){
				if(count($fieldnameArray) > 2){
					array_shift($fieldnameArray);
					$fieldnameArray[1] =  implode('_',$fieldnameArray);
				}
				$whereArray[$i] = ' AND ress.key = "'.$fieldnameArray[1].'" AND eref.value  '. $operator  .' "%'.$fieldvalue.'%" ';
			}
			$i++;	
		}
		
		foreach ($whereArray as $wh){
				$swhere .= $wh;
		}
		
		
		return $swhere;		
		
	}
	
	private function get_segment_subscription_data($where){
		$rows = array();
		$sql = 'SELECT DISTINCT res.subscriber_id
				FROM (`red_email_subscribers` as res)
				LEFT JOIN `red_subsciber_extra_fields` as ress ON `res`.`subscriber_id` =`ress`.`subscriber_id`
				LEFT JOIN `red_extra_fields_ref` as eref ON `ress`.`id` =`eref`.`ex_ref_id`
				LEFT JOIN `red_global_fields_ref` as ref ON `res`.`subscriber_id` =`ref`.`subscriber_id`
				LEFT JOIN `red_global_fields` as gl ON `ref`.`glob_ref_id` =`gl`.`id`
				WHERE '.$where;
			
		$result=$this->db->query($sql);	
		foreach($result as $row){
			 $rows[] = $row;
		}			
		return $rows;		
	}
	
	
	

}
