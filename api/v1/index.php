<?php
/*
Title: Documentation
Tagline: Commenting the classes can be more rewarding
Tags: create, retrieve, read, update, delete, post, get, put, routing, doc, production, debug
Requires: PHP >= 5.3
Description: How to document and let your users explore your API.
We have modified SwaggerUI to create 
[Restler API Explorer](https://github.com/Luracast/Restler-API-Explorer)
which is used [here](explorer/index.html#!/authors-v1).

[![Restler API Explorer](../resources/explorer1.png)](explorer/index.html#!/authors-v1)

We are progressively improving the Authors class from CRUD example 
to Rate Limiting Example to show Best Practices and Restler 3 Features.

Make sure you compare them to understand.


In order to provide access to those protected methods we use a class that
implements `iAuthenticate`. Also note that An Authentication class is also an
API class so all public methods that does not begin with `_` will be exposed as
API for example [SimpleAuth::key](simpleauth/key). It can be used to create
login/logout methods.

Example 1: GET restricted returns

{
  "error": {
    "code": 401,
    "message": "Unauthorized"
  }
}

 Example 2: GET restricted?key=rEsTlEr2 returns "protected method"

 Example 3: GET secured?key=rEsTlEr2 returns "protected class"
*/
session_start();
error_reporting(0);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, PATCH, DELETE');
header('Access-Control-Max-Age: 1000');
if(array_key_exists('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', $_SERVER)) {
    header('Access-Control-Allow-Headers: '
           . $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
} else {
    header('Access-Control-Allow-Headers: *');
}

require_once './lib/restler.php';
require_once './config.php';
use Luracast\Restler\Restler;
use Luracast\Restler\Defaults;
use Luracast\Restler\Filter\RateLimit;
//use Luracast\Restler\Format;

Defaults::$cacheClass = 'SessionCache';
Defaults::$useUrlBasedVersioning = false;


//Defaults::$smartAutoRouting = false;
//RateLimit::$usagePerUnit = 10000;
Defaults::$throttle = 20; //time in milliseconds for bandwidth throttling

spl_autoload_register('spl_autoload');

$r = new Restler(true,true);
//$r->setAPIVersion(1);
$r->addAPIClass('Resources'); //this creates resources.json at API Root
$r->addAPIClass('lists'); //map it to root
$r->addAPIClass('contacts'); //map it to root
$r->addAPIClass('campaigns'); //map it to root
$r->addAPIClass('signupforms'); //map it to root
$r->addAPIClass('autoresponders'); //map it to root
$r->addFilterClass('RateLimit');
//$r->setSupportedFormats('JsonpFormat','JsonFormat', 'XmlFormat', 'UploadFormat');
//$r->setOverridingFormats('HtmlFormat','UploadFormat','JsonFormat');
$r->addAuthenticationClass('AccessControl');
$r->handle();

 