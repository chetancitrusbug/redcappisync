<?php
require_once './config.php';
/**
* Auto Responder Management function.
* function name postadd with "Auto-Responder Name" as parameter.
* function name putrename with "Auto-Responder ID" and "Auto-Responder Name" as parameter.
* function name deleteremove with "Auto-Responder ID" as parameter.
*/
class Autoresponders {

	private $db;

    function __construct(){
        try {
            //Make sure you are using UTF-8
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

            //Update the dbname username and password to suit your server
            // $this->db = new PDO('mysql:host=localhost;dbname=redcappi', 'redcappi', 'redcappi', $options );
           // $this->db = new PDO('mysql:host=localhost;dbname=test', 'pravinjha', 'pravinjha', $options );
			$this->db = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_NAME, DB_USER, DB_PASSWORD, $options );
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            //If you are using older version of PHP and having issues with Unicode
            //uncomment the following line
            //$this->db->exec("SET NAMES utf8");

        } catch (PDOException $e) {
            throw new RestException(500, 'MySQL: ' . $e->getMessage());
        }
    }
	/**
	* Get AutoResponder(s).
	* If ID {id} is provided, response will be the AutoResponder name
	* else, response will be all lists in json-encoded format.
	* @return array
	* param int $id  {@from body}
	* @url GET
	*/
	function get($id=0){
		
		$mid = AccessControl::$member_id;
		
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();
		
		if($id > 0)$individualListClause = " and `id`='$id' ";else $individualListClause ='';
		$sqlShowLists = "select `group_name`,`id` from `red_autoresponder_group` where `autoresponder_created_by` = '$mid' and `is_deleted`=0 $individualListClause order by `id` desc";
		
			$rsShowLists = $this->db->query($sqlShowLists);

			if($rsShowLists->rowCount() <= 0){
				$this->log_api_event($mid, 'view_autoresponder', json_encode($id), "No AutoResponder exists");
				throw new RestException(404, "No AutoResponder exists");
			}else{
				foreach($rsShowLists as $row => $key) {
					$strLid = $key['id'];
					$strLName = $key['group_name'];
					$retVal[$row]['id'] = $strLid;
					$retVal[$row]['name'] = $strLName;
				}
			}
			
			return  ($retVal);

	}
	
	/**
	* Get Open date Auto-Responder.
	* Requires Auto-Responder ID {aid}.
	* Requires Campaign ID {cid}.
	* @return array
    * param int $name  {@from body}
    * @url GET {aid}/getopendate/{cid}
    */
	function getopendate($aid,$cid){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		if($aid > 0)$individualListClause = "and a.`autoresponder_group_id`=$aid";else $individualListClause ='';
		if($cid > 0)$campaignClause = "and   a.`campaign_id`=$cid";else $campaignClause='';

		$sqlShowContacts = "SELECT e.`campaign_id`,e.`user_id`,e.`subscriber_id`,e.`subscriber_email_address`,e.`email_track_read_date`,e.`email_sent`,e.`email_delivered`,a.`autoresponder_group_id`,a.`campaign_id`
		FROM `red_email_autoresponders` a
		INNER JOIN `red_email_track` e on e.`campaign_id`= a.`campaign_id`
		WHERE e.`user_id` = $mid and e.`email_sent`=1 and e.`email_delivered`=1 $individualListClause $campaignClause";
		
			$rsShowContacts = $this->db->query($sqlShowContacts);

			if($rsShowContacts->rowCount() <= 0){
				$this->log_api_event($mid, 'Search_autoresponder', json_encode($aid), "No autoresponder exists");
				throw new RestException(404, "No autoresponder exists");
			}else{
				foreach($rsShowContacts as $row) {
					//echo "<pre>";print_r($row);
					 $strCid = $row['id'];
					//$campaign_id = $row['campaign_id'];
					$retVal[$strCid] = $row['campaign_id'];
				}
			}
			#echo "<pre>";print_r($retVal);exit;
			return  ($retVal);

	}
	
	


	/**
	* GET Campaign from a Auto-Responder.
	* Requires Auto-Responder ID {aid}. 
	* @return array
	* param int $aid  {@from body}
	* @url GET {aid}/getCampaignforAuto
	*
    */
	function getCampaignforAuto($aid){
		
		$mid = AccessControl::$member_id;
		
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();
		
		if($aid > 0)$individualListClause = " and `autoresponder_group_id`='$aid' ";else $individualListClause ='';
		$sqlShowLists = "select `campaign_id`,`autoresponder_group_id`,`campaign_title` from `red_email_autoresponders` where `campaign_created_by` = '$mid' and `is_deleted`=0 $individualListClause order by `campaign_id` desc";
		
			$rsShowLists = $this->db->query($sqlShowLists);

			if($rsShowLists->rowCount() <= 0){
				$this->log_api_event($mid, 'view_autoresponder', json_encode($aid), "No AutoResponder exists");
				throw new RestException(404, "No AutoResponder exists");
			}else{
				foreach($rsShowLists as $row => $key) {
					$strLid = $key['campaign_id'];
					$strLName = $key['campaign_title'];
					$retVal[$row]['campaign_id'] = $strLid;
					$retVal[$row]['campaign_title'] = $strLName;
				}
			}
			
			return  ($retVal);

	}
	
	/**
	* Search Auto-Responder.
	* Requires Auto-Responder Name {name}.
	* @return array
    * param string $name  {@from body}
    * @url GET {name}/searchAuto
    */
	function searchAutoResponder($name){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		if($name != NULL)$individualListClause = " and `group_name` LIKE '%$name%'";else $individualListClause ='';

		$sqlShowContacts = "select a.`group_name`,a.`id`,r.`campaign_id`,r.`campaign_title` from `red_autoresponder_group` a left join `red_email_autoresponders` r on a.`id` = r.`autoresponder_group_id` where a.`autoresponder_created_by` = '$mid' and a.`is_deleted`=0 ".$individualListClause;
		#echo $sqlShowContacts;exit;
			$rsShowContacts = $this->db->query($sqlShowContacts);

			if($rsShowContacts->rowCount() <= 0){
				$this->log_api_event($mid, 'Search_autoresponder', json_encode($lid), "No autoresponder exists");
				throw new RestException(404, "No autoresponder exists");
			}else{
				$i = 0;
				foreach($rsShowContacts as $row => $val) {
					
					#echo "<pre>";print_r($val);
					 $strCid = $val['id'];
					//$campaign_id = $row['campaign_id'];
					$retVal[$strCid]['autoresponder_id'] = $val['id'];
					$retVal[$strCid]['autoresponder_name'] = $val['group_name'];
						
					if(count($val['campaign_id'])>0){
						$retVal[$strCid]['campaign'][$i]['id'] = $val['campaign_id'];
						$retVal[$strCid]['campaign'][$i]['title'] = $val['campaign_title'];
						$i++;
					}
				}
			}
			//echo "<pre>";print_r($retVal);exit;
			$finalARray = array_values($retVal);

			return  ($finalARray);

	}
	
	/**
	* Get Unsubscriber Auto-Responder.
	* Requires Auto-Responder ID {aid}.
	* @return array
    * param int $aid  {@from body}
    * @url GET {aid}/getunsubscriber
    */
	function getunsubscriber($aid){
		$mid = AccessControl::$member_id;
		if($mid < 1) throw new RestException(401, "Invalid Request");
		$retVal = array();

		
		if($aid > 0)$campaignClause = "and a.`autoresponder_group_id` = $aid";else $campaignClause='';
		
			
		$sqlShowContacts = "select a.`campain_id`,a.`autoresponder_group_id`,e.`campaign_id`,e.`email_track_unsubscribes`,
		e.`subscriber_id`,e.`subscriber_email_address` from `red_email_autoresponders`  a INNER Join `red_email_track` e on e.`campaign_id`= a.`campaign_id` WHERE e.`user_id` =$mid and email_track_unsubscribes = 1 $campaignClause";
		echo $sqlShowContacts;exit;
		
			$rsShowContacts = $this->db->query($sqlShowContacts);

			if($rsShowContacts->rowCount() <= 0){
				$this->log_api_event($mid, 'get_auto_unsubscriber', json_encode($aid), "No Unsubscriber exists");
				throw new RestException(404, "No Unsubscriber exists");
			}else{
				foreach($rsShowContacts as $row => $val) {
				#	echo "<pre>";print_r($row);
					$strCid = $val['subscriber_id'];
					
					$retVal[$row]['autoresponder_group_id'] = $val['autoresponder_group_id'];
					$retVal[$row]['Contact ID'] = $val['subscriber_id'];
					$retVal[$row]['Contact Email Address'] = $val['subscriber_email_address']; 
				}
			}
			
			return  ($retVal);

	}
	


	private function log_api_event($mid, $action, $request, $error){
		$qryApiLog = "insert into `red_api_log` set `member_id`='$mid', `date_added`=now(), `api_action`='$action', `api_request`='$request', `api_error`='$error'";
		$this->db->exec($qryApiLog);

	}
	private function validate_lists_ownership($lists){
		$retval = true;
		$mid = AccessControl::$member_id;
		if(is_array($lists))$strLists = @implode(',',$lists);else $strLists = $lists;
		if($strLists !=''){
			$sqlValidateList = "select `subscription_id` from `red_email_subscriptions` where `subscription_id` in($strLists) and `subscription_created_by` = '$mid' and `is_deleted`=0";
			$stmt = $this->db->query($sqlValidateList);
			$row_count = $stmt->rowCount();

			if(count(explode(',',$strLists)) !== $row_count)
			$retval = false;
		}
		return $retval;

	}

}
