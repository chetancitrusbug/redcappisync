$(document).ready( function(){
	$('.error-msg').hide();
	$('.error-msg2').hide();
	$('.error-msg3').hide();
	$('.error-msg4').hide();
	jQuery('#elementsnew').hide();
	var segmenttype = jQuery('.segmenttype').val();
	var radios = $('input:radio[name=select_segment]');
	
	$('#segment_simple').hide();
	$('#segment_recent').hide();
	
	if(segmenttype!= '' ){
		jQuery('#segment_radio'+segmenttype).attr('checked',true);
		if(segmenttype == 5){
			$('#segment_simple').show();
		}else{
			$('#segment_recent').show();
		}
	}else{
		jQuery('#segment_radio5').attr('checked',true);
		$('#segment_simple').show();
	}
	
	
	
	/* var r_field = $( ".rfields option:selected" ).val();
	var r_field_val = $(".r_fields_value").val();
	if(r_field == 'unresponsive'){	
		$('.r_fields_value').hide();
		$('.in').hide();
		$('.days').hide();
	}else{
		$('.r_fields_value').show();
		$('.in').show();
		$('.days').show();
	} */
	
	/* $( ".rfields" ).change(function() {
		var field = $(this).val();
		if(field == 'unresponsive'){
			$('.r_fields_value').val(60);
			$('.r_fields_value').hide();
			$('.days').hide();
			$('.in').hide();
		}else{
			$('.r_fields_value').val(r_field_val);
			$('.r_fields_value').show();
			$('.in').show();
			$('.days').show();
		}
	}); */

	$('input[type=radio][name=select_segment]').change(function() {
		if (this.value == '1') {
			$('#segment_simple').show();
			$('#segment_recent').hide();
		}else{
			$('#segment_simple').hide();
			$('#segment_recent').show();
		}
	});
	

	var segmentid = $('.segmentid').val();
	var base_url = 	$('.baseurl').val();
	var loaderimagepath = 	$('.loaderimagepath').val();
	
	/* Submit click event for the normal segment */
	$('#segmentation').submit(function(){
		jQuery('#elementsnew').remove();
		var bool = 0;
		var seg_title = $('.seg_title').val();
		if(seg_title == ''){
			bool = 1;			
			jQuery('.seg_title').addClass('error-class');
			$('.error-msg3').show();
			return false;
		}else{
			jQuery('.seg_title').removeClass('error-class');
			$('.error-msg3').hide();
		}	
	
		if($('.fields').length == 0){
			bool = 1;	
			$('.error-msg4').show();
		}else{
			$('.error-msg4').hide();
		}
		$('.fields').each(function(i){
			if ($(this).val() == '') {
				bool = 1;				
				$(this).addClass('error-class');
				$('.error-msg').show();
			}else{
				$(this).removeClass('error-class');
				$('.error-msg').hide();
			}
		});
		
		$('.fields_value').each(function(j){
			if ($(this).val() == '') {
				bool = 1;
				$(this).addClass('error-class');
				$('.error-msg2').show();	
			}else{
				$(this).removeClass('error-class');
				$('.error-msg2').hide();
			}
		});
		
		if(bool == 1){
			return false;
		}else if(bool == 0){
			var postData = {};	
			$('.segment_family').each(function (index) {
				var obj = {};
				var field = $(this).find('.fields').val();
				var criteria_is = $(this).find('.criteria_is').val();
				var fields_value = $(this).find('.fields_value').val();
				var ref_id = $(this).attr("data_val");
					obj['field'] = field;
					obj['criteria_is'] = criteria_is;
					obj['fields_value'] = fields_value;
					obj['sg_ref_id'] = ref_id;
					obj['seg_title'] = seg_title;
					obj['seg_type'] = 5;
					postData[index] = obj;
			
			});
			jQuery('.msg').html("<img border='0'  style='margin:0;' src="+loaderimagepath+" />");
			$('.segment_form').addClass('disabled');
			$('.delete_segment').addClass('disabled');
			$('.add_segment').addClass('disabled');
			
			jQuery.post(base_url+"newsletter/segmentation/segment_create/"+segmentid, postData, function(result) {
				var response = jQuery.parseJSON(result);
				if(response.status == 'success'){
					window.location.href = base_url+'newsletter/contacts';					
				} 
			});
			
		}
		return false;
	}); 
	
	/*Add new div on click of add segment*/
		 var divhtml = $("#elementsnew").html();
			$(".add_segment").click(function(){
				$("#elements").append(divhtml);
					//$(".segment_family_new").last().attr('id','segment_family');	
			}); 
		
		
		/*delete div on click of delete segment*/		
		$('.delete_segment').live('click', function () {
			var parent_ob = $(this).parents('.segment_family');
			parent_ob.remove();
			/* if ($(this).parents('.segment_family').attr('data_val') == ref_id){
				$(this).parents('.segment_family').remove();
			}; */
			
		});	  
		
			
	/* Submit click event for the recent segment */
	
	
		$('#recent').submit(function(){
			$('.error-msg3').hide();
			$('.error-msg').hide();
			$('.error-msg2').hide();
			jQuery('.seg_title').removeClass('error-class');
			jQuery('.rfields').removeClass('error-class');
			jQuery('.r_fields_value').removeClass('error-class');
		
			var error = 0;
			var seg_title = $('.seg_title').val();
			var field = $( ".rfields option:selected" ).val();
			var field_val = $(".r_fields_value").val();
			var number_pattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
			var test_pattern = /\D/g;

			if(seg_title == ''){
				jQuery('.seg_title').addClass('error-class');
				$('.error-msg3').show();
				return false;
			}else if(field == ''){
				jQuery('.rfields').addClass('error-class');
				$('.error-msg').show();
				return false;		
			}else if(field_val == ''){
				jQuery('.r_fields_value').addClass('error-class');
				$('.error-msg2').show();
				return false;		
			}else if(field_val != '' && test_pattern.test(field_val) == true){
				jQuery('.r_fields_value').addClass('error-class');
				return false;
			}else{
				var postData = {};
				
				postData['seg_title'] = seg_title;
				postData['field'] = field;
				postData['field_val'] = field_val;
				postData['seg_type'] = 6;
				
				$('.recent_form').addClass('disabled');
				jQuery('.msg').html("<img border='0'  style='margin:0;' src="+loaderimagepath+" />");
				jQuery.post(base_url+"newsletter/segmentation/segment_create/"+segmentid, postData, function(result) {
					var response = jQuery.parseJSON(result);
					if(response.status == 'success'){
						window.location.href = base_url+'newsletter/contacts';					
					} 
				});
			
				return false;
			}
			
		});
	
	
	
});