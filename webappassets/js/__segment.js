$(document).ready( function(){
	jQuery('#elementsnew').hide();
	$('.error-msg').hide();
	$('.error-msg2').hide();
	$('.error-msg3').hide();
	$('.error-msg4').hide();
	var segmentid = $('.segmentid').val();
	var base_url = 	$('.baseurl').val();
	var loaderimagepath = 	$('.loaderimagepath').val();
	
	
	$('#segmentation').submit(function(){
		jQuery('#elementsnew').remove();
		var seg_title = $('.seg_title').val();
		if(seg_title == ''){
			bool = 1;			
			jQuery('.seg_title').addClass('error-class');
			$('.error-msg3').show();
			return false;
		}else{
			jQuery('.seg_title').removeClass('error-class');
			$('.error-msg3').hide();
		}	
		var bool = 0;
		if($('.fields').length == 0){
			bool = 1;	
			$('.error-msg4').show();
		}else{
			$('.error-msg4').hide();
		}
		$('.fields').each(function(i){
			if ($(this).val() == '') {
				bool = 1;				
				$(this).addClass('error-class');
				$('.error-msg').show();
			}else{
				$(this).removeClass('error-class');
				$('.error-msg').hide();
			}
		});
		
		$('.fields_value').each(function(j){
			if ($(this).val() == '') {
				bool = 1;
				$(this).addClass('error-class');
				$('.error-msg2').show();	
			}else{
				$(this).removeClass('error-class');
				$('.error-msg2').hide();
			}
		});
		
		if(bool == 1){
			return false;
		}else if(bool == 0){
			var postData = {};	
			$('.segment_family').each(function (index) {
				var obj = {};
				var field = $(this).find('.fields').val();
				var criteria_is = $(this).find('.criteria_is').val();
				var fields_value = $(this).find('.fields_value').val();
				var ref_id = $(this).attr("data_val");
					obj['field'] = field;
					obj['criteria_is'] = criteria_is;
					obj['fields_value'] = fields_value;
					obj['sg_ref_id'] = ref_id;
					obj['seg_title'] = seg_title;
					postData[index] = obj;
			
			});
			jQuery('.msg').html("<img border='0'  style='margin:0;' src="+loaderimagepath+" />");
			$('.segment_form').addClass('disabled');
			$('.delete_segment').addClass('disabled');
			$('.add_segment').addClass('disabled');
			
			jQuery.post(base_url+"newsletter/segmentation/segment_create/"+segmentid, postData, function(result) {
				var response = jQuery.parseJSON(result);
				if(response.status == 'success'){
					window.location.href = base_url+'newsletter/contacts';					
				} 
			});
			
		}
		return false;
	}); 
	
	/*Add new div on click of add segment*/
		 var divhtml = $("#elementsnew").html();
			$(".add_segment").click(function(){
				$("#elements").append(divhtml);
					//$(".segment_family_new").last().attr('id','segment_family');	
			}); 
		
		
		/*delete div on click of delete segment*/		
		$('.delete_segment').live('click', function () {
			var parent_ob = $(this).parents('.segment_family');
			parent_ob.remove();
			/* if ($(this).parents('.segment_family').attr('data_val') == ref_id){
				$(this).parents('.segment_family').remove();
			}; */
			
		});	  
	
	
	
});