<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=1100"/>
<title>RedCappi is currently down for maintenance</title>
<link rel="shortcut icon" href="http://www.redcappi.com/favicon.ico">
<link href="http://www.redcappi.com/webappassets/css/inner_red.css?v=7-9-13" rel="stylesheet" type="text/css" /><!--[if IE 7]>
  <link href="http://www.redcappi.com/webappassets/css/font-awesome-ie7.min.css?v=6-20-13" rel="stylesheet" type="text/css" /><![endif]-->
<script type="text/javascript" src="http://www.redcappi.com/webappassets/js/jquery-1.4.4.min.js?v=6-20-13"></script>
<script type="text/javascript" src="http://www.redcappi.com/webappassets/js/fancybox/jquery.fancybox-1.3.4.pack.js?v=6-20-13"></script>
<script type="text/javascript" src="http://www.redcappi.com/webappassets/js/jquery.blockUI.js?v=6-20-13"></script>
<link rel="stylesheet" type="text/css" href="http://www.redcappi.com/webappassets/js/fancybox/jquery.fancybox-1.3.4.css?v=6-20-13" media="screen" />
<!--[if lt IE 9]>
<script src="http://www.redcappi.com/webappassets/js/html5shiv-printshiv.js?v=6-20-13"></script>
<![endif]-->
</head>
<body>

<!--[page html]-->
<div id="wrapper">
  <!--[header]-->
    

  <div id="header-main">
    <div id="header-menu">
      <a href="http://www.redcappi.com/newsletter/campaign" id="logo" title="RedCappi"></a>
    </div>
  </div>
  <!--[header]-->

  <!--[body]-->
<div id="body-dashborad">
  <div id="first-time-sender" class="container">
  <h1>RedCappi is currently down for maintenance</h1>
    <div>

      <p style="font-size:20px;">
        <img src="http://www.redcappi.com/webappassets/images/home-page-face.png?v=6-20-13" width="150" alt="logo" title="logo"/>
      
	  
	  <br/> 
         
        
          We promise to be back soon. Thanks for your patience.
        
		
		
		<br/>
		<br/>
		<br/>
          Cheers,<br />
          RedCappi Support Team<br/>
          support@redcappi.com        </p>	 
    </div>
  </div>
</div>

  <!--[/body]-->
</body>
</html>
<?php
header('HTTP/1.1 503 Service Temporarily Unavailable');
header('Status: 503 Service Temporarily Unavailable');
header('Retry-After: 3600');
?>